/*
Navicat MySQL Data Transfer

Source Server         : 47.52.57.16
Source Server Version : 50718
Source Host           : 47.52.57.16:3306
Source Database       : community

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2020-01-08 18:37:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `community_user_article`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_article`;
CREATE TABLE `community_user_article` (
  `pid` varchar(32) NOT NULL COMMENT '文章ID',
  `uid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `p_title` varchar(100) DEFAULT NULL COMMENT '文章标题',
  `p_message` varchar(1000) DEFAULT NULL COMMENT '文章信息',
  `p_tags` varchar(100) DEFAULT NULL COMMENT '文章标签',
  `p_sendtime` datetime DEFAULT NULL COMMENT '发布时间',
  `p_type` int(2) DEFAULT NULL COMMENT '文章分类',
  `p_isdel` int(2) DEFAULT NULL COMMENT '是否删除',
  `p_hotscore` int(10) DEFAULT NULL COMMENT '文章热度观看数',
  `p_topscore` int(10) DEFAULT NULL COMMENT '排行热度推荐学习值',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块文章表';

-- ----------------------------
-- Records of community_user_article
-- ----------------------------

-- ----------------------------
-- Table structure for `community_user_chat`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_chat`;
CREATE TABLE `community_user_chat` (
  `from_uid` varchar(32) DEFAULT NULL,
  `to_uid` varchar(32) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `type` int(1) DEFAULT '0' COMMENT '消息类型0文字1图片2视频3语音',
  KEY `index1` (`from_uid`,`to_uid`,`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of community_user_chat
-- ----------------------------
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但是多', '2019-12-31 22:12:26', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但是多1133', '2019-12-31 22:12:40', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但是多1133', '2019-12-31 22:12:42', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2019-12-31 22:12:45', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2019-12-31 22:44:56', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2019-12-31 22:44:57', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2019-12-31 22:44:58', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2020-01-02 21:35:49', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2020-01-02 21:36:20', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2020-01-02 21:36:21', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2020-01-02 21:36:21', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2020-01-02 21:36:21', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '但是但111是多1133', '2020-01-02 21:36:21', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 21:37:03', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 21:37:04', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 21:37:04', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:24:19', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:26', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:26', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:26', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:27', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:27', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:27', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:27', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:27', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:28', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:28', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:28', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:28', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:27:28', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:35:11', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:38:26', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '41479828b219239398cc75363fc6fa9f', '但是但111是多1133', '2020-01-02 23:45:22', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '张三你就是个傻逼', '2020-01-02 23:47:42', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '张三你就是个傻逼1', '2020-01-03 00:22:43', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '张三你就是个傻逼2', '2020-01-03 00:44:54', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '张三你就是个傻逼3', '2020-01-03 00:44:55', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '张三你就是个傻逼4', '2020-01-03 00:45:15', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '但是但111是多1133114441', '2020-01-03 01:14:07', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', 'cao', '2020-01-04 01:59:05', '0');
INSERT INTO `community_user_chat` VALUES ('5484f6fdc9b0faf1a32f31476d38c9d9', '41479828b219239398cc75363fc6fa9f', '你草个试试', '2020-01-04 02:08:47', '0');
INSERT INTO `community_user_chat` VALUES ('5484f6fdc9b0faf1a32f31476d38c9d9', '41479828b219239398cc75363fc6fa9f', '你草个试试1', '2020-01-04 02:08:49', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '你草个试试3', '2020-01-04 02:15:59', '0');
INSERT INTO `community_user_chat` VALUES ('5484f6fdc9b0faf1a32f31476d38c9d9', '41479828b219239398cc75363fc6fa9f', '你草个试试4', '2020-01-04 02:16:11', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '你草里个试试5', '2020-01-04 03:23:33', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '哈哈', '2020-01-04 03:24:29', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '哈哈', '2020-01-04 03:24:43', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '哈哈', '2020-01-04 03:25:00', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '傻逼', '2020-01-04 03:25:13', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '傻逼', '2020-01-04 03:25:35', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '傻逼', '2020-01-04 03:27:35', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '渣子', '2020-01-04 03:27:50', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '渣子', '2020-01-04 03:28:11', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈', '2020-01-04 03:32:59', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈', '2020-01-04 03:34:50', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈', '2020-01-04 03:35:18', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈', '2020-01-04 03:36:42', '0');
INSERT INTO `community_user_chat` VALUES ('5484f6fdc9b0faf1a32f31476d38c9d9', '41479828b219239398cc75363fc6fa9f', '你草个试试4', '2020-01-04 04:50:16', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '你草个试试4', '2020-01-04 04:50:58', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '62fa7cae653e4051abe917d18cbb59d0', '你草个试试4', '2020-01-04 04:54:01', '0');
INSERT INTO `community_user_chat` VALUES ('62fa7cae653e4051abe917d18cbb59d0', '41479828b219239398cc75363fc6fa9f', '你再骂一句', '2020-01-04 04:54:51', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '好吧', '2020-01-06 10:35:05', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', '哈哈', '2020-01-06 10:35:43', '0');
INSERT INTO `community_user_chat` VALUES ('41479828b219239398cc75363fc6fa9f', '5484f6fdc9b0faf1a32f31476d38c9d9', 'lala', '2020-01-06 11:16:27', '0');
INSERT INTO `community_user_chat` VALUES ('44444444444444', '62fa7cae653e4051abe917d18cbb59d0', '你好', '2020-01-08 05:10:47', '0');
INSERT INTO `community_user_chat` VALUES ('44444444444444', '62fa7cae653e4051abe917d18cbb59d0', '你好', '2020-01-08 11:20:27', '0');
INSERT INTO `community_user_chat` VALUES ('44444444444444', '62fa7cae653e4051abe917d18cbb59d0', '你呢', '2020-01-08 18:11:36', '0');

-- ----------------------------
-- Table structure for `community_user_discuss`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_discuss`;
CREATE TABLE `community_user_discuss` (
  `disc_id` varchar(32) NOT NULL COMMENT '评论ID',
  `disc_did` varchar(32) DEFAULT NULL COMMENT '动态ID',
  `disc_pid` varchar(32) DEFAULT NULL COMMENT '文章ID',
  `disc_uid` varchar(32) DEFAULT NULL COMMENT '评论用户ID',
  `disc_at_uid` varchar(32) DEFAULT NULL COMMENT 'AT的用户ID',
  `disc_message` varchar(200) DEFAULT NULL COMMENT '评论信息',
  `disc_creatime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`disc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='动态评论表';

-- ----------------------------
-- Records of community_user_discuss
-- ----------------------------
INSERT INTO `community_user_discuss` VALUES ('0086a8baedd7fba4fadba053cf4df289', '', '10b981f796c24c4fe27fb16d5a327348', '62fa7cae653e4051abe917d18cbb59d0', '', '这是一个评论', '2019-12-26 21:13:03');
INSERT INTO `community_user_discuss` VALUES ('0226d780884a5fcf30e716fab423750f', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:03');
INSERT INTO `community_user_discuss` VALUES ('072c8e6e6ce96610bb3ae58ea67b3ba1', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多877777888-----', '2019-12-06 17:33:51');
INSERT INTO `community_user_discuss` VALUES ('0f8e5860acf70d4be0f4b91cff252196', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 17:43:58');
INSERT INTO `community_user_discuss` VALUES ('10b981f796c24c4fe27fb16d5a327348', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:16:26');
INSERT INTO `community_user_discuss` VALUES ('11111', '04424c3597db40099c9cb0de3bec71d1', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '热热我', '2019-12-06 14:14:46');
INSERT INTO `community_user_discuss` VALUES ('111618ed3b2e0e1cda990a17c0f9b8c3', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 17:47:13');
INSERT INTO `community_user_discuss` VALUES ('15fd5a211bfdde01dbdafcaf88730273', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:04');
INSERT INTO `community_user_discuss` VALUES ('16dea77fe308c20b389f691bc0890522', 'ec7d6f7a253f3bc3df44b92bebf24064', '', '44444444444444', '', '嗯ヽ(○^㉨^)ﾉ♪', '2020-01-08 08:31:34');
INSERT INTO `community_user_discuss` VALUES ('1880489ab312af20b3dcdd69e0359cd7', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 21:55:03');
INSERT INTO `community_user_discuss` VALUES ('21db4005618877e7a60d5cb725b3d192', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '', '在吗', '2020-01-08 11:21:09');
INSERT INTO `community_user_discuss` VALUES ('2222', '04424c3597db40099c9cb0de3bec71d1', null, '41479828b219239398cc75363fc6fa9f', null, '特温特we', '2019-12-06 14:15:02');
INSERT INTO `community_user_discuss` VALUES ('2346e73bdc1c3d241f25035fa10a32cd', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 21:55:02');
INSERT INTO `community_user_discuss` VALUES ('240b50be22d0b2c7d75b0e50b639767a', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多877777888-----', '2019-12-06 17:30:21');
INSERT INTO `community_user_discuss` VALUES ('2c38202933bf89e3d0c6c28beffe00dd', 'd513ac404eed33e73cbb7bd68d2b1d3c', '', '62fa7cae653e4051abe917d18cbb59d0', '', '这是一个评论', '2020-01-02 06:12:50');
INSERT INTO `community_user_discuss` VALUES ('30be68343db5e78e1926e4907e453d61', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '', '哈', '2020-01-08 14:15:41');
INSERT INTO `community_user_discuss` VALUES ('313b7124e6ff703e7af809108dd8074b', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:02');
INSERT INTO `community_user_discuss` VALUES ('3333', '04424c3597db40099c9cb0de3bec71d1', null, '87b1c6eff2c4a1b98cd381246c89fae8', null, '是上电视大V', '2019-12-06 14:17:10');
INSERT INTO `community_user_discuss` VALUES ('3643262f1c21154703b9fd3619520914', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:00');
INSERT INTO `community_user_discuss` VALUES ('3f577bdf4c313b21ac44714b8b6ea32e', '10b981f796c24c4fe27fb16d5a327348', '', '62fa7cae653e4051abe917d18cbb59d0', '', '这是一个评论', '2019-12-26 21:13:45');
INSERT INTO `community_user_discuss` VALUES ('4971d722f1a902aae2230b05e8e2e7f9', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 21:57:07');
INSERT INTO `community_user_discuss` VALUES ('4d9d78e2ff98516066b12821a9b6dcdc', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '', '饺子不错', '2020-01-08 05:11:53');
INSERT INTO `community_user_discuss` VALUES ('4f5f181567134dab4b23812d8c7419e7', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多8888-----', '2019-12-06 17:21:52');
INSERT INTO `community_user_discuss` VALUES ('544d802942eea22f20229839c8dbfb77', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '', '哈1', '2020-01-08 14:16:21');
INSERT INTO `community_user_discuss` VALUES ('5e48a68c1cdc824547de659068db8e69', 'd513ac404eed33e73cbb7bd68d2b1d3c', '', '62fa7cae653e4051abe917d18cbb59d0', '252b1d14e8b96d194cc9b449c90ea98a', '这是一个评论,@了李斯', '2020-01-02 06:27:38');
INSERT INTO `community_user_discuss` VALUES ('5ecde6660895e74961324c834fa907c9', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111大萨达撒大所多877777888-----', '2019-12-06 17:35:06');
INSERT INTO `community_user_discuss` VALUES ('6000ed765f57c79a7f8164b39df70507', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '', '啊啊啊', '2020-01-08 14:11:49');
INSERT INTO `community_user_discuss` VALUES ('7e54d7703a3293aa61fcc35a5ae3c223', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '', '哈1哈2', '2020-01-08 14:16:27');
INSERT INTO `community_user_discuss` VALUES ('88e1bc71f55b416719d1aa4ff91e9620', 'ffcf73ebb228b97ccd7891cf52cb6e89', '', '44444444444444', '', '来一个', '2020-01-08 14:43:53');
INSERT INTO `community_user_discuss` VALUES ('8e7fd9de4570c124109380ccf27b4c16', '10b981f796c24c4fe27fb16d5a327348', '', '62fa7cae653e4051abe917d18cbb59d0', '', '这是一个评论', '2020-01-01 22:16:17');
INSERT INTO `community_user_discuss` VALUES ('964abf6ab0ad5942393cbdcc1e4f838d', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多8888-----', '2019-12-06 17:23:27');
INSERT INTO `community_user_discuss` VALUES ('ab69d921ce649995d0f07cbfac959604', 'ffcf73ebb228b97ccd7891cf52cb6e89', '', '44444444444444', '', '哎', '2020-01-08 15:34:17');
INSERT INTO `community_user_discuss` VALUES ('ac41ab08357ed7032ee18b620516cb09', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 17:42:12');
INSERT INTO `community_user_discuss` VALUES ('d570b83fd667c9266935972eddd40d59', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:03');
INSERT INTO `community_user_discuss` VALUES ('d57aa5c8b1775b52b0c12f267b4b1060', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多8888-----', '2019-12-06 17:15:30');
INSERT INTO `community_user_discuss` VALUES ('e239a002491c72c713ed31aaeb64e941', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '44444444444444', '哈哈3', '2020-01-08 14:18:01');
INSERT INTO `community_user_discuss` VALUES ('f612e50c13aa7759617ab5306a6018e0', '9ca2858c005a11424a27ba51455dab56', '', '44444444444444', '', '好吗', '2020-01-08 14:15:04');

-- ----------------------------
-- Table structure for `community_user_dynamics`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_dynamics`;
CREATE TABLE `community_user_dynamics` (
  `did` varchar(32) NOT NULL COMMENT '动态ID',
  `uid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `d_message` varchar(500) DEFAULT NULL COMMENT '动态标题',
  `d_posttime` datetime DEFAULT NULL COMMENT '发布时间',
  `d_title` varchar(100) DEFAULT NULL COMMENT '动态标题',
  `d_type` int(2) DEFAULT '0' COMMENT '动态类型:0文字1图片和文字2语音和文字3视频和文字',
  `d_isdel` int(2) DEFAULT '0' COMMENT '是否删除',
  `d_hotscore` int(10) DEFAULT '0' COMMENT '热度分数观看数',
  `d_zancount` int(10) DEFAULT '0' COMMENT '点赞数',
  `d_discusscount` int(10) DEFAULT '0' COMMENT '评论数',
  `location` varchar(100) DEFAULT NULL COMMENT '位置',
  `d_url` varchar(1000) DEFAULT NULL COMMENT '资源ur：9张图片，1视频，1语音',
  PRIMARY KEY (`did`),
  KEY `post_time_index` (`d_posttime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块动态表';

-- ----------------------------
-- Records of community_user_dynamics
-- ----------------------------
INSERT INTO `community_user_dynamics` VALUES ('01d970cadb25d4211b902b13997a84df', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '888', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('04424c3597db40099c9cb0de3bec71d1', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('054a602555157c9d4b440dca1b6e7736', '62fa7cae653e4051abe917d18cbb59d0', '还是啥的', '2019-12-27 19:06:51', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('0559c8a42cfd1769f4f00c9b1bd53481', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('05748d6dff6ed5a01e253a9522c402d2', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('08bd8511ba87073f5a15f3aa5cc3681e', '44444444444444', '这是一个信息1', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('09661ac8289502796ed81e14ae79e3a1', '44444444444444', '这是一个信息2', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('099a99dfcd299a1def3d5479f4c165b9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('0c335da2235eb8fc9879b999c056302c', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('0ea5d26dec7f057ce32dc7810f3704d9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('10b981f796c24c4fe27fb16d5a327348', '44444444444444', '这是一个信息666', '2020-01-01 22:42:45', '这是一个标题', '0', '0', '0', '1', '20', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('131e13abe2cf65e4d0fbf8c9a5f7e637', '44444444444444', '这是一个信息3', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('183a1c909d72a852e9b224ad70f7aa91', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:02:38', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912527512_TleRDF.png|');
INSERT INTO `community_user_dynamics` VALUES ('1854a48517e416e8d5e77ac9b6f5f0af', '44444444444444', '这是一个信息4', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('18bf535e5c3728001d56d8c0ce81492a', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。', '2020-01-02 05:22:06', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577913725043_MEjUUC.gif|');
INSERT INTO `community_user_dynamics` VALUES ('1aeafc3d86963f8fcdd8d6cc5e776050', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('1ebbbeeb68dde554ea7f8f6d7b834841', '44444444444444', '这是一个信息5', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('2263ed45aa384d1b890fa2775a88896b', '62fa7cae653e4051abe917d18cbb59d0', 'null', '2019-12-19 01:18:13', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1576689491508_GZNYeD.jpg|photo/1576689491122_PECcmU.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('241b69556be9e9fe4429687e326b7c1d', '44444444444444', '这是一个信息6', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('273292b720fc9cbea0f7160c0c9397d2', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('28680e8c17a84ad637b97be7ce6e0307', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('2a1fc2c7c4f95233f6e19c2e08255ab8', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('2abf9c92f759d6fd95bb3580c40b090e', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('2ac06a460dd9d6f9677bc9569e6a2e2d', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('2cbe46f5786e14b1d1acec74279b2b4b', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:33:50', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo//storage/emulated/0/tencent/MicroMsg/WeiXin/mmexport1577564654560.gif|');
INSERT INTO `community_user_dynamics` VALUES ('2d67a2cd6f0e20df811e0eea723fbb8a', '44444444444444', '这是一个信息7', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('2f07798cabfd0375830af82b0f0be495', '44444444444444', '这是一个信息8', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('3570c6a38edfe50bd5b7ae1f81c677c9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('3678d9f740c59f6929447ab8b78acb5f', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('39c24f7b4678e5aa992fed983b470a02', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:36', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('3a5799e62dc6ded096b3d34ad55bc3fb', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('3a9aee6cc9f24b80883e4f870fdd17e8', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('3b2644c28a527bd54c4fe13e8dbea4b8', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('40606aa1b9be11372f0583e023dfb34e', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:36:49', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577914607579_wURFsb.gif|');
INSERT INTO `community_user_dynamics` VALUES ('4095da18e797f969612f897a73a3033e', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('420b3ce5ab90b0316ff5e5af6336b1ba', '44444444444444', '这是一个信息9', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('469de6843c0083ae3dda75800635b36b', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('47dc1fd24073d8ee0f8e6d0e4bc15f98', '44444444444444', '这是一个信息10', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('493af65f72bb10b8e3fdb40a08c80ea8', '44444444444444', '这是一个信息11', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('4954343cba16724f17a9964c56f27e32', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:02:23', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('4cae346cc62fcb5cd017d4e8cd619300', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:00:40', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912438121_UdXCMb.png|');
INSERT INTO `community_user_dynamics` VALUES ('4d2560b1bbbdf6401fcde16be0b42ccf', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('4d7f6d84cba4723f6adef192e9e5ba19', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('4f3bcc9370daad71b1219c1dbf960843', '44444444444444', '这是一个信息12', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('543fd63a53479653c22f0b55764998e0', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('56eccfea3f92ba851f39774a3c12f89c', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('57424bcb71ad245e5356399244d904f4', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:07:08', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912826604_UThKbH.png|');
INSERT INTO `community_user_dynamics` VALUES ('574455801b4a3d2eca17e70109b7c9e3', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '33', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('5d1a42a704749e5eec651a7517ff1709', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('5e689b618166ca8e2c9f4b1c4cf480a2', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('602fa5f37d585dfe67194499451eba1c', '44444444444444', '这是一个信息13', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('614e2e263917366bbd438fd0c4d9ff42', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:18:42', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577913520809_idheRe.gif|');
INSERT INTO `community_user_dynamics` VALUES ('63d55424515ea9cbb2ddf508b59d7f5c', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('6485ccce4d6a4f65ea06916403c85f7e', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('67efdef0f573732789fe6654be7cc484', '62fa7cae653e4051abe917d18cbb59d0', '我爱你', '2020-01-01 09:36:05', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('693b54594c070f62aae60613857765ab', '44444444444444', '这是一个信息14', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('69a89adfc988542f2f090a7d945765b5', '44444444444444', '这是一个信息15', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('6a12491c5c61a2e8a51dea016f01ae88', '44444444444444', '这是一个信息16', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('6d13fd01f7c9cdc619fd50e78b2406ec', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈112', '2020-01-01 08:02:36', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('6d612c36214588830b03ef17d5fe9f2b', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('6f4a5bbc745ec2d4cb36a9242d0b5ba2', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('6f4cc19661a4e6547191e9328291c154', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('70c1ee7cf19aecf12feda6ff5cfab4fe', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('725dce607e570a33b09309d6975a2b87', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('72c84fe5243e0c1bb71ba03c34fd2198', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。', '2020-01-02 05:33:17', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo//storage/emulated/0/tencent/MicroMsg/WeiXin/mmexport1577564654560.gif|');
INSERT INTO `community_user_dynamics` VALUES ('78763c1fee128e417d2d3cf406042b98', '44444444444444', '这是一个信息17', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('7c1ed9e360fea5fb3499681cd0ee52f3', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:38:25', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577914703920_CIkSUB.gif|');
INSERT INTO `community_user_dynamics` VALUES ('7e5afa5489b516abef0d09a914636c9c', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈112', '2020-01-01 08:02:56', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('80c44635e0a095f7ef402d72be1c5cc0', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈哈哈', '2019-12-20 14:38:26', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1576823903618_hxyhur.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('82ff6275c2d466169a3d2d652b17577b', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('83170974f636c2b8d2ec9ae70211ce2e', '44444444444444', '这是一个信息18', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('8389becdc3cbb335c4e1c09e25122cd9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('878aadbc1a12edfcae7a3cf6d33a0daf', '44444444444444', '这是一个信息19', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('88940627518f5e5b9d6e23f7e88aaad1', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:32', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('890d2bc1abb84abfdae81e3b728b1c76', '44444444444444', '这是一个信息20', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('8a2d4be7b2fa718c130167ee9dd4fc9e', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:07:52', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912870815_BKXbVh.gif|');
INSERT INTO `community_user_dynamics` VALUES ('8da11d7cb1fd0cbcb8a129b0e093df8f', '44444444444444', '这是一个信息21', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('90e99501a150be665393b9e7a6888f2d', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('92055b682d1807cca4c2250a21278a2c', '62fa7cae653e4051abe917d18cbb59d0', '我去', '2020-01-01 10:42:13', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577846530369_naJcJs.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('92ee90518cefd217d34fa5d551642bab', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('934c95bd69a4108fe938a1ec55769013', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:17:33', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577913451808_jGUNHL.gif|');
INSERT INTO `community_user_dynamics` VALUES ('95d13ad96f122a879a4982e150006959', '44444444444444', '这是一个信息22', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('995a175e5a5fbd12d124b589b04b9966', '62fa7cae653e4051abe917d18cbb59d0', '局域12', '2020-01-01 11:48:17', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850494542_wHyqJb.png|photo/1577850494338_oyrBDP.png|photo/1577850494177_NMNQhy.png|photo/1577850494758_FVNmnt.png|photo/1577850494956_yyEheB.png|photo/1577850494665_lcVLjF.png|');
INSERT INTO `community_user_dynamics` VALUES ('9aec5db3345cc7ea94b68c5c55b5fd81', '62fa7cae653e4051abe917d18cbb59d0', '我去', '2020-01-01 11:39:09', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577849947591_fvdAYa.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('9ca2858c005a11424a27ba51455dab56', '44444444444444', '早安', '2020-01-08 05:11:35', '', '1', '0', '0', '1', '8', '北京市海淀区', 'photo/1578431493689_cvwymy.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('9d395c10f69c0de2406ed1de638b97bf', '62fa7cae653e4051abe917d18cbb59d0', '还是啥的', '2019-12-27 19:07:17', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577444834649_BmcCmC.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('9d96d2b61b795e41f07404019d7a232b', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:38', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('9eaaba9833e92902a4afc02161d099c1', '555555', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a0cc90927e5c3a361a674025f85a1df5', '44444444444444', '这是一个信息23', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a164e859bbf356da785d7c2027ee06f7', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a1f49fd354be81f585e13d30e5169367', '44444444444444', '这是一个信息24', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a261b5d9cea215b7c359d6df5cd870f2', '44444444444444', '这是一个信息25', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a3219dc227b60646f9474c5f7bb08220', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a3fa9e6557b718428fe34c0b832bfabc', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a497192993684b6581321d896fd27534', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:02:26', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('a7f3abf41edd9223c532fcaef1209bf1', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a8847bab20e050234dc56bcfbe47e64d', '44444444444444', '这是一个信息26', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a9e69b08f68404431da8b1437a55b673', '62fa7cae653e4051abe917d18cbb59d0', '局域', '2020-01-01 11:45:54', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850352198_MuCehj.png|photo/1577850352522_aTuRXb.png|');
INSERT INTO `community_user_dynamics` VALUES ('aae43f2e8187c35884711044a8499ae2', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ab17cd55cfec6bd4af89812edea74c64', '62fa7cae653e4051abe917d18cbb59d0', '我们的', '2020-01-02 05:58:59', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('ad195212b5f002e22c652d8d19e4739a', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b2427ee05663d61b032506d3ed690ada', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b47f204b64c1160de0842fa0bbf85a2a', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b491fceef94385a9978e4fd35cca9b04', '62fa7cae653e4051abe917d18cbb59d0', '局域12', '2020-01-01 11:47:25', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850443667_mpOuyq.png|');
INSERT INTO `community_user_dynamics` VALUES ('b63486c12bba28efd3acf361fa933f06', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b75bbf2f5150004d8fde94cea1425df8', '44444444444444', '这是一个信息27', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('bb28c123275846ca86fb156c2565bafd', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('bcecfe248adc0d2fc2712740cb8ca7f7', '62fa7cae653e4051abe917d18cbb59d0', '局域1', '2020-01-01 11:46:21', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850379073_RDTfVO.png|');
INSERT INTO `community_user_dynamics` VALUES ('bf9aa8ce5c7267252be5dbc63301c5ea', '62fa7cae653e4051abe917d18cbb59d0', '基督教诶嘿嘿122', '2019-12-19 02:17:02', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('bfa865791f3e4e91bc62df428232c92a', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c041fc743028542bb074f6bab493c099', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c55337f71fc9ba666f33c301de8e0dfd', '44444444444444', '这是一个信息28', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('c6604f73548dc536b2e90bce2287f6b8', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c6a830861d993febe0d641768148f658', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('c75e4588239dab7cd5787a57641803d7', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c8a36e511a37e27bb54cc976a1fdb51b', '44444444444444', '这是一个信息29', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '3', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('caca24b421b3eaaee0400611b162bb7b', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('cbb0a6e8d958adf2fb547ac62a1a6a7b', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('cde0d3f0b5442c23a1c323e85d3f61dc', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:03', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('ce6a2dffd556e0eb3bbb927ae49f1d91', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ce6df0ff95285fa11ebd986878a6145e', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:35:11', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577914481444_wswBnR.gif|');
INSERT INTO `community_user_dynamics` VALUES ('ceb0b923d0958c18c3a5904ce30e5e2c', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 20:50:31', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577883030181_tNZAgb.png|');
INSERT INTO `community_user_dynamics` VALUES ('d10c0947c706a86d7b237bfc1444c6b6', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d2dc40d38e90033a3f835a84c9b74e9f', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d3945f0897932c02092d223b0f9986c1', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d513ac404eed33e73cbb7bd68d2b1d3c', '62fa7cae653e4051abe917d18cbb59d0', '我们', '2020-01-02 05:49:51', '', '1', '0', '0', '0', '2', '北京市海淀区', 'photo/1577915384044_uIeKRT.gif|photo/1577915383284_QkdZec.gif|photo/1577915384060_eQdngW.gif|');
INSERT INTO `community_user_dynamics` VALUES ('d528da6c853c6d952c1bd112995b4b9b', '44444444444444', '这是一个信息30', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('d5bfb491b43c61df0fdbed78ceaa6ada', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d7ed5f0474f209ce3f2cdf24f55a22bf', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d983358b6ab76170b27344a5123bac3f', '44444444444444', '这是一个信息31', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('de90943fbf369d8e1e6721e085742e6a', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:34', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('e08ff3705a5b38582fca527329e154ae', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('e11f7d934153267ab1cf1baef347ef24', '62fa7cae653e4051abe917d18cbb59d0', '还是', '2020-01-07 14:20:01', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('e4cc43cc34557607f4adf135101e856d', '44444444444444', '这是一个信息32', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e548bc0c56cd66d383a2c47fa42e0c6d', '44444444444444', '这是一个信息33', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e62a821bc66b35bd3ec4b728eb5977da', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e6b4a552cba83c7daf8264fd375e5771', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:07:17', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912835706_IwRiVd.gif|');
INSERT INTO `community_user_dynamics` VALUES ('e73bd86678ae03b602873d013706eebc', '44444444444444', '这是一个信息34', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e85de1d4c39d3ab4277f117a2c71b8b2', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('e956769c2be1980beee4a7f5f8aaa6dd', '44444444444444', '这是一个信息0', '2019-12-14 22:42:45', '是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('ebb210fb7a1ec2e0022077f57496f48f', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ec7d6f7a253f3bc3df44b92bebf24064', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。', '2020-01-02 05:27:12', '', '1', '0', '0', '1', '1', '北京市海淀区', 'photo/1577914030312_rONYIh.gif|');
INSERT INTO `community_user_dynamics` VALUES ('ec80f18c937da625563e29b351dbee0d', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('ee422f12ab105ecf2eb0b3178987ad70', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ef18262d3576d50f44eef30dab09288a', '44444444444444', '这是一个信息35', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('f534b6d87822175a85d6009081c2bc22', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:41', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('f7bb7af53aa56b17d184e27de5aa82f6', '44444444444444', '这是一个信息36', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('f8122cc88a1f18ff2643a121ea136b0a', '44444444444444', '这是一个信息37', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('f9a8ead94bf9127b4f116a6f0c8472cc', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-19 01:05:06', '这是一个标题999', '1', '0', '0', '1', '0', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');
INSERT INTO `community_user_dynamics` VALUES ('fb0b6857a9aed463f6858fa0c8874a50', '44444444444444', '作者是个傻逼', '2019-12-14 22:42:45', '这是一个标题', '1', '0', '0', '1', '0', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');
INSERT INTO `community_user_dynamics` VALUES ('fd8d12b01eb36ad7a35d4dbe85474c90', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈112', '2020-01-01 08:02:53', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('fe6d5de2e320fc95aa1ab83ae771e4e0', '62fa7cae653e4051abe917d18cbb59d0', '嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯', '2019-12-15 22:42:45', '这是一个标题999', '1', '0', '0', '1', '0', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');
INSERT INTO `community_user_dynamics` VALUES ('ffcf73ebb228b97ccd7891cf52cb6e89', '44444444444444', '哈哈我是小姐姐', '2019-12-14 22:42:45', '巴巴巴不得尼玛', '1', '0', '0', '1', '2', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');

-- ----------------------------
-- Table structure for `community_user_follows`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_follows`;
CREATE TABLE `community_user_follows` (
  `from_uid` varchar(32) DEFAULT NULL COMMENT '粉丝ID',
  `to_uid` varchar(32) DEFAULT NULL COMMENT '关注ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `rel_type` int(1) DEFAULT '0' COMMENT '状态0正常1拉黑',
  UNIQUE KEY `follow_ufollow_index` (`from_uid`,`to_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块关注粉丝表';

-- ----------------------------
-- Records of community_user_follows
-- ----------------------------
INSERT INTO `community_user_follows` VALUES ('62fa7cae653e4051abe917d18cbb59d0', '44444444444444', '2019-12-06 22:08:17', '0');

-- ----------------------------
-- Table structure for `community_user_zanlist`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_zanlist`;
CREATE TABLE `community_user_zanlist` (
  `did` varchar(32) DEFAULT NULL COMMENT '动态id',
  `zan_uid` varchar(32) DEFAULT NULL COMMENT '点赞的用户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  UNIQUE KEY `follow_ufollow_index` (`did`,`zan_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块动态点赞列表';

-- ----------------------------
-- Records of community_user_zanlist
-- ----------------------------
INSERT INTO `community_user_zanlist` VALUES ('099a99dfcd299a1def3d5479f4c165b9', '41479828b219239398cc75363fc6fa9f', '2019-12-03 15:37:53');
INSERT INTO `community_user_zanlist` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '1', '2019-12-03 22:35:10');
INSERT INTO `community_user_zanlist` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '5484f6fdc9b0faf1a32f31476d38c9d9', '2019-12-06 21:58:57');
INSERT INTO `community_user_zanlist` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '111', '2019-12-10 12:43:58');
INSERT INTO `community_user_zanlist` VALUES ('b47f204b64c1160de0842fa0bbf85a2a', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:12:42');
INSERT INTO `community_user_zanlist` VALUES ('574455801b4a3d2eca17e70109b7c9e3', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:13:20');
INSERT INTO `community_user_zanlist` VALUES ('d7ed5f0474f209ce3f2cdf24f55a22bf', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:13:34');
INSERT INTO `community_user_zanlist` VALUES ('6d612c36214588830b03ef17d5fe9f2b', '7777777', '2019-12-10 13:14:05');
INSERT INTO `community_user_zanlist` VALUES ('2abf9c92f759d6fd95bb3580c40b090e', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:15:29');
INSERT INTO `community_user_zanlist` VALUES ('ebb210fb7a1ec2e0022077f57496f48f', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:15:39');
INSERT INTO `community_user_zanlist` VALUES ('56eccfea3f92ba851f39774a3c12f89c', '7777777', '2019-12-10 13:15:43');
INSERT INTO `community_user_zanlist` VALUES ('caca24b421b3eaaee0400611b162bb7b', '7777777', '2019-12-10 15:43:23');
INSERT INTO `community_user_zanlist` VALUES ('aae43f2e8187c35884711044a8499ae2', '7777777', '2019-12-10 15:43:24');
INSERT INTO `community_user_zanlist` VALUES ('92ee90518cefd217d34fa5d551642bab', '7777777', '2019-12-10 15:43:35');
INSERT INTO `community_user_zanlist` VALUES ('e85de1d4c39d3ab4277f117a2c71b8b2', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-15 13:18:47');
INSERT INTO `community_user_zanlist` VALUES ('ffcf73ebb228b97ccd7891cf52cb6e89', '44444444444444', '2019-12-15 21:13:56');
INSERT INTO `community_user_zanlist` VALUES ('fe6d5de2e320fc95aa1ab83ae771e4e0', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-16 13:42:02');
INSERT INTO `community_user_zanlist` VALUES ('ee422f12ab105ecf2eb0b3178987ad70', '555555', '2019-12-16 13:42:15');
INSERT INTO `community_user_zanlist` VALUES ('ec80f18c937da625563e29b351dbee0d', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-16 13:42:19');
INSERT INTO `community_user_zanlist` VALUES ('4e9039d097109dfd2f4046b79056cc0d', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-19 02:08:22');
INSERT INTO `community_user_zanlist` VALUES ('bf9aa8ce5c7267252be5dbc63301c5ea', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-19 02:17:17');
INSERT INTO `community_user_zanlist` VALUES ('7e5afa5489b516abef0d09a914636c9c', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 09:14:34');
INSERT INTO `community_user_zanlist` VALUES ('67efdef0f573732789fe6654be7cc484', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 09:36:14');
INSERT INTO `community_user_zanlist` VALUES ('fd8d12b01eb36ad7a35d4dbe85474c90', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 10:29:39');
INSERT INTO `community_user_zanlist` VALUES ('6d13fd01f7c9cdc619fd50e78b2406ec', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 10:32:43');
INSERT INTO `community_user_zanlist` VALUES ('a497192993684b6581321d896fd27534', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 10:34:56');
INSERT INTO `community_user_zanlist` VALUES ('f9a8ead94bf9127b4f116a6f0c8472cc', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 10:35:08');
INSERT INTO `community_user_zanlist` VALUES ('995a175e5a5fbd12d124b589b04b9966', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:50:51');
INSERT INTO `community_user_zanlist` VALUES ('a9e69b08f68404431da8b1437a55b673', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:53:19');
INSERT INTO `community_user_zanlist` VALUES ('9aec5db3345cc7ea94b68c5c55b5fd81', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:53:23');
INSERT INTO `community_user_zanlist` VALUES ('92055b682d1807cca4c2250a21278a2c', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:53:26');
INSERT INTO `community_user_zanlist` VALUES ('bcecfe248adc0d2fc2712740cb8ca7f7', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:53:34');
INSERT INTO `community_user_zanlist` VALUES ('cde0d3f0b5442c23a1c323e85d3f61dc', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:54:08');
INSERT INTO `community_user_zanlist` VALUES ('9d395c10f69c0de2406ed1de638b97bf', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:56:44');
INSERT INTO `community_user_zanlist` VALUES ('b491fceef94385a9978e4fd35cca9b04', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 11:57:08');
INSERT INTO `community_user_zanlist` VALUES ('4954343cba16724f17a9964c56f27e32', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 12:05:38');
INSERT INTO `community_user_zanlist` VALUES ('ceb0b923d0958c18c3a5904ce30e5e2c', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-01 22:17:02');
INSERT INTO `community_user_zanlist` VALUES ('ec7d6f7a253f3bc3df44b92bebf24064', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-08 11:07:05');
INSERT INTO `community_user_zanlist` VALUES ('ab17cd55cfec6bd4af89812edea74c64', '62fa7cae653e4051abe917d18cbb59d0', '2020-01-08 11:11:07');
INSERT INTO `community_user_zanlist` VALUES ('9ca2858c005a11424a27ba51455dab56', '44444444444444', '2020-01-08 14:18:19');
INSERT INTO `community_user_zanlist` VALUES ('10b981f796c24c4fe27fb16d5a327348', '44444444444444', '2020-01-08 14:40:54');
INSERT INTO `community_user_zanlist` VALUES ('fb0b6857a9aed463f6858fa0c8874a50', '44444444444444', '2020-01-08 14:51:47');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(32) NOT NULL,
  `log_type` int(2) DEFAULT NULL COMMENT '日志类型（1登录日志，2操作日志）',
  `log_content` varchar(1000) DEFAULT NULL COMMENT '日志内容',
  `operate_type` int(2) DEFAULT NULL COMMENT '操作类型',
  `userid` varchar(32) DEFAULT NULL COMMENT '操作用户账号',
  `username` varchar(100) DEFAULT NULL COMMENT '操作用户名称',
  `ip` varchar(100) DEFAULT NULL COMMENT 'IP',
  `method` varchar(500) DEFAULT NULL COMMENT '请求java方法',
  `request_url` varchar(255) DEFAULT NULL COMMENT '请求路径',
  `request_param` longtext COMMENT '请求参数',
  `request_type` varchar(10) DEFAULT NULL COMMENT '请求类型',
  `cost_time` bigint(20) DEFAULT NULL COMMENT '耗时',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_table_userid` (`userid`) USING BTREE,
  KEY `index_logt_ype` (`log_type`) USING BTREE,
  KEY `index_operate_type` (`operate_type`) USING BTREE,
  KEY `index_log_type` (`log_type`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('5dfe6d46dc0a239e26ab914d6f7a978f', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-11-30 17:36:33', null, null);
INSERT INTO `sys_log` VALUES ('f3e2f5507800fde4916ac3ba72357aa5', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-01 22:30:55', null, null);
INSERT INTO `sys_log` VALUES ('a9984c7aecce5092b4b4421780037234', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 16:47:31', null, null);
INSERT INTO `sys_log` VALUES ('b8149b62ea2f7aad07dc03d75a541111', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 19:42:55', null, null);
INSERT INTO `sys_log` VALUES ('de4431c704441090b18e7b0f3adc0ba4', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 21:16:03', null, null);
INSERT INTO `sys_log` VALUES ('17d7cbe0cb36672655e1ee7ab0b05f91', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 22:14:13', null, null);
INSERT INTO `sys_log` VALUES ('1054293d9ca2d6b14706918643280c6b', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 23:36:42', null, null);
INSERT INTO `sys_log` VALUES ('e6c3903ac5fabbe45af3e861f9cf1b2d', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 00:37:11', null, null);
INSERT INTO `sys_log` VALUES ('1a6ac59124a3b7cf4c883d707613b23b', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 00:56:04', null, null);
INSERT INTO `sys_log` VALUES ('edb17c76423a539108aef05e4d9bc2b2', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 00:56:51', null, null);
INSERT INTO `sys_log` VALUES ('6a25393dcca14d056fad1ae16ad59867', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:18:41', null, null);
INSERT INTO `sys_log` VALUES ('31b82309ec9c376df21a4b76d7124c47', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:23:03', null, null);
INSERT INTO `sys_log` VALUES ('d71406f980df21d137f1e13293601fe7', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:34:31', null, null);
INSERT INTO `sys_log` VALUES ('b53dc43f17998829c22e7e07a85b8f9a', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:38:55', null, null);
INSERT INTO `sys_log` VALUES ('8280a588764f106e64a09bec385e6f60', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:40:14', null, null);
INSERT INTO `sys_log` VALUES ('af91642c51ea3a0360c377e724f18d3f', '1', '用户名: test,登录成功！', null, null, null, '106.122.220.130', null, null, null, null, null, null, '2019-12-14 22:47:47', null, null);
INSERT INTO `sys_log` VALUES ('c8d765baceece692a86922227bd73a06', '1', '用户名: test,退出成功！', null, null, null, '120.244.112.131', null, null, null, null, null, null, '2019-12-14 22:47:54', null, null);
INSERT INTO `sys_log` VALUES ('7bfed134f8952fa76a237c72b725f508', '1', '用户名: test,退出成功！', null, null, null, '120.244.112.131', null, null, null, null, null, null, '2019-12-14 22:48:01', null, null);
INSERT INTO `sys_log` VALUES ('65358aba58ddd79b604156c555a8a345', '1', '用户名: test,退出成功！', null, null, null, '120.244.112.131', null, null, null, null, null, null, '2019-12-14 22:48:01', null, null);
INSERT INTO `sys_log` VALUES ('c10dcf6291aacf479f6fd926f46338f9', '1', '用户名: test,登录成功！', null, null, null, '120.244.112.131', null, null, null, null, null, null, '2019-12-14 23:50:51', null, null);
INSERT INTO `sys_log` VALUES ('d37f28628f124cbe6adb98ab2e64baae', '1', '用户名: test,登录成功！', null, null, null, '106.122.220.130', null, null, null, null, null, null, '2019-12-14 23:56:00', null, null);
INSERT INTO `sys_log` VALUES ('831469f1418668fa29341be960c13343', '1', '用户名: test,登录成功！', null, null, null, '223.72.62.245', null, null, null, null, null, null, '2019-12-15 12:46:43', null, null);
INSERT INTO `sys_log` VALUES ('2ba3d0bd1059fabf6a583a4106771e7a', '1', '用户名: test,登录成功！', null, null, null, '223.72.62.245', null, null, null, null, null, null, '2019-12-15 13:10:13', null, null);
INSERT INTO `sys_log` VALUES ('d3a65d897912d97d53bdeee5adb906b5', '1', '用户名: test,登录成功！', null, null, null, '223.72.62.245', null, null, null, null, null, null, '2019-12-15 13:10:13', null, null);
INSERT INTO `sys_log` VALUES ('c6fccf1cedfd05c664b2e8ba67cc2b42', '1', '用户名: test,登录成功！', null, null, null, '106.122.220.130', null, null, null, null, null, null, '2019-12-15 22:24:37', null, null);
INSERT INTO `sys_log` VALUES ('32bd15660fed319ee1055380b0403d96', '1', '用户名: test,登录成功！', null, null, null, '106.122.220.130', null, null, null, null, null, null, '2019-12-15 22:36:29', null, null);
INSERT INTO `sys_log` VALUES ('937bafc815351ab8686fcfd09c62323c', '1', '用户名: test,登录成功！', null, null, null, '106.122.220.130', null, null, null, null, null, null, '2019-12-15 23:54:24', null, null);
INSERT INTO `sys_log` VALUES ('d02c45d84ab57f7c3d80eeed94b32180', '1', '用户名: test,登录成功！', null, null, null, '106.122.220.130', null, null, null, null, null, null, '2019-12-15 23:57:52', null, null);
INSERT INTO `sys_log` VALUES ('4795633a96f1973132a0b166468c71cc', '1', '用户名: test,登录成功！', null, null, null, '106.122.220.130', null, null, null, null, null, null, '2019-12-16 00:33:33', null, null);
INSERT INTO `sys_log` VALUES ('75be4058ab9e4c7478c56844efee0cfe', '1', '用户名: test,登录成功！', null, null, null, '106.122.222.70', null, null, null, null, null, null, '2019-12-26 20:04:07', null, null);
INSERT INTO `sys_log` VALUES ('3c5fdd274d983e2db2da1685ac65e775', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-04 01:25:22', null, null);
INSERT INTO `sys_log` VALUES ('b56eeeac12a1669e41b88f6595b295d0', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-06 11:02:09', null, null);
INSERT INTO `sys_log` VALUES ('8e176b355d30a6b4fc6b18f3693dec22', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-06 11:02:12', null, null);
INSERT INTO `sys_log` VALUES ('fd31522bcd145204d9e4cf87f4f1ef29', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-06 11:02:15', null, null);
INSERT INTO `sys_log` VALUES ('3ac22782318473518eecf8f11e08dc1c', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-06 11:02:15', null, null);
INSERT INTO `sys_log` VALUES ('3baee0fe5aeb08c7dd6ac79c903668f8', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-06 11:02:23', null, null);
INSERT INTO `sys_log` VALUES ('64603bb366743e4d67f57c123f1f622f', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-06 11:02:55', null, null);
INSERT INTO `sys_log` VALUES ('28d85973a7380eb64712b13113694b02', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-07 15:19:51', null, null);
INSERT INTO `sys_log` VALUES ('69adb4b502e676f21aa85962dfb1e512', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-07 15:21:11', null, null);
INSERT INTO `sys_log` VALUES ('d5eb4660130b487421e4cbf9b04b45fd', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-07 15:21:12', null, null);
INSERT INTO `sys_log` VALUES ('ba5df241d07d1c6b8d7c13f6d7e265fa', '1', '用户名: test,登录成功！', null, null, null, '223.72.91.113', null, null, null, null, null, null, '2020-01-07 15:22:08', null, null);

-- ----------------------------
-- Table structure for `ucenter_user_oplog`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_user_oplog`;
CREATE TABLE `ucenter_user_oplog` (
  `uid` varchar(32) NOT NULL COMMENT '用户ID',
  `op_no` varchar(32) DEFAULT NULL COMMENT '操作流水号',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `op_type` int(2) DEFAULT NULL COMMENT '操作类型',
  `op_dec` varchar(200) DEFAULT NULL COMMENT '操作描述',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户操作记录';

-- ----------------------------
-- Records of ucenter_user_oplog
-- ----------------------------

-- ----------------------------
-- Table structure for `ucenter_user_thirdinfo`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_user_thirdinfo`;
CREATE TABLE `ucenter_user_thirdinfo` (
  `uid` varchar(32) NOT NULL COMMENT '用户ID',
  `wx_id` varchar(50) DEFAULT NULL COMMENT '微信ID',
  `qq_id` varchar(50) DEFAULT NULL COMMENT '腾讯ID',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户三方信息';

-- ----------------------------
-- Records of ucenter_user_thirdinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `ucenter_userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_userinfo`;
CREATE TABLE `ucenter_userinfo` (
  `uid` varchar(32) NOT NULL DEFAULT '' COMMENT '用户ID',
  `username` varchar(100) DEFAULT NULL COMMENT '用户名',
  `pwd` varchar(100) DEFAULT '' COMMENT '密码',
  `nickname` varchar(50) DEFAULT '' COMMENT '昵称',
  `sex` varchar(2) DEFAULT '' COMMENT '性别',
  `age` varchar(2) DEFAULT '' COMMENT '年龄',
  `addr` varchar(100) DEFAULT '' COMMENT '地址',
  `location` varchar(10) DEFAULT '' COMMENT '定位',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号',
  `guanzhu_count` int(8) DEFAULT '0' COMMENT '关注数',
  `fensi_count` int(8) DEFAULT '0' COMMENT '粉丝数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(1) unsigned zerofill DEFAULT '0' COMMENT '用户状态。0正常1删除2停用',
  `salt` varchar(50) DEFAULT NULL COMMENT 'md5密码盐',
  `lat` decimal(10,6) DEFAULT NULL COMMENT '纬度',
  `lng` decimal(10,6) DEFAULT NULL COMMENT '经度',
  `head` varchar(500) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `weiyi_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户基础信息表';

-- ----------------------------
-- Records of ucenter_userinfo
-- ----------------------------
INSERT INTO `ucenter_userinfo` VALUES ('252b1d14e8b96d194cc9b449c90ea98a', '1133334444', '111111', '李斯', '0', '1', '111', '', '11', null, null, '2019-12-05 00:06:20', null, '0', null, null, null, '/head/sexboy.jpg');
INSERT INTO `ucenter_userinfo` VALUES ('41479828b219239398cc75363fc6fa9f', 'elastic', '111111', '333333333', '0', '31', '333', '', '13426159617', null, null, '2019-12-05 00:06:20', null, '0', null, null, null, '/head/sexboy.jpg');
INSERT INTO `ucenter_userinfo` VALUES ('44444444444444', 'test', 'dc01d8ce88b1e063', '小鸟', '1', '23', '1', '北京市海淀区', '1', null, null, '2019-12-05 00:06:20', '2020-01-07 15:23:58', '0', 'He9KgmUM', null, null, '/head/sexboy.jpg');
INSERT INTO `ucenter_userinfo` VALUES ('5484f6fdc9b0faf1a32f31476d38c9d9', '1133334', '111111', '115t444', '1', '1', '111', '', '11', null, null, '2019-12-05 00:00:59', null, '0', null, null, null, '/head/sexboy.jpg');
INSERT INTO `ucenter_userinfo` VALUES ('62fa7cae653e4051abe917d18cbb59d0', '11', '111111', '张三', '1', '1', '111', '', '11', null, null, '2019-12-04 23:58:28', null, '0', null, null, null, '/head/timg.jpg');
INSERT INTO `ucenter_userinfo` VALUES ('87b1c6eff2c4a1b98cd381246c89fae8', '555555224', '111111', '11', '0', '1', '111', '', '11', null, null, '2019-12-05 00:08:32', null, '0', null, null, null, '/head/sexboy.jpg');
