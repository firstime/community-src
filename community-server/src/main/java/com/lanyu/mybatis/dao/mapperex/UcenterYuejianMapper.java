package com.lanyu.mybatis.dao.mapperex;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.CommunityUserDynamics;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;
import com.lanyu.mybatis.dao.entity.UcenterYuejian;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface UcenterYuejianMapper extends BaseMapper<UcenterYuejian>{

    @Select("select count(*) from ucenter_yuejian where jiauid = #{jiauid} and yiuid= #{yiuid} and createtime > #{time}")
    int checkYuejian(String jiauid,String yiuid,String time);

    @Update("update ucenter_yuejian set jiaok=#{agree},jiatime=now() where bianhao=#{bianhao}")
    int yuejianUpdateJia(String bianhao,int agree);

    @Update("update ucenter_yuejian set yiok=#{agree},yitime=now() where bianhao=#{bianhao}")
    int yuejianUpdateYi(String bianhao,int agree);

    List<UcenterYuejian> queryYuyue(Page<UcenterYuejian> page, Map<String,Object> m);

    List<UcenterYuejian> queryYuyueFwd(Page<UcenterYuejian> page, Map<String,Object> m);

    List<UcenterYuejian> queryHnFwd(Page<UcenterYuejian> page, Map<String,Object> m);
}