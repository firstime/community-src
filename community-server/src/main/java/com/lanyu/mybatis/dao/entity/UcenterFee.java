package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: suibin
 * @Date: 2021/09/28/23:40
 * @Description:
 */

@Data
@TableName("ucenter_fee")
//@TableComment("用户表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class UcenterFee implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.UUID)
    @TableField("uuid")
    private String uuid;

    @TableField("orderno")
    private String orderno;

    @TableField("fee")
    private String fee;

    @TableField("uid")
    private String uid;

    @TableField("isok")
    private Integer isok;

    @TableField("creatime")
    private Date creatime;

    @TableField("prepayid")
    private String prepayid;


}