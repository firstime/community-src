package com.lanyu.mybatis.dao.mapperex;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.UcenterHongniang;
import com.lanyu.mybatis.dao.entity.UcenterYuejian;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface UcenterHongniangMapper extends BaseMapper<UcenterHongniang>{

    @Select("SELECT e.hn_uid  from ucenter_guanxi e,ucenter_hongniang r where  e.hn_uid=r.hn_uid and e.uid= #{uid}")
    String gethnIdByUid(String uid);

    @Select("SELECT * from ucenter_userinfo e,ucenter_hongniang r where  e.uid=r.hn_uid and e.userbianhao= #{userbianhao}")
    UcenterHongniang gethnByUid(String userbianhao);
}