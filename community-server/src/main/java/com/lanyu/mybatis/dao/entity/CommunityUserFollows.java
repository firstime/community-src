package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName("community_user_follows")
//@TableComment("粉丝表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class CommunityUserFollows implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField("from_uid")
    //@Column(length = 100)
    //@ColumnComment("来自用户ID")
    @JsonProperty("fromUid")
    private String fromUid;

    @TableField("to_uid")
    //@Column(length = 100)
    //@ColumnComment("目标用户ID")
    @JsonProperty("toUid")
    private String toUid;

    @TableField("create_time")
    @JsonProperty("createTime")
    private Date createTime;

    @TableField("rel_type")
    //@Column(length = 11)
    //@ColumnComment("类型")
    @JsonProperty("relType")
    private Integer relType;

}