package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;

@Data
@TableName("ucenter_guanxi")
//@TableComment("用户表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class UcenterGuanxi implements Serializable {
    private static final long serialVersionUID = 1L;


    @TableField("uid")
    private String uid;

    @TableField("fu_uid")
    @JsonProperty("fuuid")
    private String fuuid;

    @TableField("mu_uid")
    @JsonProperty("muuid")
    private String muuid;

    @TableField("hpy_uid")
    @JsonProperty("hpyuid")
    private String hpyuid;

    @TableField("hn_uid")
    @JsonProperty("hnuid")
    private String hnuid;

    @TableField("gx_time")
    @JsonProperty("gxtime")
    private Date gxtime;


    @TableField(exist = false)
    private String fuhead;

    @TableField(exist = false)
    private String muhead;

    @TableField(exist = false)
    private String hpyhead;

    @TableField(exist = false)
    private String hnhead;
    
}