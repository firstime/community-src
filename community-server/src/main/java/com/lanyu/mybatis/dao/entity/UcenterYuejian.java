package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
@TableName("ucenter_yuejian")

public class UcenterYuejian implements Serializable {
    private static final long serialVersionUID = 1L;


    @TableId(type = IdType.UUID)
    @TableField("uuid")
    private String uuid;

    @TableField("jiauid")
	private String jiauid;

    @TableField("yiuid")
    @JsonProperty("yiuid")
    private String yiuid;

    @TableField("jiatime")
    @JsonProperty("jiatime")
    private Date jiatime;

    @TableField("yitime")
    @JsonProperty("yitime")
    private Date yitime;

    @TableField("jiaok")
    @JsonProperty("jiaok")
    private Integer jiaok;

    @TableField("yiok")
    @JsonProperty("yiok")
    private Integer yiok;

    @TableField("createtime")
    @JsonProperty("createtime")
    private Date createtime;

    @TableField("bianhao")
    private String bianhao;

    @TableField("yjiahn")
    private String yjiahn;

    @TableField("yyihn")
    private String yyihn;

    @TableField("zhifuok")
    private Integer zhifuok;

    @TableField(exist = false)
    private String jiafang;

    @TableField(exist = false)
    private String yifang;


    @TableField(exist = false)
    private String jiahead;

    @TableField(exist = false)
    private String yihead;

    @TableField("jiedanok")
    @JsonProperty("jiedanok")
    private Integer jiedanok;

    @TableField(exist = false)
    private String jiahn;

    @TableField(exist = false)
    private String yihn;


    @TableField(exist = false)
    @JsonProperty("jiahn_uid")
    private String jiahnuid;

    @TableField(exist = false)
    @JsonProperty("yihn_uid")
    private String yihnuid;

    @TableField(exist = false)
    private String img;


}