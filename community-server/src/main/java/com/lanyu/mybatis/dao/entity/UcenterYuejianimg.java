package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
@TableName("ucenter_yuejianimg")

public class UcenterYuejianimg implements Serializable {
    private static final long serialVersionUID = 1L;



    @TableField("yuejianid")
    private String yuejianid;

    @TableField("img")
	private String img;





}