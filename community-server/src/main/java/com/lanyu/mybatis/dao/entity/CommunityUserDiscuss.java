package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName("community_user_discuss")
//@TableComment("评论表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class CommunityUserDiscuss implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
    @TableField("disc_id")
    //@Column(length = 100)
    //@ColumnComment("聊天ID")
    @JsonProperty("discId")
    private String discId;

    @TableField("disc_did")
    //@Column(length = 100)
    //@ColumnComment("动态ID")
    @JsonProperty("discDid")
    private String discDid;


    @TableField(exist = false)
    @JsonProperty("atUsers")
    private String atUsers;


    @TableField(exist = false)
    @JsonProperty("usrNickname")
    private String usrNickname;


    @TableField(exist = false)
    @JsonProperty("userHead")
    private String userHead;

    @TableField("disc_pid")
    //@Column(length = 100)
    //@ColumnComment("PID")
    @JsonProperty("discPid")
    private String discPid;

    @TableField("disc_uid")
    //@Column(length = 100)
    //@ColumnComment("用户ID")
    @JsonProperty("discUid")
    private String discUid;

    @TableField("disc_at_uid")
    //@Column(length = 100)
    //@ColumnComment("评价的用户")
    @JsonProperty("discAtUid")
    private String discAtUid;

    @TableField("disc_message")
    //@Column(length = 1000)
    //@ColumnComment("评论的内容")
    @JsonProperty("discMessage")
    private String discMessage;

    @TableField("disc_creatime")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("评论创建时间")
    @JsonProperty("discCreatime")
    private Date discCreatime;

    @TableField("disc_huifu_discid")
    @JsonProperty("discHuifuDiscid")
    private String discHuifuDiscid;

}