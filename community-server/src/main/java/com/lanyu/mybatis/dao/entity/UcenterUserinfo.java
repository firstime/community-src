package com.lanyu.mybatis.dao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@TableName("ucenter_userinfo")
//@TableComment("用户表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class UcenterUserinfo implements Serializable {
    private static final long serialVersionUID = 1L;
	@TableId(type = IdType.UUID)
    @TableField("uid")
    //@Column(length = 100)
    //@ColumnComment("用户ID")
    private String uid;

    @TableField("username")
    //@Column(length = 100)
    //@ColumnComment("用户账号")
    private String username;

    @TableField("pwd")
    //@Column(length = 100)
    //@ColumnComment("用户密码")
    private String pwd;

    @TableField(exist = false)
    private String cpwd;

    @TableField("nickname")
    //@Column(length = 100)
    //@ColumnComment("用户昵称")
    private String nickname;

    @TableField("sex")
    //@Column(length = 2)
    //@ColumnComment("性别")
    private String sex;

    @TableField("age")
    //@Column(length = 2)
    //@ColumnComment("年龄")
    private String age;

    @TableField("addr")
    //@Column(length = 100)
    //@ColumnComment("地址")
    private String addr;

    @TableField("location")
    //@Column(length = 100)
    //@ColumnComment("位置")
    private String location;

    @TableField("mobile")
    //@Column(length = 100)
    //@ColumnComment("电话")
    private String mobile;

    @TableField("guanzhu_count")
    //@Column(length = 11)
    //@ColumnComment("关注数")
    @JsonProperty("guanzhuCount")
    private Integer guanzhuCount;

    @TableField("fensi_count")
    //@Column(length = 11)
    //@ColumnComment("粉丝数")
    @JsonProperty("fensiCount")
    private Integer fensiCount;


    @TableField("create_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("创建时间")
    @JsonProperty("createTime")
    private Date createTime;

    @TableField("update_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("更新时间")
    @JsonProperty("updateTime")
    private Date updateTime;

    @TableField("status")
   // @Column(length = 2)
    //@ColumnComment("状态")
    private Integer status;

    @TableField("salt")
    //@Column(length = 100)
    //@ColumnComment("密码盐")
    private String salt;

    @TableField("lat")
    //@Column(length = 11)
    //@ColumnComment("坐标")
    private BigDecimal lat;

    @TableField("lng")
    //@Column(length = 11)
    //@ColumnComment("坐标")
    private BigDecimal lng;

    @TableField("head")
    //@Column(length = 100)
    //@ColumnComment("头像")
    private String head;

    @TableField("qianming")
    private String qianming;

    @TableField("shengao")
    private String shengao;

    @TableField("shencai")
    private String shencai;

    @TableField("login_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("登录时间")
    @JsonProperty("loginTime")
    private String loginTime;

    @TableField("education")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("登录时间")
    @JsonProperty("education")
    private String education;

    @TableField("userbianhao")
    private String userbianhao;

    @TableField("shouru")
    private String shouru;


    @TableField("jifen")
    private Integer jifen;

    @TableField("fenghao")
    private Integer fenghao;


    @TableField("regifrom")
    private String regifrom;

    @TableField("zhaohufee")
    private String zhaohufee;

    @TableField("yuejianfee")
    private String yuejianfee;

    @TableField("huntype")
    private String huntype;


    @TableField(exist = false)
    private String token;


    @TableField(exist = false)
    private String fuwuhuiyuan;

    @TableField(exist = false)
    private String fuwushouru;

    @TableField(exist = false)
    private String biaoyu;

    @TableField(exist = false)
    private String hnmobile;

    @TableField(exist = false)
    private Integer fuwufei;

    @TableField(exist = false)
    private String shuoming;

    @TableField(exist = false)
    private Integer isHn;

    @TableField(exist = false)
    private Integer isopen;

    @TableField(exist = false)
    private String fwdcount;


}