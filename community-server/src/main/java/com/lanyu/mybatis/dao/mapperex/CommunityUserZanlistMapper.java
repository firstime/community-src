package com.lanyu.mybatis.dao.mapperex;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.community.api.beans.ZanlistParams;
import com.lanyu.mybatis.dao.entity.CommunityUserDynamics;
import com.lanyu.mybatis.dao.entity.CommunityUserZanlist;
import com.lanyu.mybatis.dao.entity.SysLog;

/**
 * <p>
 * 点赞表 Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-26
 */
public interface CommunityUserZanlistMapper extends BaseMapper<CommunityUserZanlist> {
	
	List<ZanlistParams> getzan_list(Page<ZanlistParams> page,@Param("did") String did);
	
	@Update("update community_user_dynamics set d_zancount = d_zancount+1 where did = #{did}")
	int upzan(String did);
	
	
}
