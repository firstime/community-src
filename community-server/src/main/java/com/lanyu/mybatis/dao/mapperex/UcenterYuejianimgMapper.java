package com.lanyu.mybatis.dao.mapperex;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.UcenterYuejian;
import com.lanyu.mybatis.dao.entity.UcenterYuejianimg;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface UcenterYuejianimgMapper extends BaseMapper<UcenterYuejianimg>{

}