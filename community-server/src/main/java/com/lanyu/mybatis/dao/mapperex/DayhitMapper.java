package com.lanyu.mybatis.dao.mapperex;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanyu.mybatis.dao.entity.CommunityUserBeitai;
import com.lanyu.mybatis.dao.entity.Dayhit;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 备胎表 Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-26
 */
public interface DayhitMapper extends BaseMapper<Dayhit> {



	// @Select("SELECT CONCAT(username,' ',level1,' ',level2,' ',level3,' ',level4,' ',level5)  from ucenter_beitai")
	//List<String> queryBeitais();

	 @Select("SELECT SUM(cnt) count,to_user from day_hit where from_user=#{fromuser} GROUP BY from_user,to_user ORDER BY count desc LIMIT 5")
	List<CommunityUserBeitai> getPaiming(String fromuser);

	 //insert into ucenter_beitai set level1='112',level2='1',level3='1',level4='1',level5='1',username='张三' ON DUPLICATE KEY UPDATE  level1='12',level2='1',level3='1',level4='1',level5='1',username='张三'
	// @Insert("insert into ucenter_beitai set level1=#{level1},level2=#{level2},level3=#{level3},level4=#{level4},level5=#{level5},username='#{fromuser}' ON DUPLICATE KEY UPDATE  level1=#{level1},level2=#{level2},level3=#{level3},level4=#{level4},level5=#{level5},username='#{fromuser}'")
	//int insertBertais(String level1, String level2, String level3, String level4, String level5, String fromuser);

	//@Update("UPDATE ucenter_beitai set level1=#{level1},level2=#{level2},level3=#{level3},level4=#{level4},level5=#{level5} where username='#{fromuser}'")
	//int updateBertais(String level1, String level2, String level3, String level4, String level5);


	@Insert("INSERT INTO day_hit(day,slot,cnt,from_user,to_user) VALUES(CURRENT_DATE(),RAND() * 100 ,1,#{fromuser},#{touser}) ON DUPLICATE key UPDATE cnt = cnt + 1")
	int insertDayhit(String fromuser, String touser);



}
