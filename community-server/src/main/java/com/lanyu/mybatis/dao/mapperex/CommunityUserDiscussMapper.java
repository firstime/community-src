package com.lanyu.mybatis.dao.mapperex;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.CommunityUserDiscuss;
import com.lanyu.mybatis.dao.entity.CommunityUserFollows;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论表 Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-26
 */
public interface CommunityUserDiscussMapper extends BaseMapper<CommunityUserDiscuss> {

	//获取评论列表
	List<CommunityUserDiscuss> queryCommuntiyUserDiscussByParams(Page<CommunityUserDiscuss> page, Map<String, Object> m);

	@Select("SELECT b.username,b.nickname as usrNickname,b.head as userHead,(select CONCAT(username,'-',nickname,'-',head)  from ucenter_userinfo where uid=a.disc_at_uid) as atUsers,a.* FROM community_user_discuss a,ucenter_userinfo b where a.disc_uid=b.uid and a.disc_huifu_discid =#{m.discid} ORDER BY a.disc_creatime DESC")
	List<CommunityUserDiscuss> queryCommuntiyUserDiscussByParamsHuifu(Page<CommunityUserDiscuss> page, Map<String,Object> m);

}
