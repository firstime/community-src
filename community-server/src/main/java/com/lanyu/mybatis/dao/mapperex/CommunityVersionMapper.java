package com.lanyu.mybatis.dao.mapperex;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.CommunityUserFollows;
import com.lanyu.mybatis.dao.entity.CommunityVersion;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 更新版本表 Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-26
 */
public interface CommunityVersionMapper extends BaseMapper<CommunityVersion> {

	@Select("select * from community_version order by version_time desc limit 5")
	List<CommunityVersion> checkVersion();
}
