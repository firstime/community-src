package com.lanyu.mybatis.dao.mapperex;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.CommunityUserChat;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 聊天表 Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-26
 */
public interface CommunityUserChatMapper extends BaseMapper<CommunityUserChat> {

	//获取用户聊天明细列表
	List<CommunityUserChat> queryCommuntiyUserChatByParams(Page<CommunityUserChat> page, Map<String, Object> m);

	//获取用户聊天人员列表
	List<CommunityUserChat> queryCommuntiyUserChatPersonByParams(Page<CommunityUserChat> page, Map<String, Object> m);


}
