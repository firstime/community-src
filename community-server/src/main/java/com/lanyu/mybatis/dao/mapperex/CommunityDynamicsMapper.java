package com.lanyu.mybatis.dao.mapperex;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.community.api.beans.ZanlistParams;
import com.lanyu.mybatis.dao.entity.CommunityUserDynamics;
import com.lanyu.mybatis.dao.entity.SysLog;

/**
 * <p>
 * 动态表 Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-26
 */
public interface CommunityDynamicsMapper extends BaseMapper<CommunityUserDynamics> {
	


	 //@Select("SELECT * FROM community_user_dynamics WHERE DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= (d_posttime) ORDER BY (d_posttime) DESC")
	List<CommunityUserDynamics> queryCommuntiyListByParams(Page<CommunityUserDynamics> page, Map<String,Object> m);
	List<CommunityUserDynamics> queryCommuntiyListByWeek(Page<CommunityUserDynamics> page, Map<String,Object> m);
	List<CommunityUserDynamics> queryCommuntiyListByGuanzhuWeek(Page<CommunityUserDynamics> page, Map<String,Object> m);

	@Update("update community_user_dynamics set d_discusscount = d_discusscount+1 where did = #{did}")
	int updiscount(String did);


}
