package com.lanyu.mybatis.dao.mapperex;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.UcenterGuanxi;
import com.lanyu.mybatis.dao.entity.UcenterYuejian;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface UcenterGuanxiMapper extends BaseMapper<UcenterGuanxi>{

}