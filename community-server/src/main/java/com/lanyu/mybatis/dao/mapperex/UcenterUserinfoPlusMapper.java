package com.lanyu.mybatis.dao.mapperex;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.lanyu.mybatis.dao.entity.CommunityUserDynamics;
import com.lanyu.mybatis.dao.entity.UcenterGuanxi;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;
import org.apache.ibatis.annotations.Select;

public interface UcenterUserinfoPlusMapper  extends BaseMapper<UcenterUserinfo>{
	
	/**
	  * 通过坐标获取用户
	 * @param 
	 * @return
	 */
    UcenterUserinfo getUserByLatLng(@Param("lat") BigDecimal lat, @Param("lng") BigDecimal lng);
	
	/**
	  * 通过用户账号查询用户信息
	 * @param username
	 * @return
	 */
    UcenterUserinfo getUserByName(@Param("username") String username);

	/**
	 *  根据部门Id查询用户信息
	 * @param page
	 * @param departId
	 * @return
	 */
	IPage<UcenterUserinfo> getUserByDepId(Page page, @Param("departId") String departId, @Param("username") String username);

	/**
	 * 根据角色Id查询用户信息
	 * @param page
	 * @param
	 * @return
	 */
	IPage<UcenterUserinfo> getUserByRoleId(Page page, @Param("roleId") String roleId, @Param("username") String username);
	
	/**
	 * 根据用户名设置部门ID
	 * @param username
	 * @param departId
	 */
	void updateUserDepart(@Param("username") String username,@Param("orgCode") String orgCode);
	
	/**
	 * 根据手机号查询用户信息
	 * @param phone
	 * @return
	 */
    UcenterUserinfo getUserByPhone(@Param("phone") String phone);
	
	
	/**
	 * 根据邮箱查询用户信息
	 * @param email
	 * @return
	 */
    UcenterUserinfo getUserByEmail(@Param("email") String email);


	List<UcenterUserinfo> getUserByTuijian(Page<UcenterUserinfo> page, Map<String,Object> m);

	//查询红娘信息列表
	@Select("SELECT a.*,b.biaoyu,b.fuwushouru,b.fuwuhuiyuan,b.fuwufei,(\n" +
			"    SELECT count(*) from (\n" +
			"    SELECT\n" +
			"    (SELECT e.hn_uid  from ucenter_guanxi e,ucenter_hongniang r,ucenter_userinfo h where h.uid=e.hn_uid and e.hn_uid=r.hn_uid and e.uid=a.jiauid) as jiahn_uid\n" +
			"    ,(SELECT e.hn_uid  from ucenter_guanxi e,ucenter_hongniang r,ucenter_userinfo h where h.uid=e.hn_uid and e.hn_uid=r.hn_uid and e.uid=a.yiuid) as yihn_uid\n" +
			"    from ucenter_yuejian a WHERE a.jiedanok=1) as z\n" +
			"    where jiahn_uid=b.hn_uid or yihn_uid=b.hn_uid \n" +
			") as fwdcount FROM ucenter_userinfo a,ucenter_hongniang b WHERE b.isopen=1 and b.disable=0 and a.uid=b.hn_uid order by b.creatime desc")
	List<UcenterUserinfo> queryHongniangListByParams(Page<UcenterUserinfo> page, Map<String,Object> m);

	//查询红娘信息
	@Select("SELECT a.*,b.biaoyu,b.fuwushouru,b.fuwuhuiyuan,b.hnmobile,b.shuoming,b.fuwufei,b.isopen FROM ucenter_userinfo a,ucenter_hongniang b WHERE a.uid=b.hn_uid and a.uid=#{uid}")
	UcenterUserinfo queryHongniangByParams(@Param("uid") String uid);

	//查询亲属信息
	@Select("SELECT \n" +
			"z.fu_uid,z.mu_uid,z.hpy_uid,hn_uid,\n" +
			"(SELECT head from ucenter_userinfo where uid=z.fu_uid) fuhead,\n" +
			"(SELECT head from ucenter_userinfo where uid=z.mu_uid) muhead,\n" +
			"(SELECT head from ucenter_userinfo where uid=z.hpy_uid) hpyhead,\n" +
			"(SELECT head from ucenter_userinfo where uid=z.hn_uid) hnhead\n" +
			"from ucenter_guanxi z where z.uid=#{uid}")
	UcenterGuanxi queryQianshuByParams(@Param("uid") String uid);

	//@Insert("insert into ucenter_guanxi set uid=,hn_uid=")
	//int saveHnGuanxi(@Param("uid") String orguid,@Param("uid") String shouhuuid);

	//收藏的红娘列表
	@Select("SELECT a.*,b.biaoyu,b.fuwushouru,b.fuwuhuiyuan,b.fuwufei,(\n" +
			"    SELECT count(*) from (\n" +
			"    SELECT\n" +
			"    (SELECT e.hn_uid  from ucenter_guanxi e,ucenter_hongniang r,ucenter_userinfo h where h.uid=e.hn_uid and e.hn_uid=r.hn_uid and e.uid=a.jiauid) as jiahn_uid\n" +
			"    ,(SELECT e.hn_uid  from ucenter_guanxi e,ucenter_hongniang r,ucenter_userinfo h where h.uid=e.hn_uid and e.hn_uid=r.hn_uid and e.uid=a.yiuid) as yihn_uid\n" +
			"    from ucenter_yuejian a WHERE a.jiedanok=1) as z\n" +
			"    where jiahn_uid=b.hn_uid or yihn_uid=b.hn_uid \n" +
			") as fwdcount FROM ucenter_userinfo a,ucenter_shoucanghn c,ucenter_hongniang b WHERE b.disable=0 and a.uid=b.hn_uid and c.hnuid=a.uid and c.myuid=#{m.uid} order by c.shoucangtime desc")
	List<UcenterUserinfo> queryShoucangHongniangList(Page<UcenterUserinfo> page, Map<String,Object> m);

	//查询红娘守护的会员
	@Select("SELECT a.* FROM ucenter_userinfo a ,ucenter_guanxi b WHERE a.uid=b.uid and b.hn_uid=#{m.uid} order by b.gx_time desc")
	List<UcenterUserinfo> queryShouhuHongniangList(Page<UcenterUserinfo> page, Map<String,Object> m);

	//查询红娘ALL守护的会员
	@Select("SELECT a.* FROM ucenter_userinfo a ,ucenter_guanxi_hj b WHERE a.uid=b.uid and b.hn_uid=#{m.uid} order by b.gx_time desc")
	List<UcenterUserinfo> queryShouhuHongniangAllList(Page<UcenterUserinfo> page, Map<String,Object> m);

}