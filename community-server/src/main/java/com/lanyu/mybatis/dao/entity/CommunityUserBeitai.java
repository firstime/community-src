package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;

@Data
@TableName("ucenter_beitai")
public class CommunityUserBeitai implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField("username")
    @JsonProperty("username")
    private String username;

    @TableField("level1")
    @JsonProperty("level1")
    private String level1;

    @TableField("level2")
    @JsonProperty("level2")
    private String level2;

    @TableField("level3")
    @JsonProperty("level3")
    private String level3;

    @TableField("level4")
    @JsonProperty("level4")
    private String level4;

    @TableField("level5")
    @JsonProperty("level5")
    private String level5;

    @TableField(exist = false)
    private Integer count;

    @TableField(exist = false)
    @JsonProperty("to_user")
    private String toUser;


    @Override
    public String toString() {
        return username+" "+level1+" "+level2+" "+level3+" "+level4+" "+level5;
    }
}