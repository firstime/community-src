package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName("community_user_zanlist")
//@TableComment("点赞表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class CommunityUserZanlist implements Serializable{
	
	private static final long serialVersionUID = 1L;

    @TableField("did")
    //@Column(length = 100)
    //@ColumnComment("动态ID")
    @JsonProperty("did")
    private String did;

    @TableField("zan_uid")
    //@Column(length = 100)
    //@ColumnComment("点赞用户ID")
    @JsonProperty("zanUid")
    private String zanUid;

    @TableField("create_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("点赞时间")
    @JsonProperty("createTime")
    private Date createTime;

}