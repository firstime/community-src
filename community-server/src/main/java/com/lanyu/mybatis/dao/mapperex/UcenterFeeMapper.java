package com.lanyu.mybatis.dao.mapperex;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanyu.mybatis.dao.entity.UcenterFee;
import com.lanyu.mybatis.dao.entity.UcenterGuanxi;

public interface UcenterFeeMapper extends BaseMapper<UcenterFee>{

}