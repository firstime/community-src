package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName("ucenter_user_oplog")
//@TableComment("用户日志表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class UcenterUserOplog implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField("uid")
    //@Column(length = 100)
    //@ColumnComment("用户ID")
	private String uid;

    @TableField("op_no")
    //@Column(length = 100)
    //@ColumnComment("操作流水号")
    @JsonProperty("opNo")
    private String opNo;

    @TableField("op_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("操作时间")
    @JsonProperty("opTime")
    private Date opTime;

    @TableField("op_type")
    //@Column(length = 11)
    //@ColumnComment("操作类型")
    @JsonProperty("opType")
    private Integer opType;

    @TableField("op_dec")
    //@Column(length = 500)
    //@ColumnComment("操作描述")
    @JsonProperty("opDec")
    private String opDec;


}