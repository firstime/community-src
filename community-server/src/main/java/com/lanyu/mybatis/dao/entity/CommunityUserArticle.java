package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("community_user_article")
//@TableComment("文章表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class CommunityUserArticle implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField("pid")
    //@Column(length = 100)
    //@ColumnComment("文章ID")
	private String pid;

    @TableField("uid")
    //@Column(length = 100)
    //@ColumnComment("用户ID")
    private String uid;

    @TableField("p_title")
    //@Column(length = 100)
    //@ColumnComment("发布的标题")
    @JsonProperty("pTitle")
    private String pTitle;

    @TableField("p_message")
    //@Column(length = 1000)
    //@ColumnComment("发布的内容")
    @JsonProperty("pMessage")
    private String pMessage;

    @TableField("p_tags")
    //@Column(length = 100)
    //@ColumnComment("发布的标签")
    @JsonProperty("pTags")
    private String pTags;

    @TableField("p_send_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("创建时间")
    @JsonProperty("pSendtime")
    private Date pSendtime;

    @TableField("p_type")
    //@Column(length = 2)
    //@ColumnComment("发布的类型")
    @JsonProperty("pType")
    private Integer pType;

    @TableField("p_isdel")
    //@Column(length = 2)
    //@ColumnComment("是否删除")
    @JsonProperty("pIsdel")
    private Integer pIsdel;

    @TableField("p_hotscore")
    //@Column(length = 10)
    //@ColumnComment("热度分值")
    @JsonProperty("pHotscore")
    private Integer pHotscore;

    @TableField("p_topscore")
    //@Column(length = 10)
    //@ColumnComment("排名分值")
    @JsonProperty("pTopscore")
    private Integer pTopscore;

}