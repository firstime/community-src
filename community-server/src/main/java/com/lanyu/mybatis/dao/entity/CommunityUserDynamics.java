package com.lanyu.mybatis.dao.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@TableName("community_user_dynamics")
//@TableComment("动态表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
//序列化，是为了用springboot 缓存redis。他是JSON，会导致大小写问题，所以要加上@JsonProperty("dMessage")
public class CommunityUserDynamics implements Serializable {
	
	private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    @JsonProperty("usrNickname")
    private String usrNickname;

    @TableField(exist = false)
    @JsonProperty("userHead")
    private String userHead;


	@TableId(type = IdType.UUID)
    @TableField("did")
    //@Column(length = 100)
    //@ColumnComment("动态ID")
    private String did;

    @TableField("uid")
    //@Column(length = 100)
    //@ColumnComment("用户ID")
    private String uid;

    @TableField("d_message")
    //@Column(length = 1000)
    //@ColumnComment("动态内容")
    @JsonProperty("dMessage")
    private String dMessage;

    @TableField("d_url")
    //@Column(length = 500)
    //@ColumnComment("外部资源")
    @JsonProperty("dUrl")
    private String dUrl;

    @TableField("d_posttime")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("发布时间")
    @JsonProperty("dPosttime")
    private Date dPosttime;

    @TableField("d_title")
    //@Column(length = 200)
    //@ColumnComment("动态标题")
    @JsonProperty("dTitle")
    private String dTitle;

    @TableField("d_type")
    //@Column(length = 2)
    //@ColumnComment("动态类型")
    @JsonProperty("dType")
    private Integer dType;

    @TableField("d_isdel")
    //@Column(length = 2)
    //@ColumnComment("是否删除")
    @JsonProperty("dIsdel")
    private Integer dIsdel;

    @TableField("d_hotscore")
    //@Column(length = 11)
    //@ColumnComment("动态积分")
    @JsonProperty("dHotscore")
    private Integer dHotscore;

    @TableField("d_zancount")
    //@Column(length = 11)
    //@ColumnComment("点赞数")
    @JsonProperty("dZancount")
    private Integer dZancount;

    @TableField("d_discusscount")
    //@Column(length = 11)
    //@ColumnComment("评论数")
    @JsonProperty("dDiscusscount")
    private Integer dDiscusscount;

    @TableField("location")
    //@Column(length = 100)
    //@ColumnComment("位置")
    @JsonProperty("location")
    private String location;


    @TableField("d_hiden")
    @JsonProperty("dHiden")
    private Integer dHiden;
}