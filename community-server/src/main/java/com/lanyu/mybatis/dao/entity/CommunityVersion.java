package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: suibin
 * @Date: 2021/09/28/23:40
 * @Description:
 */

@Data
@TableName("community_version")
//@TableComment("更新版本表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class CommunityVersion implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableField("version_code")
    @JsonProperty("versionCode")
    private String versionCode;

    @TableField("version_name")
    @JsonProperty("versionName")
    private String versionName;

    @TableField("version_platform")
    @JsonProperty("versionPlatform")
    private String versionPlatform;

    @TableField("version_app")
    @JsonProperty("versionApp")
    private String versionApp;

    @TableField("version_time")
    @JsonProperty("versionTime")
    private Date versionTime;

}