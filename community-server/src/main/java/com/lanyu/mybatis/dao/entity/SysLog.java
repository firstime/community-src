package com.lanyu.mybatis.dao.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@TableName("sys_log")
//@TableComment("日志表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class SysLog implements Serializable {
    private static final long serialVersionUID = 1L;
	@TableId(type = IdType.UUID)
    @TableField("id")
    //@Column(length = 100)
    //@ColumnComment("系统日志ID")
    private String id;

    @TableField("log_type")
    //@Column(length = 2)
    //@ColumnComment("日志类型")
    @JsonProperty("logType")
    private Integer logType;

    @TableField("log_content")
    //@Column(length = 500)
    //@ColumnComment("日志内容")
    @JsonProperty("logContent")
    private String logContent;

    @TableField("operate_type")
    //@Column(length = 2)
    //@ColumnComment("操作类型")
    @JsonProperty("operateType")
    private Integer operateType;

    @TableField("userid")
    //@Column(length = 100)
    //@ColumnComment("用户ID")
    @JsonProperty("userid")
    private String userid;

    @TableField("username")
    //@Column(length = 100)
    //@ColumnComment("用户账号")
    @JsonProperty("username")
    private String username;

    @TableField("ip")
    //@Column(length = 100)
    //@ColumnComment("用户IP")
    @JsonProperty("ip")
    private String ip;

    @TableField("method")
    //@Column(length = 100)
    //@ColumnComment("用户行为")
    @JsonProperty("method")
    private String method;

    @TableField("request_url")
    //@Column(length = 500)
    //@ColumnComment("用户行为连接")
    @JsonProperty("requestUrl")
    private String requestUrl;

    @TableField("request_type")
    //@Column(length = 100)
    //@ColumnComment("用户行为类型")
    @JsonProperty("requestType")
    private String requestType;

    @TableField("cost_time")
    //@Column(length = 11)
    //@ColumnComment("行为耗时")
    @JsonProperty("costTime")
    private Long costTime;

    @TableField("create_by")
    //@Column(length = 100)
    //@ColumnComment("创建人")
    @JsonProperty("createBy")
    private String createBy;

    @TableField("create_time")
    //@ColumnType(value = MySqlTypeConstant.TIMESTAMP,length = 3)
    //@Column(type = MySqlTypeConstant.TIMESTAMP,length = 3)
    //@ColumnComment("创建时间,毫秒级")
    @JsonProperty("createTime")
    private Date createTime;

    @TableField("update_by")
    //@Column(length = 100)
    //@ColumnComment("变更人")
    @JsonProperty("updateBy")
    private String updateBy;

    @TableField("update_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("用户ID")
    @JsonProperty("updateTime")
    private Date updateTime;

    @TableField("request_param")
    //@Column(length = 100)
    //@ColumnComment("请求参数")
    @JsonProperty("requestParam")
    private String requestParam;


}