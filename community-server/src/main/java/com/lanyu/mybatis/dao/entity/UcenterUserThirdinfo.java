package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("ucenter_user_thirdinfo")
//@TableComment("用户三方信息表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class UcenterUserThirdinfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField("uid")
    //@Column(length = 100)
    //@ColumnComment("用户ID")
	private String uid;

    @TableField("wx_id")
    //@Column(length = 100)
    //@ColumnComment("微信ID")
    @JsonProperty("wxId")
    private String wxId;

    @TableField("qq_id")
    //@Column(length = 100)
    //@ColumnComment("QQID")
    @JsonProperty("qqId")
    private String qqId;


}