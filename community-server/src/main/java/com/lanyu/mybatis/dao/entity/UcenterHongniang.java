package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;

@Data
@TableName("ucenter_hongniang")
//@TableComment("用户表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class UcenterHongniang implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableField("hn_uid")
    @JsonProperty("hnuid")
    private String hnuid;


    @TableField("toupiao_uid")
    @JsonProperty("toupiaouid")
    private Integer toupiaouid;

    @TableField("creatime")
    private Date creatime;

    @TableField("fuwuhuiyuan")
    private String fuwuhuiyuan;

    @TableField("fuwushouru")
    private String fuwushouru;

    @TableField("hnmobile")
    private String hnmobile;

    @TableField("shuoming")
    private String shuoming;

    @TableField("biaoyu")
    private String biaoyu;

    @TableField("fuwufei")
    private Integer fuwufei;

    @TableField("isopen")
    private Integer isopen;

    @TableField("disable")
    private Integer disable;

    
}