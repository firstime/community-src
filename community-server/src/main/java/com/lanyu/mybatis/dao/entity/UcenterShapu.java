package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;

@Data
@TableName("ucenter_shapu")
//@TableComment("用户表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class UcenterShapu implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableField("from_uid")
    @JsonProperty("fromUid")
    private String fromUid;

    @TableField("to_uid")
    @JsonProperty("toUid")
    private String toUid;

    @TableField("nickname1")
    @JsonProperty("nickname1")
    private String nickname1;

    @TableField("nickname2")
    @JsonProperty("nickname2")
    private String nickname2;

    @TableField("head1")
    @JsonProperty("head1")
    private String head1;

    @TableField("head2")
    @JsonProperty("head2")
    private String head2;

    @TableField("info1")
    @JsonProperty("info1")
    private String info1;

    @TableField("info2")
    @JsonProperty("info2")
    private String info2;
    
}