package com.lanyu.mybatis.dao.mapperex;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanyu.mybatis.dao.entity.CommunityVersion;
import com.lanyu.mybatis.dao.entity.UcenterFee;
import com.lanyu.mybatis.dao.entity.UcenterShapu;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UcenterShapuMapper extends BaseMapper<UcenterShapu>{

    //清空指定表
    @Update("truncate table ucenter_shapu")
    void deleteUserShapuTemp();

    @Select("select from_uid,to_uid, \n" +
            "(select nickname from ucenter_userinfo where uid=a.from_uid) nickname1,\n" +
            "(select nickname from ucenter_userinfo where uid=a.to_uid) nickname2,\n" +
            "(select head from ucenter_userinfo where uid=a.from_uid) head1,\n" +
            "(select head from ucenter_userinfo where uid=a.to_uid) head2,\n" +
            "(select CONCAT(if(sex=1,'男','女'),' ',age,'岁') from ucenter_userinfo where uid=a.from_uid) info1,\n" +
            "(select CONCAT(if(sex=1,'男','女'),' ',age,'岁') from ucenter_userinfo where uid=a.to_uid) info2 \n" +
            "from ucenter_shapu a where a.from_uid=#{fromUid} or a.to_uid=#{fromUid} GROUP BY nickname1 HAVING nickname1 is not null")
    List<UcenterShapu> queryShapuList(String fromUid);


}