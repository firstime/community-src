package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;

@Data
@TableName("day_hit")
public class Dayhit implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField("from_user")
    @JsonProperty("fromUser")
    private String fromUser;

    @TableField("to_user")
    @JsonProperty("toUser")
    private String toUser;

    @TableField("day")
    @JsonProperty("day")
    private Date day;

    @TableField("slot")
    @JsonProperty("slot")
    private String slot;

    @TableField("cnt")
    @JsonProperty("cnt")
    private Integer cnt;

}