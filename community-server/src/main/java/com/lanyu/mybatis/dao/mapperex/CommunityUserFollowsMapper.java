package com.lanyu.mybatis.dao.mapperex;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.community.api.beans.ZanlistParams;
import com.lanyu.mybatis.dao.entity.CommunityUserDynamics;
import com.lanyu.mybatis.dao.entity.CommunityUserFollows;
import com.lanyu.mybatis.dao.entity.SysLog;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;

/**
 * <p>
 * 动态关注表 Mapper 接口
 * </p>
 *
 * @Author suibin
 * @since 2018-12-26
 */
public interface CommunityUserFollowsMapper extends BaseMapper<CommunityUserFollows> {
	
	//获取粉丝用户列表
	List<UcenterUserinfo> queryCommuntiyUserFollowsByParams(Page<UcenterUserinfo> page, Map<String,Object> m);


}
