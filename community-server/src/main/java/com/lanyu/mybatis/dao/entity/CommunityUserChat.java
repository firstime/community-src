package com.lanyu.mybatis.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
//import com.gitee.sunchenbin.mybatis.actable.annotation.*;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlCharsetConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlEngineConstant;
//import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName("community_user_chat")
//@TableComment("聊天表")
//@TableCharset(MySqlCharsetConstant.UTF8MB4)
//@TableEngine(MySqlEngineConstant.InnoDB)
public class CommunityUserChat implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
    @TableField("chat_id")
    //@Column(length = 100)
    //@ColumnComment("聊天ID")
    @JsonProperty("chatId")
    private String chatId;

    @TableField(exist = false)
    private String fromname;

    @TableField(exist = false)
    private String toname;

    @TableField(exist = false)
    @JsonProperty("fromnameHead")
    private String fromnameHead;

    @TableField(exist = false)
    @JsonProperty("tonameHead")
    private String tonameHead;

    @TableField("from_uid")
    //@Column(length = 100)
    //@ColumnComment("来自用户ID")
    @JsonProperty("fromUid")
    private String fromUid;

    @TableField("to_uid")
    //@Column(length = 100)
    //@ColumnComment("发给用户ID")
    @JsonProperty("toUid")
    private String toUid;

    @TableField("message")
    //@Column(length = 1000)
    //@ColumnComment("聊天内容")
    private String message;

    @TableField("create_time")
    //@ColumnType(MySqlTypeConstant.TIMESTAMP)
    //@ColumnComment("创建时间")
    @JsonProperty("createTime")
    private Date createTime;

    @TableField("type")
    //@Column(length = 2)
    //@ColumnComment("信息类型")
    private Integer type;

}