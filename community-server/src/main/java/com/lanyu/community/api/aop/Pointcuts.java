package com.lanyu.community.api.aop;

import org.aspectj.lang.annotation.Pointcut;

public class Pointcuts {
    //
    @Pointcut("execution(public * com.lanyu.community.api.controller.*.*(..))")
    public void webLog() {
    }

}
