package com.lanyu.community.api.service;

import com.aliyun.tea.*;
import com.aliyun.dysmsapi20170525.*;
import com.aliyun.dysmsapi20170525.models.*;
import com.aliyun.teaopenapi.*;
import com.aliyun.teaopenapi.models.*;
import com.lanyu.community.api.service.galeshapley.StableMatching;
import com.lanyu.mybatis.dao.entity.CommunityUserBeitai;
import com.lanyu.mybatis.dao.entity.UcenterShapu;
import com.lanyu.mybatis.dao.mapperex.CommunityUserBeitaiMapper;
import com.lanyu.mybatis.dao.mapperex.UcenterShapuMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Max;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: suibin
 * @Date: 2021/12/01/9:46
 * @Description:
 */
@Component
public class MsgTask {
    @Autowired
    CommunityUserBeitaiMapper communityUserBeitaiMapper;
    @Autowired
    UcenterShapuMapper ucenterShapuMapper;

    private Logger logger = LoggerFactory.getLogger(getClass());
    private int i;

    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }


    //@Scheduled(cron = "*/15 * * * * ?")
    public void execute() throws Exception {
        //java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.dysmsapi20170525.Client client = createClient("LTAIY3zcWYxFbZ3r", "lq1vqBNdk1W1gnSiK7gxwr7v7oYFwC");
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName("昊弘科技")
                .setTemplateCode("SMS_130990254")
                .setPhoneNumbers("13426159617");
        // 复制代码运行请自行打印 API 的返回值
        client.sendSms(sendSmsRequest);
        logger.info("thread id:{},FixedPrintTask execute times:{}", Thread.currentThread().getId(), ++i);
    }

    @Transactional
    //@Scheduled(cron = "*/15 * * * * ?")
    @Scheduled(cron = "0 0 1 * * ?")
    public void manWomenPipei() throws Exception {
        //java.util.List<String> args = java.util.Arrays.asList(args_);


        List<String> c=communityUserBeitaiMapper.queryBeitais();
        StringBuffer  st = new StringBuffer();
        for (int x=0;x<c.size();x++){
            st.append(c.get(x)+"\n");
        }

        Map<String, String> matches=new StableMatching().StableMatching("",st.toString());
        ucenterShapuMapper.deleteUserShapuTemp();
        for(Map.Entry<String, String> matching:matches.entrySet()){
            UcenterShapu us=new UcenterShapu();
            us.setFromUid(matching.getKey());
            us.setToUid(matching.getValue());
            ucenterShapuMapper.insert(us);
            System.out.println(matching.getKey() + " " + matching.getValue());
        }
        logger.info("thread id:{},FixedPrintTask execute times:{}", Thread.currentThread().getId(), ++i);
    }
}
