package com.lanyu.community.api.service;


import java.util.List;
import java.util.Map;
import java.util.Set;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.community.api.beans.ZanlistParams;
import com.lanyu.community.api.util.*;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lanyu.mybatis.dao.entity.*;

/**
 * <p>
 * 动态 逻辑
 * </p>
 *
 * @Author suibin
 * @since 2018-12-20
 */
public interface ICommunityDynamicService extends IService<CommunityUserDynamics> {
	//点赞
    int upzan(String uid, String did);
	
	//添加关注
	
	int follow(String from_uid, String to_uid);

	//取消关注
	
	int unfollow(String from_uid, String to_uid);
	
	//删除动态
	
	int delDt(String uid, String did);

	//隐藏动态
	int hidenDt(String uid, String did,int hiden);

	//撤回聊天

	int delChat(String chat_id);

	//发布动态
    String publishDt(CommunityUserDynamics cud);

	//获取单个用户信息
    UcenterUserinfo getUserinfo(String uid);

	//获取关注用户信息列表
    Page<UcenterUserinfo> getFollowUserinfos(Page<UcenterUserinfo> page, String uid);

	//获取粉丝用户信息列表
    Page<UcenterUserinfo> getFensiUserinfos(Page<UcenterUserinfo> page, Map<String, Object> m);
	//获取点赞列表
    Page<ZanlistParams> getZanlist(Page<ZanlistParams> page, String did);
	
	//获取最近7天动态列表
    Page<CommunityUserDynamics> getCommunityDynamicsLatestweek(Page<CommunityUserDynamics> page, Map<String, Object> m);

	//获取最近7天关注的动态列表
    Page<CommunityUserDynamics> getCommunityGuanzhuDynamicsLatestweek(Page<CommunityUserDynamics> page, Map<String, Object> m);

	//发布聊天
    String publishChat(CommunityUserChat cuc);

	//我的聊天记录列表
    Page<CommunityUserChat> getCommunityChatList(Page<CommunityUserChat> page, Map<String, Object> m);

	//我的聊天人员列表
    Page<CommunityUserChat> getCommunityChatPersonList(Page<CommunityUserChat> page, Map<String, Object> m);

	//获取用户的动态列表
    Page<CommunityUserDynamics> getCommunityDynamicsByParams(Page<CommunityUserDynamics> page, Map<String, Object> m);


	//删除评论
    int delDiscuss(String discId, String uid,String did);

	//发布评论
    String addDiscuss(CommunityUserDiscuss cud);

	//获取评论列表
    Page<CommunityUserDiscuss> getDiscussList(Page<CommunityUserDiscuss> page, Map<String, Object> m);

	//获取评论回复列表
	Page<CommunityUserDiscuss> getDiscussListHuifu(Page<CommunityUserDiscuss> page, Map<String, Object> m);

    //用户推荐列表
    Page<UcenterUserinfo> getTuijianUserinfos(Page<UcenterUserinfo> page,Map<String, Object> m) ;

    //约见
	String yuejian(UcenterYuejian yuejian);

	//查询约见
	int checkYuejian(String a,String b,String time);
	//约见列表
	Page<UcenterYuejian> queryYuyue(Page<UcenterYuejian> page, Map<String,Object> m);

	Page<UcenterYuejian> queryYuyueFwd(Page<UcenterYuejian> page, Map<String,Object> m);

	Page<UcenterYuejian> queryHnFwd(Page<UcenterYuejian> page, Map<String,Object> m);


	int yuejianUpdateJia(String bianhao,int agree);
	int yuejianUpdateYi(String bianhao,int agree);
	//红娘列表
	Page<UcenterUserinfo> queryHongniangList(Page<UcenterUserinfo> page, Map<String,Object> m);

	Page<UcenterUserinfo> queryShoucangHongniangList(Page<UcenterUserinfo> page, Map<String,Object> m);

	Page<UcenterUserinfo> queryShouhuHongniangList(Page<UcenterUserinfo> page, Map<String,Object> m);

	Page<UcenterUserinfo> queryShouhuHongniangAllList(Page<UcenterUserinfo> page, Map<String,Object> m);

}
