package com.lanyu.community.api.service;


import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.community.api.util.*;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @Author suibin
 * @since 2018-12-20
 */
public interface ISysUserService extends IService<UcenterUserinfo> {
	
	UcenterUserinfo getUserByName(String username);
	
	/**
	 * 添加用户和用户角色关系
	 * @param user
	 * @param roles
	 */
    void addUserWithRole(UcenterUserinfo user, String roles);
	
	
	/**
	 * 修改用户和用户角色关系
	 * @param user
	 * @param roles
	 */
    void editUserWithRole(UcenterUserinfo user, String roles);

	/**
	 * 获取用户的授权角色
	 * @param username
	 * @return
	 */
    List<String> getRole(String username);
	
	/**
	  * 查询用户信息包括 部门信息
	 * @param username
	 * @return
	 */
	//public SysUserCacheInfo getCacheUser(String username);

	/**
	 * 根据部门Id查询
	 * @param
	 * @return
	 */
    IPage<UcenterUserinfo> getUserByDepId(Page<UcenterUserinfo> page, String departId, String username);

	/**
	 * 根据角色Id查询
	 * @param
	 * @return
	 */
    IPage<UcenterUserinfo> getUserByRoleId(Page<UcenterUserinfo> page, String roleId, String username);

	/**
	 * 通过用户名获取用户角色集合
	 *
	 * @param username 用户名
	 * @return 角色集合
	 */
	Set<String> getUserRolesSet(String username);

	/**
	 * 通过用户名获取用户权限集合
	 *
	 * @param username 用户名
	 * @return 权限集合
	 */
	Set<String> getUserPermissionsSet(String username);
	
	/**
	 * 根据用户名设置部门ID
	 * @param username
	 * @param orgCode
	 */
	void updateUserDepart(String username,String orgCode);
	
	/**
	 * 根据手机号获取用户名和密码
	 */
    UcenterUserinfo getUserByPhone(String phone);


	/**
	 * 根据邮箱获取用户
	 */
    UcenterUserinfo getUserByEmail(String email);


	/**
	 * 添加用户和用户部门关系
	 * @param user
	 * @param selectedParts
	 */
	void addUserWithDepart(UcenterUserinfo user, String selectedParts);

	/**
	 * 编辑用户和用户部门关系
	 * @param user
	 * @param departs
	 */
	void editUserWithDepart(UcenterUserinfo user, String departs);
	
	/**
	   * 校验用户是否有效
	 * @param sysUser
	 * @return
	 */
	Result checkUserIsEffective(UcenterUserinfo sysUser);

}
