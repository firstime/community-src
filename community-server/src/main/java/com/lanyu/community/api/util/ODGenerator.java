package com.lanyu.community.api.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.apache.commons.lang3.time.FastDateFormat;
import org.springframework.util.StopWatch;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 订单编号策略
 *
 * @project common-utils
 * @fileName ODGenerator.java
 * @Description
 * @author
 * @date
 * @version 1.0.0
 */
public abstract class ODGenerator {
    private static final FastDateFormat pattern = FastDateFormat.getInstance("yyyyMMddHHmmss");
    private static final AtomicInteger atomicInteger = new AtomicInteger(1);
    private static ThreadLocal<StringBuilder> threadLocal = new ThreadLocal<StringBuilder>();

    /**
     * 长码生成策略
     *  20201116114351753590384993
     * @param lock uuid
     * @return
     */
    public static String getC(String lock) {
        if (Objects.isNull(threadLocal.get())) {
            lock = Objects.isNull(lock) ? UUID.randomUUID().toString() : lock;
            StringBuilder builder = new StringBuilder(pattern.format(Instant.now().toEpochMilli()));// 取系统当前时间作为订单号前半部分
            builder.append(Math.abs(lock.hashCode()));// HASH-CODE
            builder.append(atomicInteger.getAndIncrement());// 自增顺序
            threadLocal.set(builder);
        }
        return threadLocal.get().toString();
    }

    /**
     * 短码生成策略
     * 1307891882965
     * @param lock
     * @return
     */
    public static String getD(String lock) {
        if (Objects.isNull(threadLocal.get())) {
            lock = Objects.isNull(lock) ? UUID.randomUUID().toString() : lock;
            StringBuilder builder = new StringBuilder(ThreadLocalRandom.current().nextInt(0, 999)); // 随机数
            builder.append(Math.abs(lock.hashCode()));// HASH-CODE
            builder.append(atomicInteger.getAndIncrement());// 自增顺序
            threadLocal.set(builder);
        }
        return threadLocal.get().toString();
    }
    public static void main234(String[] args) {
        //为了大并发重复，用同步数组
        List<String> orderNos = Collections.synchronizedList(new ArrayList<String>());
        //创建8000次循环
        IntStream.range(0,1008000).parallel().forEach(i->{
            orderNos.add(UUID.randomUUID().toString());//创建订单号，插入数组里
        });
        //流方式去重复
        List<String> filterOrderNos = orderNos.stream().distinct().collect(Collectors.toList());

        System.out.println("订单样例："+ orderNos.get(22));
        System.out.println("生成订单数："+orderNos.size());
        System.out.println("过滤重复后订单数："+filterOrderNos.size());
        System.out.println("重复订单数："+(orderNos.size()-filterOrderNos.size()));
    }
    /**
     * 1000个线程并发测试
     *
     * @param args
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Snowflake snowflake = IdUtil.createSnowflake(0, 1);
        List<String> orderNos = Collections.synchronizedList(new ArrayList<String>());
        Set<String> set = new HashSet<String>();
        FutureTask<String> task = null;
        StopWatch watchTime = new StopWatch();
        watchTime.start();
        for (int i = 0; i < 1000; i++) {
            Callable<String> callable = new Callable<String>() {
                @Override
                public String call() throws Exception {
                    // System.out.println("当前线程:>>>>> ".concat(Thread.currentThread().getId()+""));
                    //return getC(null);

                    IntStream.range(0,1008).parallel().forEach(i->{
                        //orderNos.add(UUID.randomUUID().toString());//UUID全局唯一ID，测试100万不重复
                        //orderNos.add(snowflake.nextIdStr()+"");//雪花算法全局唯一ID，测试100万不重复
                        //orderNos.add(new IdGeneratorSnowflake().snowflakeId()+"");//网上小白用的不对，网上很多小白讨论说雪花算法重复，是这样用的吧？不会封装就别封！
                        orderNos.add(IdGeneratorSnowflake.snowflakeIdStatic()+"");//工具类封装的雪花算法全局唯一ID，测试100万不重复
                        //orderNos.add(OrderGen2Test.generateOrderNo());//网上的XXX，全是ID重复的
                        //orderNos.add(getC(null));//网上短码生成ID，全是ID重复的
                    });

                    return null;
                    //return getD(null);
                }
            };
            task = new FutureTask<String>(callable);
            new Thread(task).start();
            set.add(task.get());
            System.out.println(task.get());
        }
        watchTime.stop();
        System.out.println(watchTime.getTotalTimeMillis());
        System.out.println(set.size());


        List<String> filterOrderNos = orderNos.stream().distinct().collect(Collectors.toList());

        System.out.println("订单样例："+ orderNos.get(22));
        System.out.println("生成订单数："+orderNos.size());
        System.out.println("过滤重复后订单数："+filterOrderNos.size());
        System.out.println("重复订单数："+(orderNos.size()-filterOrderNos.size()));
    }
}