package com.lanyu.community.api.service.impl;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lanyu.community.api.constat.CommonConstant;
import com.lanyu.community.api.service.ISysUserService;
import com.lanyu.community.api.util.Result;
import com.lanyu.mybatis.dao.entity.UcenterUserinfo;

import com.lanyu.mybatis.dao.mapperex.UcenterUserinfoPlusMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @Author: suibin
 * @Date: 2018-12-20
 */
@Service
@Slf4j
public class SysUserServiceImpl extends ServiceImpl<UcenterUserinfoPlusMapper, UcenterUserinfo> implements ISysUserService {
	@Override
	public boolean save(UcenterUserinfo entity) {
		// TODO Auto-generated method stub
		
		int insertcount=ucenterUserinfoPlusMapper.insert(entity);
		return insertcount > 0;
	}
	@Autowired
	private UcenterUserinfoPlusMapper ucenterUserinfoPlusMapper;
	@Override
	public boolean saveBatch(Collection<UcenterUserinfo> entityList, int batchSize) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveOrUpdateBatch(Collection<UcenterUserinfo> entityList, int batchSize) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(Wrapper<UcenterUserinfo> queryWrapper) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateById(UcenterUserinfo entity) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public boolean updateBatchById(Collection<UcenterUserinfo> entityList, int batchSize) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveOrUpdate(UcenterUserinfo entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public UcenterUserinfo getOne(Wrapper<UcenterUserinfo> queryWrapper, boolean throwEx) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getMap(Wrapper<UcenterUserinfo> queryWrapper) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <V> V getObj(Wrapper<UcenterUserinfo> queryWrapper, Function<? super Object, V> mapper) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count(Wrapper<UcenterUserinfo> queryWrapper) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<UcenterUserinfo> list(Wrapper<UcenterUserinfo> queryWrapper) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Map<String, Object>> listMaps(Wrapper<UcenterUserinfo> queryWrapper) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <V> List<V> listObjs(Wrapper<UcenterUserinfo> queryWrapper, Function<? super Object, V> mapper) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public UcenterUserinfo getUserByName(String username) {
		// TODO Auto-generated method stub
		return ucenterUserinfoPlusMapper.getUserByName(username);
		
	}

	@Override
	public void addUserWithRole(UcenterUserinfo user, String roles) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editUserWithRole(UcenterUserinfo user, String roles) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getRole(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPage<UcenterUserinfo> getUserByDepId(Page<UcenterUserinfo> page, String departId, String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPage<UcenterUserinfo> getUserByRoleId(Page<UcenterUserinfo> page, String roleId, String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> getUserRolesSet(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> getUserPermissionsSet(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateUserDepart(String username, String orgCode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UcenterUserinfo getUserByPhone(String phone) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UcenterUserinfo getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addUserWithDepart(UcenterUserinfo user, String selectedParts) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editUserWithDepart(UcenterUserinfo user, String departs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Result checkUserIsEffective(UcenterUserinfo sysUser) {
		Result<?> result = new Result<Object>();
		//情况1：根据用户信息查询，该用户不存在
		if (sysUser == null) {
			result.error500("该用户不存在，请注册");
			//sysBaseAPI.addLog("用户登录失败，用户不存在！", CommonConstant.LOG_TYPE_1, null);
			return result;
		}
		//情况2：根据用户信息查询，该用户已注销
		if (CommonConstant.USER_DEL.toString().equals(sysUser.getStatus())) {
			//sysBaseAPI.addLog("用户登录失败，用户名:" + sysUser.getUsername() + "已注销！", CommonConstant.LOG_TYPE_1, null);
			result.error500("该用户已注销");
			return result;
		}
		//情况3：根据用户信息查询，该用户已冻结
		if (CommonConstant.USER_FREEZE.equals(sysUser.getStatus())) {
			//sysBaseAPI.addLog("用户登录失败，用户名:" + sysUser.getUsername() + "已冻结！", CommonConstant.LOG_TYPE_1, null);
			result.error500("该用户已冻结");
			return result;
		}
		return result;
	}
	
}
