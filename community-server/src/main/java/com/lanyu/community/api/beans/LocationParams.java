package com.lanyu.community.api.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 接口参数基础类
 * </p>
 *
 * @Author suibin
 * @since 2018-12-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LocationParams implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 定位信息
     */
    private String location;

    private BigDecimal lat;

    private BigDecimal lng;


}
