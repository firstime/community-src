package com.lanyu.community.api.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.lanyu.community.api.config.WebSocket;
import com.lanyu.community.api.shiro.DefContants;
import com.lanyu.community.api.util.*;
import com.lanyu.mybatis.dao.entity.*;
import com.lanyu.mybatis.dao.mapperex.*;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanyu.community.api.beans.LocationParams;
import com.lanyu.community.api.beans.RequestPageSearchParams;
import com.lanyu.community.api.beans.ZanlistParams;
import com.lanyu.community.api.constat.CommonConstant;
import com.lanyu.community.api.entity.User;
import com.lanyu.community.api.service.ICommunityDynamicService;

import com.lanyu.community.api.shiro.JwtUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import redisson.lock.DistributedLocker;

/*
 * create by suibin
 * API-FEED
 * 2018-03-19
 */
@Api("API-FEED")
@RestController
@RequestMapping("feed")
public class FeedController {

	@Autowired
	UcenterShapuMapper ucenterShapuMapper;
	//@Autowired
	//IFeedService feedService;
	@Autowired
	UcenterUserinfoPlusMapper ucenterUserinfoPlusMapper;
	@Autowired
	CommunityUserBeitaiMapper communityUserBeitaiMapper;
	@Autowired
	DayhitMapper dayhitMapper;


	@Autowired
	CommunityDynamicsMapper communityDynamicsMapper;

	@Autowired
	ICommunityDynamicService CommunityDynamicService;

	@Resource
	private WebSocket webSocket;

	@Autowired
	@Lazy
	private RedisUtil redisUtil;

    @Autowired
    private DistributedLocker distributedLocker;//redis分布式锁，支付，防止金额不对。

	@Autowired
	UcenterHongniangMapper ucenterHongniangMapper;

	@Autowired
	UcenterYuejianMapper ucenterYuejianMapper;

	@Autowired
	com.lanyu.mybatis.dao.mapperex.UcenterShouCangHongniangMapper ucenterShouCangHongniangMapper;

	@Autowired
	private CommunityUserChatMapper communityUserChatMapper;

	@Autowired
	com.lanyu.mybatis.dao.mapperex.UcenterGuanxiMapper ucenterGuanxiMapper;
	@Autowired
	UcenterGuanxiHjMapper ucenterGuanxiHjMapper;
	@Autowired
	UcenterFeeMapper ucenterFeeMapper;
	@Autowired
	UcenterYuejianimgMapper ucenterYuejianimgMapper;
	/*
	@ApiOperation(value = "say", notes = "")
	@RequestMapping(value = "/saya", method = RequestMethod.POST)
	@ResponseBody
	public Result saya(@RequestParam("uid") String uid) throws Exception {
		Result<String> result = new Result<String>();

		result.setResult(Say.get());

		return result;

	}
*/

	//
	@Transactional
	@ApiOperation(value = "支付约见", notes = "")
	@RequestMapping(value = "/zhifuyuejian", method = RequestMethod.POST)
	@ResponseBody
	public Result zhifuyuejian(@RequestParam String bianhao,@RequestParam String jiauid,@RequestParam String yiuid) throws Exception {
		Result<String> result = new Result<String>();

		Wrapper<UcenterYuejian> wrapper11=new QueryWrapper<UcenterYuejian>()
				.eq("bianhao", bianhao).eq("zhifuok", 1).last("limit 1")
				;
		UcenterYuejian xxx=ucenterYuejianMapper.selectOne(wrapper11);
		if(xxx==null){


					//1同意，0不同意
					//乙方修改
					Wrapper<UcenterUserinfo> wrapper=new QueryWrapper<UcenterUserinfo>()
							.eq("uid", yiuid)
							;
					UcenterUserinfo uu=ucenterUserinfoPlusMapper.selectOne(wrapper);
					int yuejianfee=0;
					int yuejiansong=0;

					if(uu.getYuejianfee().equals("免费")){}
					if(uu.getYuejianfee().equals("收188恋币(送他150恋币)")){yuejianfee=188;yuejiansong=150;}
					if(uu.getYuejianfee().equals("收1000恋币(送他500恋币)")){yuejianfee=1000;yuejiansong=500;}
					if(uu.getYuejianfee().equals("收1888恋币(送他1000恋币)")){yuejianfee=1888;yuejiansong=1000;}
					if(uu.getYuejianfee().equals("收5888恋币(送他5888恋币)")){yuejianfee=5888;yuejiansong=5888;}


					//甲方扣款
					Wrapper<UcenterUserinfo> wrapper12=new QueryWrapper<UcenterUserinfo>()
							.eq("uid", jiauid)
							;
					UcenterUserinfo uu11=ucenterUserinfoPlusMapper.selectOne(wrapper12);
					if(uu11.getJifen()>=yuejianfee){

						uu.setJifen(uu.getJifen()+yuejiansong);
						ucenterUserinfoPlusMapper.update(uu,wrapper);

						uu11.setJifen(uu11.getJifen()-yuejianfee);
						ucenterUserinfoPlusMapper.update(uu11,wrapper12);


						Wrapper<UcenterYuejian> wrapper1=new QueryWrapper<UcenterYuejian>()
								.eq("bianhao", bianhao).last("limit 1")
								;
						UcenterYuejian cuclist=ucenterYuejianMapper.selectOne(wrapper1);
						cuclist.setZhifuok(1);
						ucenterYuejianMapper.update(cuclist,wrapper1);
						result.setResult("1");
					}else {
						//没钱了
						result.setResult("-1");
						return result;
					}
		}
		result.setResult("-2");//已支付
		return result;
	}




	@Transactional
	@ApiOperation(value = "同意约见", notes = "")
	@RequestMapping(value = "/yuejianAgree", method = RequestMethod.POST)
	@ResponseBody
	public Result yuejianAgree(@RequestParam String bianhao,@RequestParam int agree,@RequestParam int isJia,@RequestParam String jiauid,@RequestParam String yiuid) throws Exception {
		Result<String> result = new Result<String>();
		//1同意，0不同意

		if(isJia==1){
			result.setResult(CommunityDynamicService.yuejianUpdateJia(bianhao,agree)+"");
		}else if(isJia==2){
			result.setResult(CommunityDynamicService.yuejianUpdateYi(bianhao,agree)+"");
		}

		return result;
	}
	@Transactional
	@ApiOperation(value = "微信支付到账", notes = "")
	@RequestMapping(value = "/wxpayOk", method = RequestMethod.POST)
	@ResponseBody
	public Result wxpayOk(@RequestParam String jiami) throws Exception {
		Result<String> result = new Result<String>();
		result.setResult("1");
		//jiami
		//String keyWord = URLDecoder.decode(jiami, "utf-8");
		String decode = AESUtil.decrypt(jiami, "adddfe2153da7a1cf3bf26c92fb8c9bc");
		System.out.println("解密后: " + decode);
		JSONObject ucflist= (JSONObject) JSONObject.parse(decode);
		//List<UcenterFee> ucflist=JSONObject.parseArray(decode,UcenterFee.class);
		//ucflist.get(0).getUid();
		//ucflist.get(0).getFee();
		//ucflist.get(0).getOrderno();

		Wrapper<UcenterFee> wrapper12=new QueryWrapper<UcenterFee>()
				.eq("orderno", ucflist.getString("orderno")).eq("isok", 0).last("limit 1")
				;
		UcenterFee uf=ucenterFeeMapper.selectOne(wrapper12);
		if(uf!=null){//如果未支付，继续
			uf.setIsok(1);
			ucenterFeeMapper.update(uf,wrapper12);
		}else{
			//已支付，不存在订单，返回
			return result;
		}
		//更新金额
		Wrapper<UcenterUserinfo> wrapper11=new QueryWrapper<UcenterUserinfo>()
				.eq("uid", ucflist.getString("uid")).last("limit 1")
				;

		UcenterUserinfo uu=ucenterUserinfoPlusMapper.selectOne(wrapper11);
		uu.setJifen(uu.getJifen()+Integer.parseInt(uf.getFee())/100*10);
		ucenterUserinfoPlusMapper.update(uu,wrapper11);






		return result;

	}
	@ApiOperation(value = "微信支付签名", notes = "")
	@RequestMapping(value = "/wxQianming", method = RequestMethod.POST)
	@ResponseBody
	public Result<UcenterFee> wxQianming(@RequestParam String fee,@RequestParam String uid) throws Exception {
		Result<UcenterFee> result = new Result<UcenterFee>();
		String orderNo="no_"+ODGenerator.getD(null);


		String stringA="appid=wx809cda02fd46f32c" +
				"&body=恋恋相亲角支付" +
				"&device_info=WEB" +
				"&mch_id=1614575885" +
				"&nonce_str=111" +
				"&notify_url="+"http://suibin.online"+
				"&out_trade_no="+orderNo+
				"&total_fee="+fee+"" +
				"&trade_type=APP"
				;

		String stringSignTemp=stringA+"&key=11111111111111111111111111111111"; //注：key为商户平台设置的密钥key
		String sign=MD5Util.MD5Encode(stringSignTemp,"").toUpperCase(); //注：MD5签名方式
		System.out.println(stringSignTemp);
		String body ="<xml>"+
				"<appid>wx809cda02fd46f32c</appid>"+
				//"<attach>恋恋相亲角支付</attach>"+
				"<body>恋恋相亲角支付</body>"+
				"<device_info>WEB</device_info>"+
				"<mch_id>1614575885</mch_id>"+
				"<nonce_str>111</nonce_str>"+
				"<notify_url>http://suibin.online</notify_url>"+
				"<out_trade_no>"+orderNo+"</out_trade_no>"+
				"<total_fee>"+fee+"</total_fee>"+
				"<trade_type>APP</trade_type>"+
				//"<spbill_create_ip>47.52.57.16</spbill_create_ip>"+
				"<sign>"+sign+"</sign>"+
				"</xml>";

		Map<String, Object> mm=HttpUtil.xmlPost("https://api.mch.weixin.qq.com/pay/unifiedorder",body);



		//数据库记录订单
		UcenterFee ufee=new UcenterFee();
		ufee.setUid(uid);
		ufee.setFee(fee);
		ufee.setCreatime(new Date());
		ufee.setOrderno(orderNo);
		ufee.setIsok(0);
		ufee.setPrepayid(mm.get("prepay_id").toString());
		ucenterFeeMapper.insert(ufee);

		Map map = new HashMap();
		map.put("prepay_id",mm.get("prepay_id").toString());
		map.put("orderNo",orderNo);
		JSONObject jsonObject = new JSONObject(map);

		result.setResult(ufee);

		return result;

	}

	@ApiOperation(value = "约见确认", notes = "")
	@RequestMapping(value = "/yuejianQr", method = RequestMethod.POST)
	@ResponseBody
	public Result yuejianQr(@RequestParam String bianhao) throws Exception {
		Result<String> result = new Result<String>();
		Wrapper<UcenterYuejian> wrapper1=new QueryWrapper<UcenterYuejian>()
				.eq("bianhao", bianhao).eq("jiedanok", 0).last("limit 1")
				;
			List<UcenterYuejian> cuclist=ucenterYuejianMapper.selectList(wrapper1);
		if(cuclist.size()>0){
			UcenterYuejian uyj=cuclist.get(0);

				uyj.setJiedanok(1);

				Wrapper<UcenterUserinfo> wrapper11=new QueryWrapper<UcenterUserinfo>()
						.eq("uid", uyj.getJiauid()).last("limit 1")
						;

				Wrapper<UcenterUserinfo> wrapper12=new QueryWrapper<UcenterUserinfo>()
						.eq("uid", uyj.getYiuid()).last("limit 1")
						;
				UcenterUserinfo u1=ucenterUserinfoPlusMapper.selectOne(wrapper11);
				UcenterUserinfo u2=ucenterUserinfoPlusMapper.selectOne(wrapper12);
				u1.setJifen(u1.getJifen()+10);
				u2.setJifen(u2.getJifen()+10);
				ucenterUserinfoPlusMapper.update(u1,wrapper11);
				ucenterUserinfoPlusMapper.update(u2,wrapper12);

				result.setResult(ucenterYuejianMapper.update(uyj,wrapper1)+"");
			}else{
				result.setResult("-1");
			}

			return result;

		}
	@Transactional
	@ApiOperation(value = "约见", notes = "")
	@RequestMapping(value = "/yuejian", method = RequestMethod.POST)
	@ResponseBody
	public Result yuejian(@RequestBody UcenterYuejian cud) throws Exception {
		if(cud.getJiauid().equals(cud.getYiuid())){
			Result<String> result = new Result<String>();
			result.setResult("-1");
			return result;
		}

		//如果是都同意了，可以新增
		Date date = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(Calendar.DATE,-1); //把日期往后增加一天,整数  往后推,负数往前移动
		date=calendar.getTime();
		String strDateFormat = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		String time=sdf.format(date);
		if(CommunityDynamicService.checkYuejian(cud.getJiauid(),cud.getYiuid(),time)==0){
			Result<String> result = new Result<String>();
			cud.setJiaok(1);
			cud.setJiatime(new Date());
			cud.setBianhao("yu_"+ODGenerator.getD(null));
			cud.setCreatetime(new Date());
			cud.setJiedanok(0);
			//查询甲乙红娘，插入

			//SELECT e.hn_uid  from ucenter_guanxi e,ucenter_hongniang r where  e.hn_uid=r.hn_uid and e.uid=

			String jiauid=ucenterHongniangMapper.gethnIdByUid(cud.getJiauid());
			String yiuid=ucenterHongniangMapper.gethnIdByUid(cud.getYiuid());
			cud.setYjiahn(jiauid);
			cud.setYyihn(yiuid);

			result.setResult(CommunityDynamicService.yuejian(cud));
			return result;
		}else{
			Result<String> result = new Result<String>();
			result.setResult("-2");
			return result;

		}

	}

	@ApiOperation(value = "上传约见图片", notes = "")
	@RequestMapping(value = "/postYuejianimg", method = RequestMethod.POST)
	@ResponseBody
	public Result postYuejianimg(@RequestBody UcenterYuejianimg cud) throws Exception {
		Result<Integer> result = new Result<Integer>();

		Wrapper<UcenterYuejianimg> wrapperq=new QueryWrapper<UcenterYuejianimg>()
				.eq("yuejianid",cud.getYuejianid())
				;
		UcenterYuejianimg cud1=ucenterYuejianimgMapper.selectOne(wrapperq);
		if(cud1==null){
			ucenterYuejianimgMapper.insert(cud);
		}else{
			ucenterYuejianimgMapper.update(cud,wrapperq);
		}
		result.setResult(1);
		return result;

	}

	@ApiOperation(value = "动态发布", notes = "")
	@RequestMapping(value = "/publishDt", method = RequestMethod.POST)
	@ResponseBody
	public Result publishDt(@RequestBody CommunityUserDynamics cud) throws Exception {
		Result<String> result = new Result<String>();
		long timeStamp = System.currentTimeMillis();//获取当前时间戳(这是毫秒时间戳)
		cud.setDPosttime(new Date());
		cud.setDHiden(0);
		result.setResult(CommunityDynamicService.publishDt(cud));
		String tojson=GsonUtil.GsonString(cud);
		//redisUtil.zAdd("dongtai:mydongtai_home_"+cud.getUid(), tojson, timeStamp);//发送到我的动态列表
		redisUtil.del("dongtai:mydongtai_home_"+cud.getUid()+0);
		redisUtil.del("dongtai:mydongtai_home_"+cud.getUid()+1);
		return result;

	}

	@Cacheable(value = "person:persondetail_cache",key = "'params_'+#uid")
	@ApiOperation(value = "个人资料接口", notes = "")
	@RequestMapping(value = "/getPersondetail", method = RequestMethod.POST)
	@ResponseBody
	public UcenterUserinfo getPersondetail(@RequestParam String uid) throws Exception {

		List<UcenterUserinfo> uulist=ucenterUserinfoPlusMapper.selectList(new QueryWrapper<UcenterUserinfo>().eq("uid",uid));
		UcenterUserinfo uu=uulist.get(0);
		uu.setPwd("");
		uu.setSalt("");

		UcenterHongniang uhn=ucenterHongniangMapper.selectOne(new QueryWrapper<UcenterHongniang>().eq("hn_uid",uid));
		if(uhn!=null){
			if(uhn.getDisable()==0){
				if(uhn.getIsopen()==1){
					uu.setIsHn(1);
				}else if(uhn.getIsopen()==0){
					uu.setIsHn(0);
				}
			}else{

			}


		}else{
			//uu.setIsHn(0);
		}
		return uu;

	}


	@ApiOperation(value = "个人资料接口无缓存", notes = "")
	@RequestMapping(value = "/getPersondetailNocache", method = RequestMethod.POST)
	@ResponseBody
	public UcenterUserinfo getPersondetailNocache(@RequestParam String uid) throws Exception {

		List<UcenterUserinfo> uulist=ucenterUserinfoPlusMapper.selectList(new QueryWrapper<UcenterUserinfo>().eq("uid",uid));
		UcenterUserinfo uu=uulist.get(0);
		uu.setPwd("");
		uu.setSalt("");
		return uu;

	}

	@ApiOperation(value = "个人视频列表", notes = "")
	@RequestMapping(value = "/getPersonVideo", method = RequestMethod.POST)
	@ResponseBody
	public Result getPersonVideo(@RequestBody String uid,
								 @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								 @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								 HttpServletRequest req
	) throws Exception {
		Result<String> result = new Result<String>();

		return result;

	}

	@ApiOperation(value = "夏普算法匹配的用户列表接口", notes = "")
	@RequestMapping(value = "/viewUserShapuList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterShapu>> viewUserShapuList(
			@RequestParam String uid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {

		Result<IPage<UcenterShapu>> result = new Result<IPage<UcenterShapu>>();

		Page<UcenterShapu> page = new Page<UcenterShapu>(pageNo, pageSize);

		IPage<UcenterShapu> pageList =null;
		Wrapper<UcenterShapu> wrapper2=new QueryWrapper<UcenterShapu>()
				;
		List<UcenterShapu> list=ucenterShapuMapper.queryShapuList(uid);
		pageList = page.setRecords(list);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;

	}

	@ApiOperation(value = "进入广场接口", notes = "")
	@RequestMapping(value = "/inSquare", method = RequestMethod.POST)
	@ResponseBody
	public UcenterUserinfo inSquare(@RequestParam String uid) throws Exception {

		List<UcenterUserinfo> uulist=ucenterUserinfoPlusMapper.selectList(new QueryWrapper<UcenterUserinfo>().eq("uid",uid));
		UcenterUserinfo uuf=uulist.get(0);
		uuf.setLoginTime(System.currentTimeMillis()+"");
		String tojson=GsonUtil.GsonString(uuf);

		redisUtil.zAdd("square:userlist", tojson,System.currentTimeMillis());//发送到我的动态列表
		Long size=redisUtil.zAllCount("square:userlist");
		if(size>500){
			redisUtil.zRemoveRange("square:userlist",0L,0L);
		}

		//redisUtil.expire("square:userlist",600	);
		return uulist.get(0);

	}

	@Transactional
	@ApiOperation(value = "ADD浏览过的用户列表接口", notes = "")
	@RequestMapping(value = "/addViewUserList", method = RequestMethod.POST)
	@ResponseBody
	public UcenterUserinfo addViewUserList(@RequestParam String myuid,@RequestParam String whouid) throws Exception {

		dayhitMapper.insertDayhit(whouid,myuid);
		List<CommunityUserBeitai> ddd=dayhitMapper.getPaiming(whouid);

		String u1="",u2="",u3="",u4="",u5="";
		for(int i=0;i<ddd.size();i++){
			if(i==0){
				u1=((CommunityUserBeitai)ddd.get(i)).getToUser();
			}
			if(i==1){
				u2=((CommunityUserBeitai)ddd.get(i)).getToUser();
			}
			if(i==2){
				u3=((CommunityUserBeitai)ddd.get(i)).getToUser();
			}
			if(i==3){
				u4=((CommunityUserBeitai)ddd.get(i)).getToUser();
			}
			if(i==4){
				u5=((CommunityUserBeitai)ddd.get(i)).getToUser();
			}


		}
		communityUserBeitaiMapper.insertBertais(u1,u2,u3,u4,u5,whouid);


		List<UcenterUserinfo> uulist=ucenterUserinfoPlusMapper.selectList(new QueryWrapper<UcenterUserinfo>().eq("uid",whouid));
		UcenterUserinfo uuf=uulist.get(0);
		uuf.setLoginTime(System.currentTimeMillis()+"");
		String tojson=GsonUtil.GsonString(uuf);
		redisUtil.zAdd("viewusers:userlist_"+myuid, tojson,System.currentTimeMillis());//发送到我的动态列表
		Long size=redisUtil.zAllCount("viewusers:userlist_"+myuid);
		if(size>50){
			redisUtil.zRemoveRange("viewusers:userlist_"+myuid,0L,0L);
		}
		//redisUtil.expire("square:userlist",600	);
		return uulist.get(0);

	}

	@ApiOperation(value = "浏览过的用户列表接口", notes = "")
	@RequestMapping(value = "/viewUserList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterUserinfo>> viewUserList(
			@RequestParam("myuid") String myuid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {

		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();

		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);
		Set<Object> xx=redisUtil.ZreverseRange("viewusers:userlist_"+myuid, (long)(pageNo-1)*pageSize, (long)(pageNo*pageSize-1));
		IPage<UcenterUserinfo> pageList =null;
		List<UcenterUserinfo> cud_list=new ArrayList<UcenterUserinfo>();
		if(xx.size()==0) {
			//从mysql获取，
		}else {
			//从redis获取，
			Iterator<Object> iterator = xx.iterator();
			while(iterator.hasNext()){
				Object next = iterator.next();
				UcenterUserinfo  zz=GsonUtil.GsonToBean(next.toString(), UcenterUserinfo.class);
				cud_list.add(zz);
				//System.out.println("value:"+next.getValue()+" score:"+next.getScore());
			}
			pageList = page.setRecords(cud_list);
		}

		result.setSuccess(true);
		result.setResult(pageList);
		return result;


	}

	@ApiOperation(value = "广场列表", notes = "")
	@RequestMapping(value = "/square", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterUserinfo>> square(
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {

		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();

		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);
		Set<Object> xx=redisUtil.ZreverseRange("square:userlist", (long)(pageNo-1)*pageSize, (long)(pageNo*pageSize-1));
		IPage<UcenterUserinfo> pageList =null;
		List<UcenterUserinfo> cud_list=new ArrayList<UcenterUserinfo>();
		if(xx.size()==0) {
			//从mysql获取，
		}else {
			//从redis获取，
			Iterator<Object> iterator = xx.iterator();
			while(iterator.hasNext()){
				Object next = iterator.next();
				UcenterUserinfo  zz=GsonUtil.GsonToBean(next.toString(), UcenterUserinfo.class);
				cud_list.add(zz);
				//System.out.println("value:"+next.getValue()+" score:"+next.getScore());
			}
			pageList = page.setRecords(cud_list);
		}

		result.setSuccess(true);
		result.setResult(pageList);
		return result;


	}

    @ApiOperation(value = "我的动态列表接口", notes = "")
	@RequestMapping(value = "/my_dynamics", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserDynamics>> my_dynamics(
			  @RequestParam("uid") String uid,
			  @RequestParam("hiden") Integer hiden,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  HttpServletRequest req
			
			) throws Exception {

		Result<IPage<CommunityUserDynamics>> result = new Result<IPage<CommunityUserDynamics>>();
		
		Page<CommunityUserDynamics> page = new Page<CommunityUserDynamics>(pageNo, pageSize);
		Set<Object> xx=redisUtil.ZreverseRange("dongtai:mydongtai_home_"+uid+hiden, (long)(pageNo-1)*pageSize, (long)(pageNo*pageSize-1));
		IPage<CommunityUserDynamics> pageList =null;
    	List<CommunityUserDynamics> cud_list=new ArrayList<CommunityUserDynamics>();
    	if(xx.size()==0) {
    		//从mysql获取，
    		Map<String,Object> m = new HashMap<>();
    		m.put("uid",uid);
    		m.put("hiden",hiden);
    		pageList =CommunityDynamicService.getCommunityDynamicsByParams(page, m);
    		if(pageList.getRecords().size()>0) {
    			Iterator<CommunityUserDynamics> iterator = pageList.getRecords().iterator();
    			while(iterator.hasNext()){
    				CommunityUserDynamics cud = iterator.next();
    				String tojson=GsonUtil.GsonString(cud);
    				redisUtil.zAdd("dongtai:mydongtai_home_"+cud.getUid()+hiden, tojson, cud.getDPosttime().getTime());//发送到我的动态列表
    			}
				redisUtil.expireDay("dongtai:mydongtai_home_"+uid+hiden,1);
    		}

    	}else {
    		//从redis获取，
			Iterator<Object> iterator = xx.iterator();
			while(iterator.hasNext()){
				Object next = iterator.next();
				CommunityUserDynamics  zz=GsonUtil.GsonToBean(next.toString(), CommunityUserDynamics.class);
				cud_list.add(zz);
				//System.out.println("value:"+next.getValue()+" score:"+next.getScore());
			}
			pageList = page.setRecords(cud_list);
    	}

		result.setSuccess(true);
		result.setResult(pageList);
		return result;    	
    	
    	
	}
	@Transactional
	@ApiOperation(value = "打招呼", notes = "")
	@RequestMapping(value = "/zhaohuChat", method = RequestMethod.POST)
	@ResponseBody
	public Result zhaohuChat(@RequestBody CommunityUserChat cud) throws Exception {
		Result<String> result = new Result<String>();

		Wrapper<CommunityUserChat> wrapper1=new QueryWrapper<CommunityUserChat>()
				.eq("from_uid", cud.getFromUid()).eq("to_uid", cud.getToUid()).last("limit 1")
				;
		List<CommunityUserChat> cuclist=communityUserChatMapper.selectList(wrapper1);
		if(cuclist.size()>0){
			result.setResult("-2");//打过招呼
			return result;
		}
		//是不是红娘，红娘可以无限打招呼

		Wrapper<UcenterHongniang> wrapper2=new QueryWrapper<UcenterHongniang>()
				.eq("hn_uid", cud.getFromUid()).eq("disable", 0)
				;
		UcenterHongniang cud2=ucenterHongniangMapper.selectOne(wrapper2);
		if(cud2!=null){
			//是红娘
			cud.setMessage("你好，我是红娘，请收藏我");
		}else{

			//查询他的招呼费
			Wrapper<UcenterUserinfo> wrapper=new QueryWrapper<UcenterUserinfo>()
					.eq("uid", cud.getToUid())
					;
			UcenterUserinfo uu=ucenterUserinfoPlusMapper.selectOne(wrapper);
			int zhaohufee=10;
			int songfee=0;
			if(uu.getZhaohufee().equals("免费")){zhaohufee=0;}
			if(uu.getZhaohufee().equals("收10恋币")){zhaohufee=10;}
			if(uu.getZhaohufee().equals("收100恋币(送他50恋币)")){zhaohufee=100;songfee=50;}
			if(uu.getZhaohufee().equals("收188恋币(送他150恋币)")){zhaohufee=188;songfee=150;}
			if(uu.getZhaohufee().equals("收300恋币(送他200恋币)")){zhaohufee=300;songfee=200;}
			if(uu.getZhaohufee().equals("收500恋币(送他300恋币)")){zhaohufee=500;songfee=300;}
			if(uu.getZhaohufee().equals("收1000恋币(送他500恋币)")){zhaohufee=1000;songfee=500;}

			//查询我的余额
			Wrapper<UcenterUserinfo> wrapperMy=new QueryWrapper<UcenterUserinfo>()
					.eq("uid", cud.getFromUid())
					;
			UcenterUserinfo uuMy=ucenterUserinfoPlusMapper.selectOne(wrapperMy);
			//我的扣款
			if(uuMy.getJifen()>=zhaohufee){
				uuMy.setJifen(uuMy.getJifen()-zhaohufee);//我的扣款
				ucenterUserinfoPlusMapper.update(uuMy,wrapperMy);
				//他的加款
				Wrapper<UcenterUserinfo> wrapper12=new QueryWrapper<UcenterUserinfo>()
						.eq("uid", cud.getToUid())
						;
				UcenterUserinfo uu11=ucenterUserinfoPlusMapper.selectOne(wrapper12);
				uu11.setJifen(uu11.getJifen()+songfee);
				ucenterUserinfoPlusMapper.update(uu11,wrapper12);

			}else{
				result.setResult("-1");//余额不足
				return result;
			}
		}

		//1,聊天信息插入MYSQL
		//2,聊天信息插入redis
		//3,相关redis缓存管理
		//4,长连接推送消息给到达用户

		cud.setCreateTime(new Date());
		String chatId=CommunityDynamicService.publishChat(cud);
		result.setResult(chatId);
		//cud.setCreateTime(null);
		//cud.setType(0);

		//cud.setMessage("");
		//查询头像
		UcenterUserinfo uu1=CommunityDynamicService.getUserinfo(cud.getFromUid());
		UcenterUserinfo uu2=CommunityDynamicService.getUserinfo(cud.getToUid());
		/*
		cud.setFromnameHead(uu1.getHead());
		cud.setTonameHead(uu2.getHead());
		cud.setFromname(uu1.getNickname());
		cud.setToname(uu2.getNickname());
		*/
		long timeStamp = System.currentTimeMillis();//获取当前时间戳(这是毫秒时间戳)
		//redisUtil.zAdd("chat:mychat_person_"+cud.getFromUid(), cud.getToUid(), timeStamp);//发送到我的聊天成员列表
		redisUtil.del("chat:mychat_person_"+cud.getFromUid());
		redisUtil.del("chat:mychat_person_"+cud.getToUid());
		String tojson1=GsonUtil.GsonString(uu1);
		String tojson2=GsonUtil.GsonString(uu2);

		String tojson3=GsonUtil.GsonString(cud);
		//更新最后一条聊天记录和时间
		redisUtil.hset("chat:mychat_person_msn_"+cud.getFromUid(), cud.getToUid(), tojson3);
		//更新聊天人的基本信息
		redisUtil.hset("chat:users_baseinfo", cud.getFromUid(), tojson1);
		redisUtil.hset("chat:users_baseinfo", cud.getToUid(), tojson2);
		//redisUtil.del("chat:mychat_"+cud.getFromUid());



		JSONObject obj = new JSONObject();
		obj.put("fromnameHead", uu1.getHead());
		obj.put("createTime",new Date());
		obj.put("fromUid", cud.getFromUid());
		obj.put("message", cud.getMessage());
		obj.put("toUid",cud.getToUid() );
		obj.put("type", 0);
		obj.put("chatId", chatId);
		webSocket.sendOneMessage(cud.getToUid(), obj.toJSONString());

		redisUtil.incr("chat:notreadfromto_"+cud.getFromUid()+"_"+cud.getToUid(),1);


		return result;

	}
	@ApiOperation(value = "聊天发布", notes = "")
	@RequestMapping(value = "/publishChat", method = RequestMethod.POST)
	@ResponseBody
	public Result publishChat(@RequestBody CommunityUserChat cud) throws Exception {
		Result<String> result = new Result<String>();


		//1,聊天信息插入MYSQL
		//2,聊天信息插入redis
		//3,相关redis缓存管理
		//4,长连接推送消息给到达用户

		cud.setCreateTime(new Date());
		String chatId=CommunityDynamicService.publishChat(cud);
		result.setResult(chatId);
		//cud.setCreateTime(null);
		//cud.setType(0);

		//cud.setMessage("");
		//查询头像
		UcenterUserinfo uu1=CommunityDynamicService.getUserinfo(cud.getFromUid());
		UcenterUserinfo uu2=CommunityDynamicService.getUserinfo(cud.getToUid());
		/*
		cud.setFromnameHead(uu1.getHead());
		cud.setTonameHead(uu2.getHead());
		cud.setFromname(uu1.getNickname());
		cud.setToname(uu2.getNickname());
		*/
		long timeStamp = System.currentTimeMillis();//获取当前时间戳(这是毫秒时间戳)
		//redisUtil.zAdd("chat:mychat_person_"+cud.getFromUid(), cud.getToUid(), timeStamp);//发送到我的聊天成员列表
		redisUtil.del("chat:mychat_person_"+cud.getFromUid());
		redisUtil.del("chat:mychat_person_"+cud.getToUid());
		String tojson1=GsonUtil.GsonString(uu1);
		String tojson2=GsonUtil.GsonString(uu2);

		String tojson3=GsonUtil.GsonString(cud);
		//更新最后一条聊天记录和时间
		redisUtil.hset("chat:mychat_person_msn_"+cud.getFromUid(), cud.getToUid(), tojson3);
		//更新聊天人的基本信息
		redisUtil.hset("chat:users_baseinfo", cud.getFromUid(), tojson1);
		redisUtil.hset("chat:users_baseinfo", cud.getToUid(), tojson2);
		//redisUtil.del("chat:mychat_"+cud.getFromUid());



		JSONObject obj = new JSONObject();
		obj.put("fromnameHead", uu1.getHead());
		obj.put("createTime",new Date());
		obj.put("fromUid", cud.getFromUid());
		obj.put("message", cud.getMessage());
		obj.put("toUid",cud.getToUid() );
		obj.put("type", 0);
		obj.put("chatId", chatId);
		webSocket.sendOneMessage(cud.getToUid(), obj.toJSONString());


		redisUtil.incr("chat:notreadfromto_"+cud.getFromUid()+"_"+cud.getToUid(),1);


		return result;

	}


	@ApiOperation(value = "我的聊天人员接口", notes = "")
	@RequestMapping(value = "/my_chatPerson", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserChat>> my_chatPerson(
			@RequestParam("myuid") String myuid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {


		Result<IPage<CommunityUserChat>> result = new Result<IPage<CommunityUserChat>>();

		Page<CommunityUserChat> page = new Page<CommunityUserChat>(pageNo, pageSize);
		Set<Object> xx=redisUtil.ZRange("chat:mychat_person_"+myuid, (long)(pageNo-1)*pageSize, (long)(pageNo*pageSize-1));
		IPage<CommunityUserChat> pageList =null;
		List<CommunityUserChat> cud_list=new ArrayList<CommunityUserChat>();
		if(xx.size()==0) {

			cud_list.clear();

			//从mysql获取，
			Map<String,Object> m = new HashMap<>();
			m.put("myuid",myuid);
			//pageList =CommunityDynamicService.getCommunityChatPersonList(page, m);
			List<CommunityUserChat> chatlist=communityUserChatMapper.queryCommuntiyUserChatPersonByParams(page, m);
			if(chatlist.size()>0) {
				Iterator<CommunityUserChat> iterator = chatlist.iterator();
				while(iterator.hasNext()){
					CommunityUserChat cuc = iterator.next();
					//String tojson=GsonUtil.GsonString(cuc);

					long timeStamp = System.currentTimeMillis();//获取当前时间戳(这是毫秒时间戳)
					redisUtil.zAdd("chat:mychat_person_"+myuid, cuc.getToUid(), timeStamp);//发送到我的聊天成员列表
					redisUtil.expire("chat:mychat_person_"+myuid,60);
					UcenterUserinfo cuc1=new UcenterUserinfo();
					cuc1.setNickname(cuc.getToname());
					cuc1.setHead(cuc.getTonameHead());
					String tojson1=GsonUtil.GsonString(cuc1);
					redisUtil.hset("chat:users_baseinfo", cuc.getToUid(), tojson1);
					String tojson3=GsonUtil.GsonString(cuc);
					//更新聊天信息列表缓存为1天,用于聊天人员列表的预览最后一条消息
					redisUtil.hset("chat:mychat_person_msn_"+myuid,cuc.getToUid(), tojson3);
                    //redisUtil.expireDay("chat:mychat_person_msn_"+myuid,1);

					Integer noread= (Integer) redisUtil.get("chat:notreadfromto_"+cuc.getToUid()+"_"+myuid);
					cuc.setMessage(noread+"");
					cud_list.add(cuc);
				}

			}


			pageList = page.setRecords(cud_list);
		}else {
			//从redis获取，
			Iterator<Object> iterator = xx.iterator();
			while(iterator.hasNext()){
				Object next = iterator.next();
				String beanstring=(String) redisUtil.hget("chat:users_baseinfo", next.toString());

				String message=(String) redisUtil.hget("chat:mychat_person_msn_"+myuid, next.toString());

				CommunityUserChat get_message=GsonUtil.GsonToBean(message, CommunityUserChat.class);

				UcenterUserinfo to_uui=GsonUtil.GsonToBean(beanstring, UcenterUserinfo.class);

				//CommunityUserChat  zz=new CommunityUserChat();
				get_message.setFromUid(myuid);
				get_message.setToUid(next.toString());
				get_message.setTonameHead(to_uui.getHead());
				get_message.setToname(to_uui.getNickname());

				Integer noread= (Integer) redisUtil.get("chat:notreadfromto_"+next.toString()+"_"+myuid);
				get_message.setMessage(noread+"");
				cud_list.add(get_message);
				//System.out.println("value:"+next.getValue()+" score:"+next.getScore());
			}
			pageList = page.setRecords(cud_list);
		}

		result.setSuccess(true);
		result.setResult(pageList);
		return result;

	}


	@ApiOperation(value = "我的聊天记录接口2", notes = "")
	@RequestMapping(value = "/my_chat2", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserChat>> my_chat2(
			@RequestParam("myuid") String myuid,
			@RequestParam("touid") String touid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {
/*
		Result<IPage<CommunityUserChat>> result = new Result<IPage<CommunityUserChat>>();

		Page<CommunityUserChat> page = new Page<CommunityUserChat>(pageNo, pageSize);
		IPage<CommunityUserChat> pageList = null;
		//从mysql获取，
		Map<String, Object> m = new HashMap<>();
		m.put("myuid", myuid);
		m.put("touid", touid);
		pageList = CommunityDynamicService.getCommunityChatList(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
*/

		Result<IPage<CommunityUserChat>> result = new Result<IPage<CommunityUserChat>>();

		Page<CommunityUserChat> page = new Page<CommunityUserChat>(pageNo, pageSize);
		Set<Object> xx=redisUtil.ZreverseRange("chat:mychat_"+myuid, (long)(pageNo-1)*pageSize, (long)(pageNo*pageSize-1));
		IPage<CommunityUserChat> pageList =null;
		List<CommunityUserChat> cud_list=new ArrayList<CommunityUserChat>();
		if(xx.size()==0) {
			//从mysql获取，
			Map<String,Object> m = new HashMap<>();
			m.put("myuid", myuid);
			m.put("touid", touid);
			pageList =CommunityDynamicService.getCommunityChatList(page, m);
			if(pageList.getRecords().size()>0) {
				Iterator<CommunityUserChat> iterator = pageList.getRecords().iterator();
				while(iterator.hasNext()){
					CommunityUserChat cud = iterator.next();
					String tojson=GsonUtil.GsonString(cud);
					redisUtil.zAdd("chat:mychat_"+cud.getFromUid(), tojson, cud.getCreateTime().getTime());//发送到我的动态列表
				}
				redisUtil.expireDay("chat:mychat_"+myuid,1);
			}

		}else {
			//从redis获取，
			Iterator<Object> iterator = xx.iterator();
			while(iterator.hasNext()){
				Object next = iterator.next();
				CommunityUserChat  zz=GsonUtil.GsonToBean(next.toString(), CommunityUserChat.class);
				cud_list.add(zz);
				//System.out.println("value:"+next.getValue()+" score:"+next.getScore());
			}
			pageList = page.setRecords(cud_list);
		}

		result.setSuccess(true);
		result.setResult(pageList);
		return result;

	}


	@ApiOperation(value = "我的聊天记录关闭check", notes = "")
	@RequestMapping(value = "/my_chat_check", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> my_chat_check(
			@RequestParam("myuid") String myuid,
			@RequestParam("touid") String touid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {
		redisUtil.del("chat:notreadfromto_"+touid+"_"+myuid);
		Result<String> result = new Result<String>();

		result.setSuccess(true);
		result.setResult("1");
		return result;


	}
	@ApiOperation(value = "我的聊天记录接口", notes = "")
	@RequestMapping(value = "/my_chat", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserChat>> my_chat(
			@RequestParam("myuid") String myuid,
			@RequestParam("touid") String touid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {
		redisUtil.del("chat:notreadfromto_"+touid+"_"+myuid);
		Result<IPage<CommunityUserChat>> result = new Result<IPage<CommunityUserChat>>();

		Page<CommunityUserChat> page = new Page<CommunityUserChat>(pageNo, pageSize);
		IPage<CommunityUserChat> pageList = null;
		//从mysql获取，
		Map<String, Object> m = new HashMap<>();
		m.put("myuid", myuid);
		m.put("touid", touid);
		pageList = CommunityDynamicService.getCommunityChatList(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;


	}

	@ApiOperation(value = "关注用户的动态列表接口", notes = "")
	@RequestMapping(value = "/guanzhu_dynamics", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserDynamics>> guanzhu_dynamics(
			@RequestParam("uid") String uid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req

	) throws Exception {

		Result<IPage<CommunityUserDynamics>> result = new Result<IPage<CommunityUserDynamics>>();

		Page<CommunityUserDynamics> page = new Page<CommunityUserDynamics>(pageNo, pageSize);
		IPage<CommunityUserDynamics> pageList =null;
		//从mysql获取，
		Map<String,Object> m = new HashMap<>();
		m.put("from_uid",uid);
		pageList =CommunityDynamicService.getCommunityGuanzhuDynamicsLatestweek(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;


	}


	@ApiOperation(value = "动态删除", notes = "")
	@RequestMapping(value = "/delDt", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> delDt(
			HttpServletRequest request,
			@RequestParam("uid") String uid,
			@RequestParam("did") String did
			) throws Exception {
		Result<String> result = new Result<String>();
		String token = request.getHeader(DefContants.X_ACCESS_TOKEN);
		if(oConvertUtils.isEmpty(token)) {
			return result;
		}
		String uid1 = getUid(token);
		if(!uid.equals(uid1)){
			return result;
		}


		int x=CommunityDynamicService.delDt(uid, did);

        result.setResult(x+"");
		redisUtil.del("dongtai:mydongtai_home_"+uid+0);
		redisUtil.del("dongtai:mydongtai_home_"+uid+1);
        return result;
	}

	@ApiOperation(value = "隐藏动态", notes = "")
	@RequestMapping(value = "/hidenDt", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> hidenDt(
			HttpServletRequest request,
			@RequestParam("uid") String uid,
			@RequestParam("did") String did,
			@RequestParam("hiden") Integer hiden
	) throws Exception {
		Result<String> result = new Result<String>();
		String token = request.getHeader(DefContants.X_ACCESS_TOKEN);
		if(oConvertUtils.isEmpty(token)) {
			return result;
		}
		String uid1 = getUid(token);
		if(!uid.equals(uid1)){
			return result;
		}


		int x=CommunityDynamicService.hidenDt(uid, did,hiden);

		result.setResult(x+"");
		redisUtil.del("dongtai:mydongtai_home_"+uid+0);
		redisUtil.del("dongtai:mydongtai_home_"+uid+1);
		return result;
	}

    @ApiOperation(value = "关注", notes = "")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "query", dataType = "string", name = "from_uid", value = "来自用户ID", required = true), 
		@ApiImplicitParam(paramType = "query", dataType = "string", name = "to_uid", value = "要关注的用户ID", required = true),
	})
	@RequestMapping(value = "/follow", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> follow(
			@RequestParam("from_uid") String from_uid,
			@RequestParam("to_uid") String to_uid
			) throws Exception {
    	int x=CommunityDynamicService.follow(from_uid, to_uid);    	
		Result<String> result = new Result<String>();
        result.setResult(x+"");
        return result;
	}
    
    @ApiOperation(value = "取消关注", notes = "")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "query", dataType = "string", name = "from_uid", value = "来自用户ID", required = true), 
		@ApiImplicitParam(paramType = "query", dataType = "string", name = "to_uid", value = "要关注的用户ID", required = true),
	})
	@RequestMapping(value = "/unfollow", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> unfollow(
			@RequestParam("from_uid") String from_uid,
			@RequestParam("to_uid") String to_uid
			) throws Exception {
    	int x=CommunityDynamicService.unfollow(from_uid, to_uid);    	
		Result<String> result = new Result<String>();
        result.setResult(x+"");
        return result;
	}
    
    
    @ApiOperation(value = "获取当前登录用户关注用户列表接口", notes = "")
	@RequestMapping(value = "/get_followlist", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<ZanlistParams>> get_followlist(
		@RequestParam("from_uid") String from_uid,
		@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
		@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
		  HttpServletRequest req
			) throws Exception {
    	
		Result<IPage<ZanlistParams>> result = new Result<IPage<ZanlistParams>>();
		
		Page<ZanlistParams> page = new Page<ZanlistParams>(pageNo, pageSize);
		//获取zset里的uid
		Set<Object> xx=redisUtil.ZreverseRange("follow:follow_guanzhu_"+from_uid, (long)(pageNo-1)*pageSize, (long)(pageNo*pageSize-1));
		IPage<ZanlistParams> pageList =null;
    	List<ZanlistParams> cud_list=new ArrayList<ZanlistParams>();
    	if(xx.size()>0) {
    
    		//遍历关注的用户uid
			Iterator<Object> iterator = xx.iterator();
			while(iterator.hasNext()){
				Object next = iterator.next();
				String beanstring=(String) redisUtil.hget("user:users_baseinfo", next.toString());
				ZanlistParams zp=new ZanlistParams();
				try {
					UcenterUserinfo  zz=GsonUtil.GsonToBean(beanstring, UcenterUserinfo.class);
					zp.setCreatetime(zz.getCreateTime());
					zp.setHead(zz.getHead());
					zp.setNickname(zz.getNickname());
					zp.setUsername(zz.getUsername());
					cud_list.add(zp);
				}catch(Exception e) {
					
				}
				
				//System.out.println("value:"+next.getValue()+" score:"+next.getScore());
			}
			pageList = page.setRecords(cud_list);
    	}

		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}  
    
    @ApiOperation(value = "获取当前登录用户粉丝用户列表接口", notes = "")
	@RequestMapping(value = "/get_followedlist", method = RequestMethod.POST)
	@ResponseBody
	public  Result<IPage<UcenterUserinfo>> get_followedlist(
			  @RequestParam("from_uid") String from_uid,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  HttpServletRequest req			
			) throws Exception {
		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();
		
		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);
	
		Map<String,Object> m = new ConcurrentHashMap<>();
		m.put("uid",from_uid);
		IPage<UcenterUserinfo> pageList = CommunityDynamicService.getFensiUserinfos(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	//地区范围内7天首页推荐，缓存10秒dongtaiHomeCache。
	@Cacheable(value = "dynamics:home_latestweek_cache",key = "'params_'+#pageNo+#cp")
    @ApiOperation(value = "首页推荐动态", notes = "")
	@RequestMapping(value = "/recommend_dynamic", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserDynamics>> recommend_dynamic(
			@RequestBody LocationParams cp,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  HttpServletRequest req
			) throws Exception {
    	//1、根据定位，查找用户
    	//BigDecimal lat=cp.getLat();//纬度
    	//BigDecimal lng=cp.getLng();//经度
    	//String location=cp.getLocation();
    	//List<UcenterUserinfo> uulist=ucenterUserinfoPlusMapper.selectList(new QueryWrapper<UcenterUserinfo>().eq("location",location));
    	//2、查询用户的动态
    	//3、按时间倒序

		Result<IPage<CommunityUserDynamics>> result = new Result<IPage<CommunityUserDynamics>>();
		
		Page<CommunityUserDynamics> page = new Page<CommunityUserDynamics>(pageNo, pageSize);
	
		Map<String,Object> m = new HashMap<>();
		m.put("location",cp.getLocation().split("-")[0]);
		IPage<CommunityUserDynamics> pageList = CommunityDynamicService.getCommunityDynamicsLatestweek(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	//@Cacheable(value = "person:tuijian",key = "'params_'+#pageNo+#usertype+#touid")
	@ApiOperation(value = "我的红娘列表", notes = "")
	@RequestMapping(value = "/queryShoucangHongniangList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterUserinfo>> queryShoucangHongniangList(
			@RequestParam(name="uid",required=false) String uid ,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();

		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();
		m.put("uid",uid);
		IPage<UcenterUserinfo> pageList = CommunityDynamicService.queryShoucangHongniangList(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	@ApiOperation(value = "红娘守护列表", notes = "")
	@RequestMapping(value = "/queryShouhuHongniangList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterUserinfo>> queryShouhuHongniangList(
			@RequestParam(name="uid",required=false) String uid ,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();

		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();
		m.put("uid",uid);
		IPage<UcenterUserinfo> pageList = CommunityDynamicService.queryShouhuHongniangList(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	@ApiOperation(value = "红娘ALL守护列表", notes = "")
	@RequestMapping(value = "/queryShouhuHongniangAllList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterUserinfo>> queryShouhuHongniangAllList(
			@RequestParam(name="uid",required=false) String uid ,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();

		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();
		m.put("uid",uid);
		IPage<UcenterUserinfo> pageList = CommunityDynamicService.queryShouhuHongniangAllList(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	@ApiOperation(value = "收藏红娘", notes = "")
	@RequestMapping(value = "/shoucangHongniang", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> shoucangHongniang(

			@RequestParam(name="myuid") String myuid ,
			@RequestParam(name="hnuid") String hnuid

	) throws Exception {

		Result<String> result = new Result<String>();
		UcenterShoucangHongniang ucenterHongniang=new UcenterShoucangHongniang();
		ucenterHongniang.setHnuid(hnuid);
		ucenterHongniang.setMyuid(myuid);
		ucenterHongniang.setShoucangtimes(new Date());
		Wrapper<UcenterShoucangHongniang> wrapper=new QueryWrapper<UcenterShoucangHongniang>()
				.eq("hnuid",hnuid).eq("myuid", myuid)
				;
		UcenterShoucangHongniang cud1=ucenterShouCangHongniangMapper.selectOne(wrapper);
		if(cud1!=null){
			ucenterShouCangHongniangMapper.update(ucenterHongniang,wrapper);
		}else{
			ucenterShouCangHongniangMapper.insert(ucenterHongniang);
		}
		//person:persondetail_cache",key = "'params_
		result.setSuccess(true);
		result.setResult("1");
		return result;
	}


	@Cacheable(value = "person:tuijian",key = "'params_'+#pageNo+#usertype+#touid")
	@ApiOperation(value = "会员推荐列表", notes = "")
	@RequestMapping(value = "/getTuijianUserinfosList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterUserinfo>> getTuijianUserinfosList(
			@RequestParam("usertype") int usertype,
			@RequestParam(name="touid",required=false) String touid ,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();

		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();
		m.put("usertype",usertype);
		m.put("touid",touid);
		IPage<UcenterUserinfo> pageList = CommunityDynamicService.getTuijianUserinfos(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	//UcenterHongniangMapper  UcenterGuanxiMapper

	@Transactional
	@ApiOperation(value = "选守护红娘", notes = "")
	@RequestMapping(value = "/getShouhuHongniang", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> getShouhuHongniang(
			@RequestParam(name="orguid") String orguid ,
			@RequestParam(name="shouhuuid") String shouhuuid,
			@RequestParam(name="type") String type
	) throws Exception {

		Result<String> result = new Result<String>();
		UcenterGuanxi guanxi=new UcenterGuanxi();
		guanxi.setUid(orguid);
		if(type.equals("1")){
			guanxi.setHnuid(shouhuuid);
		}else if(type.equals("2")){
			guanxi.setFuuid(shouhuuid);
		}else if(type.equals("3")){
			guanxi.setMuuid(shouhuuid);
		}else if(type.equals("4")){
			guanxi.setHpyuid(shouhuuid);
		}

		guanxi.setGxtime(new Date());
		Wrapper<UcenterGuanxi> wrapper=new QueryWrapper<UcenterGuanxi>()
				.eq("uid", guanxi.getUid())
				;
		UcenterGuanxi cud1=ucenterGuanxiMapper.selectOne(wrapper);
		UcenterGuanxiHj guanxiHj=new UcenterGuanxiHj();
		 BeanUtils.copyProperties(guanxi,guanxiHj);
		if(cud1!=null){
			ucenterGuanxiMapper.update(guanxi,wrapper);
		}else{
			ucenterGuanxiMapper.insert(guanxi);


		}

		Wrapper<UcenterGuanxiHj> wrapper1=new QueryWrapper<UcenterGuanxiHj>()
				.eq("uid", guanxi.getUid()).eq("hn_uid", guanxi.getHnuid())
				;
		UcenterGuanxiHj cud11=ucenterGuanxiHjMapper.selectOne(wrapper1);
		if(cud11==null) {
			ucenterGuanxiHjMapper.insert(guanxiHj);
		}
		result.setSuccess(true);
		result.setResult("1");
		return result;
	}

	@ApiOperation(value = "申请红娘", notes = "")
	@RequestMapping(value = "/shenqingHongniang", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> shenqingHongniang(

			@RequestBody UcenterHongniang uhn

	) throws Exception {

		Result<String> result = new Result<String>();
		UcenterHongniang ucenterHongniang=new UcenterHongniang();
		ucenterHongniang.setHnuid(uhn.getHnuid());
		ucenterHongniang.setCreatime(new Date());
		ucenterHongniang.setHnmobile(uhn.getHnmobile());
		ucenterHongniang.setFuwuhuiyuan(uhn.getFuwuhuiyuan());
		ucenterHongniang.setFuwushouru(uhn.getFuwushouru());
		ucenterHongniang.setShuoming(uhn.getShuoming());
		ucenterHongniang.setBiaoyu(uhn.getBiaoyu());
		ucenterHongniang.setFuwufei(uhn.getFuwufei());
		ucenterHongniang.setIsopen(uhn.getIsopen());

		Wrapper<UcenterHongniang> wrapper=new QueryWrapper<UcenterHongniang>()
				.eq("hn_uid", uhn.getHnuid())
				;
		UcenterHongniang cud1=ucenterHongniangMapper.selectOne(wrapper);
		if(cud1!=null){
			ucenterHongniangMapper.update(ucenterHongniang,wrapper);
		}else{
			ucenterHongniang.setDisable(1);
			ucenterHongniangMapper.insert(ucenterHongniang);
		}
		//person:persondetail_cache",key = "'params_
		redisUtil.del("person:persondetail_cache::params_"+uhn.getHnuid());

		result.setSuccess(true);
		result.setResult("1");
		return result;
	}

	//

	@ApiOperation(value = "亲属查询", notes = "")
	@RequestMapping(value = "/queryQianshu", method = RequestMethod.POST)
	@ResponseBody
	public Result<UcenterGuanxi> queryQianshu(
			@RequestParam(name="uid") String uid ,
			HttpServletRequest req
	) throws Exception {

		Result<UcenterGuanxi> result = new Result<UcenterGuanxi>();
		UcenterGuanxi pageList = ucenterUserinfoPlusMapper.queryQianshuByParams(uid);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	@ApiOperation(value = "红娘查询", notes = "")
	@RequestMapping(value = "/queryHongniang", method = RequestMethod.POST)
	@ResponseBody
	public Result<UcenterUserinfo> queryHongniang(
			@RequestParam(name="uid") String uid ,
			HttpServletRequest req
	) throws Exception {

		Result<UcenterUserinfo> result = new Result<UcenterUserinfo>();
		UcenterUserinfo pageList = ucenterUserinfoPlusMapper.queryHongniangByParams(uid);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	@ApiOperation(value = "红娘列表", notes = "")
	@RequestMapping(value = "/queryHongniangList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterUserinfo>> queryHongniangList(
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterUserinfo>> result = new Result<IPage<UcenterUserinfo>>();

		Page<UcenterUserinfo> page = new Page<UcenterUserinfo>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();

		IPage<UcenterUserinfo> pageList = CommunityDynamicService.queryHongniangList(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	//
	//@Cacheable(value = "person:yuejianlist",key = "'params_'+#pageNo+#usertype+#touid")
	@ApiOperation(value = "约见列表", notes = "")
	@RequestMapping(value = "/getYuejianList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterYuejian>> getYuejianList(
			@RequestParam(name="uid") String uid ,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterYuejian>> result = new Result<IPage<UcenterYuejian>>();

		Page<UcenterYuejian> page = new Page<UcenterYuejian>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();
		m.put("uid",uid);
		IPage<UcenterYuejian> pageList = CommunityDynamicService.queryYuyue(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	@ApiOperation(value = "约见服务单列表", notes = "")
	@RequestMapping(value = "/getYuejianFwdList", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterYuejian>> getYuejianFwdList(
			@RequestParam(name="uid") String uid ,
			@RequestParam(name="nickname") String nickname ,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterYuejian>> result = new Result<IPage<UcenterYuejian>>();

		Page<UcenterYuejian> page = new Page<UcenterYuejian>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();
		m.put("uid",uid);
		m.put("nickname",nickname);

		IPage<UcenterYuejian> pageList = CommunityDynamicService.queryYuyueFwd(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}
	@ApiOperation(value = "HN服务单列表", notes = "")
	@RequestMapping(value = "/queryHnFwd", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<UcenterYuejian>> queryHnFwd(
			@RequestParam(name="uid") String uid ,
			@RequestParam(name="nickname") String nickname ,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<UcenterYuejian>> result = new Result<IPage<UcenterYuejian>>();

		Page<UcenterYuejian> page = new Page<UcenterYuejian>(pageNo, pageSize);

		Map<String,Object> m = new HashMap<>();
		m.put("uid",uid);
		m.put("nickname",nickname);
		IPage<UcenterYuejian> pageList = CommunityDynamicService.queryHnFwd(page, m);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}


	@ApiOperation(value = "点赞", notes = "")
	@RequestMapping(value = "/zan", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> zan(@RequestParam("uid") String uid,@RequestParam("did") String did) throws Exception {
	
		Result<String> result = new Result<String>();
        //1.从redis中取出用户吐槽点赞记录,24小时内防止重复点赞
        String flag = (String) redisUtil.get("dianzan:userzan_" + did + "_" + uid);
        //2.如果没有记录，让用户点赞，并且把点赞记录存入redis中
        if (flag == null) {
			redisUtil.set("dianzan:userzan_" + did + "_" + uid, "1", 1*3600*24);
        	int ok=CommunityDynamicService.upzan(uid, did);
        	if(ok>0) {
        		//一天内不能重复点赞

        		result.setResult("1");
        		result.success("吐槽点赞成功!");
        		//清除did该动态的用户动态列表缓存，用于更新点赞数。
				Wrapper<CommunityUserDynamics> wrapper=new QueryWrapper<CommunityUserDynamics>()
						.eq("did", did)
						;
				CommunityUserDynamics cud=communityDynamicsMapper.selectOne(wrapper);
				redisUtil.del("dongtai:mydongtai_home_"+cud.getUid()+0);
				redisUtil.del("dongtai:mydongtai_home_"+cud.getUid()+1);
        	}else {
        		result.error500("数据库点赞异常");
        	}
            
        } else {
            //3.如果有记录，提示“不能重复点赞”
        	result.error500("不能重复点赞");
        }
        return result;

	}

	@ApiOperation(value = "获取动态的评论列表", notes = "")
	@RequestMapping(value = "/dt_discusslist", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserDiscuss>>  dt_discusslist(
			@RequestParam("did") String did,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<CommunityUserDiscuss>> result = new Result<IPage<CommunityUserDiscuss>>();

		Page<CommunityUserDiscuss> page = new Page<CommunityUserDiscuss>(pageNo, pageSize);
		Set<Object> xx=redisUtil.ZreverseRange("discuss:discusslist_"+did, (long)(pageNo-1)*pageSize, (long)(pageNo*pageSize-1));
		IPage<CommunityUserDiscuss> pageList =null;
		List<CommunityUserDiscuss> cud_list=new ArrayList<CommunityUserDiscuss>();
		if(xx.size()==0) {
			//从mysql获取，
			Map<String, Object> m = new HashMap<>();
			m.put("did", did);
			pageList = CommunityDynamicService.getDiscussList(page, m);

			if(pageList.getRecords().size()>0) {
				Iterator<CommunityUserDiscuss> iterator = pageList.getRecords().iterator();
				while(iterator.hasNext()){
					CommunityUserDiscuss cud = iterator.next();
					String tojson=GsonUtil.GsonString(cud);
					redisUtil.zAdd("discuss:discusslist_"+cud.getDiscDid(), tojson, cud.getDiscCreatime().getTime());//发送到我的动态列表

				}
				redisUtil.expireDay("discuss:discusslist_"+did,1);
			}


		}else {
			//从redis获取，
			Iterator<Object> iterator = xx.iterator();
			while(iterator.hasNext()){
				Object next = iterator.next();
				CommunityUserDiscuss  zz=GsonUtil.GsonToBean(next.toString(), CommunityUserDiscuss.class);
				cud_list.add(zz);
				//System.out.println("value:"+next.getValue()+" score:"+next.getScore());
			}
			pageList = page.setRecords(cud_list);
		}
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	@ApiOperation(value = "获取动态的回复评论列表", notes = "")
	@RequestMapping(value = "/dt_discusslistHuifu", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<CommunityUserDiscuss>>  dt_discusslistHuifu(
			@RequestParam("discid") String discid,
			@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest req
	) throws Exception {

		Result<IPage<CommunityUserDiscuss>> result = new Result<IPage<CommunityUserDiscuss>>();

		Page<CommunityUserDiscuss> page = new Page<CommunityUserDiscuss>(pageNo, pageSize);
		IPage<CommunityUserDiscuss> pageList =null;
			//从mysql获取，
			Map<String, Object> m = new HashMap<>();
			m.put("discid", discid);
			pageList = CommunityDynamicService.getDiscussListHuifu(page, m);

		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}


	@ApiOperation(value = "发表评论", notes = "")
	@RequestMapping(value = "/add_discuss", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> add_discuss(@RequestBody CommunityUserDiscuss cud) throws Exception {
		long timeStamp = System.currentTimeMillis();//获取当前时间戳(这是毫秒时间戳)
		cud.setDiscCreatime(new Date());
		//cud.setDiscId("");
		//cud.setDiscHuifuDiscid(cud.getDiscId());
		String x= CommunityDynamicService.addDiscuss(cud);
		Result<String> result = new Result<String>();
		result.setResult(x+"");
		String tojson=GsonUtil.GsonString(cud);
		redisUtil.del("discuss:discusslist_"+cud.getDiscDid());//评论后，清一下缓存。查询的时候数据库加载一份到缓存。
		//通过did查询所属uid，删除该用户的redis我的动态缓存，
		Wrapper<CommunityUserDynamics> wrapper=new QueryWrapper<CommunityUserDynamics>()
				.eq("did", cud.getDiscDid())
				;
		CommunityUserDynamics cud1=communityDynamicsMapper.selectOne(wrapper);
		redisUtil.del("dongtai:mydongtai_home_"+cud1.getUid()+0);
		redisUtil.del("dongtai:mydongtai_home_"+cud1.getUid()+1);


		//redisUtil.zAdd("discuss:discusslist_"+cud.getDiscDid(), tojson, timeStamp);//发送到我的动态列表
		return result;


	}

	public  String getUid(String token) {
		try {
			DecodedJWT jwt = JWT.decode(token);
			String username= jwt.getClaim("u56sde#$%rn#2am2e").asString();
			UcenterUserinfo uu=ucenterUserinfoPlusMapper.selectOne(new QueryWrapper<UcenterUserinfo>().eq("username",username));
			return  uu.getUid();
		} catch (JWTDecodeException e) {
			return null;
		}
	}

	@ApiOperation(value = "删除评论", notes = "")
	@RequestMapping(value = "/del_discuss", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> del_discuss(
			HttpServletRequest request,
			@RequestParam("did") String did,
			@RequestParam("discId") String discId,
			@RequestParam("uid") String uid

	) throws Exception {
		Result<String> result = new Result<String>();
		String token = request.getHeader(DefContants.X_ACCESS_TOKEN);
		if(oConvertUtils.isEmpty(token)) {
			return result;
		}
		String uid1 = getUid(token);
		if(!uid.equals(uid1)){
			return result;
		}
		int x= CommunityDynamicService.delDiscuss(discId,uid,did);


		result.setResult(x+"");
		redisUtil.del("discuss:discusslist_"+did);//
		return result;
	}

    @ApiOperation(value = "撤回聊天记录", notes = "")
    @RequestMapping(value = "/delChat", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> delChat(
			@RequestParam("touser") String touser,
            @RequestParam("chat_id") String chat_id,
			@RequestParam("index") String index
    ) throws Exception {
        int x= CommunityDynamicService.delChat(chat_id);

		JSONObject obj = new JSONObject();
		obj.put("fromnameHead", "");
		obj.put("createTime",new Date());
		obj.put("fromUid", "");
		obj.put("message", "");
		obj.put("toUid", "");
		obj.put("type", 4);
		obj.put("chatId", chat_id);
		obj.put("index", Integer.parseInt(index));
		webSocket.sendOneMessage(touser, obj.toJSONString());

        Result<String> result = new Result<String>();
        result.setResult(x+"");
        return result;
    }


	@ApiOperation(value = "点赞人列表（谁给点的赞）", notes = "")
	@RequestMapping(value = "/zan_userlist", method = RequestMethod.POST)
	@ResponseBody
	public Result<IPage<ZanlistParams>> zan_userlist(@RequestParam("did") String did,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  HttpServletRequest req
			) throws Exception {
	
		Result<IPage<ZanlistParams>> result = new Result<IPage<ZanlistParams>>();
		
		Page<ZanlistParams> page = new Page<ZanlistParams>(pageNo, pageSize);
		IPage<ZanlistParams> pageList = CommunityDynamicService.getZanlist(page,did);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}
	/*
	@ApiOperation(value = "评论人列表（谁给评论）", notes = "")
	@RequestMapping(value = "/comment_userlist", method = RequestMethod.GET)
	@ResponseBody
	public String comment_userlist() throws Exception {
		//feedService.register();
		return "------111--------";
	}

	 */
}
