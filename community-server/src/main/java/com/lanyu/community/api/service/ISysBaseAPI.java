package com.lanyu.community.api.service;


import java.sql.SQLException;
import java.util.List;

import com.lanyu.community.api.shiro.LoginUser;



/**
 * @Description: 底层共通业务API，提供其他独立模块调用
 * @Author: suibin
 * @Date:2019-11-20 
 * @Version:V1.0
 */
public interface ISysBaseAPI {

	/**
	 * 日志添加
	 * @param LogContent 内容
	 * @param logType 日志类型(0:操作日志;1:登录日志;2:定时任务)
	 * @param operatetype 操作类型(1:添加;2:修改;3:删除;)
	 */
	void addLog(String LogContent, Integer logType, Integer operatetype);
	
	/**
	  * 根据用户账号查询登录用户信息
	 * @param username
	 * @return
	 */
    LoginUser getUserByName(String username);
	
	/**
	 * 通过用户账号查询角色集合
	 * @param username
	 * @return
	 */
    List<String> getRolesByUsername(String username);

	/**
	 * 获取当前数据库类型
	 * @return
	 * @throws Exception 
	 */
    String getDatabaseType() throws SQLException;
	

	/**
	 * 发送系统消息
	 * @param fromUser 发送人(用户登录账户)
	 * @param toUser  发送给(用户登录账户)
	 * @param title  消息主题
	 * @param msgContent  消息内容
	 */
    void sendSysAnnouncement(String fromUser, String toUser, String title, String msgContent);
	
}
