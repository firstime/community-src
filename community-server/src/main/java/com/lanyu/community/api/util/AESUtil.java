package com.lanyu.community.api.util;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lanyu.mybatis.dao.entity.UcenterFee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * AES常用解密加密工具类
 * https://github.com/ourlang
 * @author 小林
 */
public class AESUtil {

    /**
     * 默认的字符编码
     */
    private static final String DEFAULT_CHARSET = "utf-8";

    /**
     * 算法
     */
    private static String ALGORITHM = "AES";


    /**
     * 算法/模式/填充
     **/
    private static final String CipherMode = "AES/ECB/PKCS5Padding";


    /**
     * 记录日志
     **/
    private final static Logger logger = LoggerFactory.getLogger(AESUtil.class);

    private AESUtil() {
    }

    /**
     * 解密AES 32位
     *
     * @param sSrc      解密的内容
     * @param secretKey 秘钥
     * @return 解密后的明文 数据
     */
    public static String decrypt(String sSrc, String secretKey) {

        if (secretKey == null) {
            logger.error("需要加密的秘钥为空");
            return null;
        }
        try {
            byte[] raw = secretKey.getBytes(DEFAULT_CHARSET);
            SecretKeySpec secretKeySpec = new SecretKeySpec(raw, ALGORITHM);
            Cipher cipher = Cipher.getInstance(CipherMode);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            // 先用base64解密
            byte[] encryptedArr = Base64.getDecoder().decode(sSrc);
            byte[] original = cipher.doFinal(encryptedArr);
            return new String(original, DEFAULT_CHARSET);
        } catch (Exception ex) {
            logger.error("AES解密失败", ex);
            return null;
        }
    }


    /**
     * 加密32位
     *
     * @param sSrc 需要加密的内容
     * @param sKey 秘钥
     * @return 加密的内容
     */
    public static String encrypt(String sSrc, String sKey) {
        if (sKey == null) {
            logger.error("需要加密的秘钥为空");
            return null;
        }
        try {
            byte[] raw = sKey.getBytes(DEFAULT_CHARSET);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, ALGORITHM);
            Cipher cipher = Cipher.getInstance(CipherMode);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(sSrc.getBytes(DEFAULT_CHARSET));

            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception ex) {
            logger.error("AES加密失败", ex);
            return null;
        }
    }
    public static void main(String args[]){
        String jiami="pkE+52P2DtwBbY29BGS8USzY5JWPIgVo2Ezq62N45c7ZtkFgjnvaRQrORxma1zbHqJ3EwWCdffcYIvx6T95ZavdKlWsPmrljRSff6v/Os48=";
        String decode = AESUtil.decrypt(jiami, "adddfe2153da7a1cf3bf26c92fb8c9bc");
        System.out.println("解密后: " + decode);
decode="{\"uid\":\"674b126fec114cdd6a30b1c20f78b337\",\"orderno\":\"no_1396421141\",\"fee\":1}";
        JSONObject ucflist= (JSONObject) JSONObject.parse(decode);

        System.out.println("解密后: " + ucflist.getString("fee"));
    }
}

