package com.lanyu.community.api.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lanyu.community.api.service.MahoutService;
import com.lanyu.mybatis.dao.entity.CommunityVersion;
import com.lanyu.mybatis.dao.mapperex.CommunityVersionMapper;
import io.swagger.annotations.Api;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/*
 * create by suibin
 * API-字体API
 * 2017-10-27
 */
@Api("API-更新APP")
@RestController
@RequestMapping("appversions")
public class AppVerController {


    
    public Logger logger = Logger.getLogger(HttpRequestHandler.class);
	@Autowired
	com.lanyu.mybatis.dao.mapperex.CommunityVersionMapper communityVersionMapper;

	@RequestMapping(value = "/latest", method = RequestMethod.GET)
	@ResponseBody
	public CommunityVersion uploadFile(HttpServletRequest request) throws Exception {

		List<CommunityVersion> list=communityVersionMapper.checkVersion();
		//计算
		return list.get(0);

	
	}
	
	

}
