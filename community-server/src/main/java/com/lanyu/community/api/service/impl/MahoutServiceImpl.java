package com.lanyu.community.api.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;

import com.lanyu.community.api.beans.RequestPageSearchParams;
import com.lanyu.community.api.entity.SyncFont;
import com.lanyu.community.api.entity.User;
import com.lanyu.community.api.service.MahoutService;
import com.lanyu.community.api.util.BusinessException;
import com.lanyu.community.api.util.ErrorCode;
import com.lanyu.community.api.util.ErrorEnum;

import com.lanyu.community.api.util.RedisUtil;


/* google的字体样式转换库
import com.google.typography.font.sfntly.Font;
import com.google.typography.font.sfntly.FontFactory;
import com.google.typography.font.sfntly.Tag;
import com.google.typography.font.sfntly.data.WritableFontData;
import com.google.typography.font.sfntly.table.core.CMapTable;
import com.google.typography.font.tools.conversion.eot.EOTWriter;
import com.google.typography.font.tools.conversion.woff.WoffWriter;
import com.google.typography.font.tools.sfnttool.GlyphCoverage;
import com.google.typography.font.tools.subsetter.RenumberingSubsetter;
import com.google.typography.font.tools.subsetter.Subsetter;
*/

@Service
public class MahoutServiceImpl implements MahoutService {
	
	@Override
	public void testeth() {
		// TODO Auto-generated method stub
		
	}



	private static final Logger _log = LoggerFactory.getLogger(MahoutServiceImpl.class);
	
  

	@Override
	public boolean uploadFile(MultipartFile file,HttpServletRequest request) throws IOException{
		// TODO Auto-generated method stub
        if(!file.isEmpty()){
            String filePath = file.getOriginalFilename();
            //windows
            String savePath = request.getSession().getServletContext().getRealPath(filePath);

            //linux
            //String savePath = "/home/odcuser/webapps/file";
            File targetFile = new File(savePath);
            file.transferTo(targetFile);
            return true;
        }

        return false;

	}
    

}
