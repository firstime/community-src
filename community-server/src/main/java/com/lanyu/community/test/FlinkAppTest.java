package com.lanyu.community.test;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.util.SingleElementIterator;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import sun.nio.cs.StreamEncoder;
import unity.predicates.In;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: suibin
 * @Date: 2021/04/23/15:30
 * @Description: 词频统计.服务器nc -lk 8888
 * flink是一个主从架构，主节点:jobManager 从节点：taskManager
 * 以下几个算子operator说明：
 * source 是一个固定task
 * flatMap 是一个并行task，受setParallelism影响
 * keyBy和sum 是一个并行task，受setParallelism影响
 * sink 是一个固定task
 */
public class FlinkAppTest {
    public static void main(String [] args) throws Exception {
        //步骤一，获取程序入口
        //StreamExecutionEnvironment env=StreamExecutionEnvironment.getExecutionEnvironment();
        StreamExecutionEnvironment env=StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());
        env.setParallelism(16);//全局并行度设置，一个任务并行处理.16是16个task并行，默认不设置的话是cpu数。。。。11> (ddd,1) 的11代表并行task的编号

        //传参
        //ParameterTool parameterTool=ParameterTool.fromArgs(args);
        //String hostname=parameterTool.get("hostname");
        //int port=parameterTool.getInt("port");

        //步骤二，数据输入
        DataStreamSource<String> dateStream =env.socketTextStream("47.52.57.16",8888);//1 task
        //步骤三，数据的处理，聚合分组
        SingleOutputStreamOperator<Tuple2<String,Integer>> result= dateStream
                .flatMap(new SplitTask())
                .keyBy(tuple->tuple.f0)
                .sum(1);
        //步骤四，数据的输出
        result.print();
        //步骤五，任务的启动
        env.execute("wordCount");
    }
    //定义一个计算函数
    public static class SplitTask implements FlatMapFunction<String,Tuple2<String,Integer>>{

        @Override
        public void flatMap(String s, Collector<Tuple2<String,Integer>> collector) throws Exception {
            String[] fields=s.split(",");
            for(String word:fields){
                collector.collect(Tuple2.of(word,1));
            }
        }
    }
    //封装的输出类型对象。新版本自定义不行
    public static class ResultObject<T> {
        public T getParam1() {
            return param1;
        }

        public void setParam1(T param1) {
            this.param1 = param1;
        }

        public T getParam2() {
            return param2;
        }

        public void setParam2(T param2) {
            this.param2 = param2;
        }

        public T getParam3() {
            return param3;
        }

        public void setParam3(T param3) {
            this.param3 = param3;
        }

        public T getParam4() {
            return param4;
        }

        public void setParam4(T param4) {
            this.param4 = param4;
        }

        public T getParam5() {
            return param5;
        }

        public void setParam5(T param5) {
            this.param5 = param5;
        }

        public T getParam6() {
            return param6;
        }

        public void setParam6(T param6) {
            this.param6 = param6;
        }

        private T param1;
        private T param2;
        private T param3;
        private T param4;
        private T param5;
        private T param6;

        public ResultObject(T param1, T param2) {
            this.param1 = param1;
            this.param2 = param2;
        }

        public ResultObject(T param1, T param2, T param3, T param4, T param5, T param6) {
            this.param1 = param1;
            this.param2 = param2;
            this.param3 = param3;
            this.param4 = param4;
            this.param5 = param5;
            this.param6 = param6;
        }

        @Override
        public String toString() {
            return "ResultObject{" +
                    "param1=" + param1 +
                    ", param2=" + param2 +
                    ", param3=" + param3 +
                    ", param4=" + param4 +
                    ", param5=" + param5 +
                    ", param6=" + param6 +
                    '}';
        }
    }
}
