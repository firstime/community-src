package com.lanyu.community.test;
public class Factory {
    
    public static Object createObj(String className) throws Exception{
            return Class.forName(className).newInstance();
    }
}