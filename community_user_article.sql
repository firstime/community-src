/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : community

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2021-09-30 04:12:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `community_user_article`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_article`;
CREATE TABLE `community_user_article` (
  `pid` varchar(32) NOT NULL COMMENT '文章ID',
  `uid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `p_title` varchar(100) DEFAULT NULL COMMENT '文章标题',
  `p_message` varchar(1000) DEFAULT NULL COMMENT '文章信息',
  `p_tags` varchar(100) DEFAULT NULL COMMENT '文章标签',
  `p_sendtime` datetime DEFAULT NULL COMMENT '发布时间',
  `p_type` int(2) DEFAULT NULL COMMENT '文章分类',
  `p_isdel` int(2) DEFAULT NULL COMMENT '是否删除',
  `p_hotscore` int(10) DEFAULT NULL COMMENT '文章热度观看数',
  `p_topscore` int(10) DEFAULT NULL COMMENT '排行热度推荐学习值',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块文章表';

-- ----------------------------
-- Records of community_user_article
-- ----------------------------

-- ----------------------------
-- Table structure for `community_user_chat`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_chat`;
CREATE TABLE `community_user_chat` (
  `chat_id` varchar(32) DEFAULT NULL,
  `from_uid` varchar(32) DEFAULT NULL,
  `to_uid` varchar(32) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `type` int(1) DEFAULT '0' COMMENT '消息类型0文字1图片2视频3语音'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of community_user_chat
-- ----------------------------
INSERT INTO `community_user_chat` VALUES ('337f60c62c888e1d8f3919b8bc13c5c6', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', '你好', '2021-09-26 06:23:08', '0');
INSERT INTO `community_user_chat` VALUES ('887a99936d27dbd096fcc44657d2b3ac', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', '额谁说的', '2021-09-26 06:28:15', '0');
INSERT INTO `community_user_chat` VALUES ('49a6e6823e0c72192b1a59ee18b16043', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', '额谁说的发v', '2021-09-26 06:28:19', '0');
INSERT INTO `community_user_chat` VALUES ('1c6f27ee0f3d9121c8e4e231fd2f4f43', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 06:28:51', '0');
INSERT INTO `community_user_chat` VALUES ('ea9830d753f8e91b7326bc736f219ac4', 'dc6e0c1be91f392055388db72e8d1366', 'e444b3c199036739d6118db8ab6ecad0', '你好', '2021-09-26 06:31:13', '0');
INSERT INTO `community_user_chat` VALUES ('2b778f79062987cf17f8d240b09c1280', 'dc6e0c1be91f392055388db72e8d1366', 'e444b3c199036739d6118db8ab6ecad0', '菲菲', '2021-09-26 06:31:40', '0');
INSERT INTO `community_user_chat` VALUES ('1b5a50c8c76fe5e0e1d0bae4d2a0ef09', 'dc6e0c1be91f392055388db72e8d1366', '674b126fec114cdd6a30b1c20f78b337', '菲菲', '2021-09-26 06:31:45', '0');
INSERT INTO `community_user_chat` VALUES ('afc9549308e074e71716ee76cd87700d', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '你好', '2021-09-26 06:43:45', '0');
INSERT INTO `community_user_chat` VALUES ('cd6fa6ce63d30f3075bbb0166ea9caab', 'dc6e0c1be91f392055388db72e8d1366', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 06:43:50', '0');
INSERT INTO `community_user_chat` VALUES ('7521790652f261d3c83d3f41f96c5e79', 'dc6e0c1be91f392055388db72e8d1366', 'fa4029a1c16e5bb1715982e1bb095b14', '菲菲', '2021-09-26 06:44:38', '0');
INSERT INTO `community_user_chat` VALUES ('250163608d8e70d495dfb7e49ed9928d', 'dc6e0c1be91f392055388db72e8d1366', 'fa4029a1c16e5bb1715982e1bb095b14', '菲菲', '2021-09-26 06:47:15', '0');
INSERT INTO `community_user_chat` VALUES ('e3ef2eb18ecf76d6ae6e46059dcc5c7c', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲q', '2021-09-26 06:47:33', '0');
INSERT INTO `community_user_chat` VALUES ('1fcf1dee50a1b020329efb123722ff26', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲q', '2021-09-26 06:48:00', '0');
INSERT INTO `community_user_chat` VALUES ('d829381a035089c13802fb3a272fc46a', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲qcc', '2021-09-26 06:51:07', '0');
INSERT INTO `community_user_chat` VALUES ('9dcac86e300e261d710cbfbd89455c8e', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲qcc', '2021-09-26 07:02:52', '0');
INSERT INTO `community_user_chat` VALUES ('e02b4a72432a0bf9b008c365ad1911ab', 'dc6e0c1be91f392055388db72e8d1366', 'fa4029a1c16e5bb1715982e1bb095b14', '菲菲qcc', '2021-09-26 07:03:15', '0');
INSERT INTO `community_user_chat` VALUES ('3027c3893f19d65a69bcc11d9bcd4677', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲qcc', '2021-09-26 07:04:17', '0');
INSERT INTO `community_user_chat` VALUES ('1eccf294a15d77cb42d232713cb6aa48', 'dc6e0c1be91f392055388db72e8d1366', '674b126fec114cdd6a30b1c20f78b337', '菲菲qcc', '2021-09-26 07:04:37', '0');
INSERT INTO `community_user_chat` VALUES ('884b4efc8fe78143edf43cf2f1619528', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲qcc', '2021-09-26 07:11:58', '0');
INSERT INTO `community_user_chat` VALUES ('4e7362ada6e9aabe5e4b7c5610ad2dff', 'dc6e0c1be91f392055388db72e8d1366', 'fa4029a1c16e5bb1715982e1bb095b14', '菲菲qcc', '2021-09-26 07:15:41', '0');
INSERT INTO `community_user_chat` VALUES ('8716c41e45da0a433674c09f46aa9e04', 'dc6e0c1be91f392055388db72e8d1366', '674b126fec114cdd6a30b1c20f78b337', '菲菲qcc', '2021-09-26 07:15:54', '0');
INSERT INTO `community_user_chat` VALUES ('d45c07ada1772ea2ea9d339cc2fd39a2', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲qcc', '2021-09-26 07:16:20', '0');
INSERT INTO `community_user_chat` VALUES ('931d3fc3cc6600045838aaf310e6977f', 'dc6e0c1be91f392055388db72e8d1366', 'e444b3c199036739d6118db8ab6ecad0', '菲菲qcc', '2021-09-26 07:16:28', '0');
INSERT INTO `community_user_chat` VALUES ('8ad6c0ebfeb68bd959a729dbd940109d', 'dc6e0c1be91f392055388db72e8d1366', 'fa4029a1c16e5bb1715982e1bb095b14', '菲菲qcc', '2021-09-26 07:16:40', '0');
INSERT INTO `community_user_chat` VALUES ('2c6980350d439ead6324a50ede097fc9', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '菲菲qcc', '2021-09-26 07:17:31', '0');
INSERT INTO `community_user_chat` VALUES ('0581802d22cb2e9a5fbe5e3b7e933a89', 'dc6e0c1be91f392055388db72e8d1366', 'fa4029a1c16e5bb1715982e1bb095b14', '菲菲qcc', '2021-09-26 07:17:43', '0');
INSERT INTO `community_user_chat` VALUES ('bfa7a101fb6fa08a7e2b83a877a77d68', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', 'hshah', '2021-09-26 07:18:36', '0');
INSERT INTO `community_user_chat` VALUES ('5767c0a8e6039fb15a28628ee9e71628', '3a8c3d78a575765bba49326e605fe4b2', 'dc6e0c1be91f392055388db72e8d1366', '你好', '2021-09-26 07:19:46', '0');
INSERT INTO `community_user_chat` VALUES ('2f1209f1eb779611df2aa0eff8e6576e', 'dc6e0c1be91f392055388db72e8d1366', '3a8c3d78a575765bba49326e605fe4b2', 'hzh', '2021-09-26 07:20:47', '0');
INSERT INTO `community_user_chat` VALUES ('ea4a06e77a64418f02e1e6189d05b7ae', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'cc', '2021-09-26 07:55:24', '0');
INSERT INTO `community_user_chat` VALUES ('d129fde60b5763c08eaae3ece8b63e63', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:55:28', '0');
INSERT INTO `community_user_chat` VALUES ('b466940630b0a19fbde30718fb47d671', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:16', '0');
INSERT INTO `community_user_chat` VALUES ('30b34696f53a611273437adc4fe6c2e6', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:17', '0');
INSERT INTO `community_user_chat` VALUES ('80f365155b4a68912a870d10c87894b0', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:18', '0');
INSERT INTO `community_user_chat` VALUES ('460230fc8697208e4ab0ffbe8b7dc2a6', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:18', '0');
INSERT INTO `community_user_chat` VALUES ('9d9c6c2596a91791b404c618f6d2bbe1', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:18', '0');
INSERT INTO `community_user_chat` VALUES ('7aa83272f782acdbb22f2a6058878240', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:18', '0');
INSERT INTO `community_user_chat` VALUES ('be313a9c7954eb0dbc7e1c9ca878b9e9', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:18', '0');
INSERT INTO `community_user_chat` VALUES ('63b89a169a7947abdb4fb52eef4636db', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:18', '0');
INSERT INTO `community_user_chat` VALUES ('56b844836541f1901d8f573f7f19f424', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:19', '0');
INSERT INTO `community_user_chat` VALUES ('6d95cad7036b1e9f77969d52368c45ff', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:19', '0');
INSERT INTO `community_user_chat` VALUES ('83a1fc28d7a5093fed5196369c36240c', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:19', '0');
INSERT INTO `community_user_chat` VALUES ('85375b08e39fb1ead03ede53efbbd89d', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:56:19', '0');
INSERT INTO `community_user_chat` VALUES ('093eea535b9f154ab2b89475bc08dac6', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:59:39', '0');
INSERT INTO `community_user_chat` VALUES ('c0de7f2f3fc6023f2d657a1130dc067f', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:59:45', '0');
INSERT INTO `community_user_chat` VALUES ('83e922a913459f808a2962e3ffa058de', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:59:47', '0');
INSERT INTO `community_user_chat` VALUES ('5e8c69a23d68de699291acb05bceaa46', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 07:59:50', '0');
INSERT INTO `community_user_chat` VALUES ('8bd830858bb4aae443b71668576c4701', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'ccvvv', '2021-09-26 08:01:01', '0');
INSERT INTO `community_user_chat` VALUES ('f08305920638df1b2149f35996440e38', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', 'vv', '2021-09-26 08:02:14', '0');
INSERT INTO `community_user_chat` VALUES ('c203702a6cba6c991562c416a118c659', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', 'ddd', '2021-09-26 08:02:21', '0');
INSERT INTO `community_user_chat` VALUES ('59ec63f99acf549ca7d330ba682a8960', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', 'dccv', '2021-09-26 08:02:27', '0');
INSERT INTO `community_user_chat` VALUES ('3f5c41194ed202b2106c4c9c0addca3c', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', '', '2021-09-26 08:02:30', '0');
INSERT INTO `community_user_chat` VALUES ('c29ac40b5649e0329fb61b0ad803ea97', '674b126fec114cdd6a30b1c20f78b337', 'dc6e0c1be91f392055388db72e8d1366', '', '2021-09-26 08:02:33', '0');
INSERT INTO `community_user_chat` VALUES ('523bb97bdb4f253719ed746b23a04aeb', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'jjhh', '2021-09-26 08:06:55', '0');
INSERT INTO `community_user_chat` VALUES ('2549db05f2174878c06f4a6c162fa285', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'jb', '2021-09-26 08:08:35', '0');
INSERT INTO `community_user_chat` VALUES ('06d98990d25094da4b4585f8d8fce109', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'vvd', '2021-09-26 08:08:46', '0');
INSERT INTO `community_user_chat` VALUES ('991c096861724989d4050a7cd7145334', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', 'vvvv', '2021-09-26 08:08:49', '0');
INSERT INTO `community_user_chat` VALUES ('9b41a5bd8f9a7681486cf58844a56d19', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '才吃饭', '2021-09-26 08:08:55', '0');
INSERT INTO `community_user_chat` VALUES ('d35f75b470b90b77a2bd4f22848ad7cf', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '扭扭捏捏', '2021-09-26 08:13:21', '0');
INSERT INTO `community_user_chat` VALUES ('8ff437efd13d187d6c90a10cc35ca204', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '叮当响', '2021-09-26 08:13:25', '0');
INSERT INTO `community_user_chat` VALUES ('406a0091afe153457bf7031a259872b8', '674b126fec114cdd6a30b1c20f78b337', '44444444444444', '你好', '2021-09-26 08:14:33', '0');
INSERT INTO `community_user_chat` VALUES ('9c1de40908caebcf0ee185f85eec7703', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 12:52:56', '0');
INSERT INTO `community_user_chat` VALUES ('af5d7cdec970bd4c4dcd78cbbc17b901', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 12:53:10', '0');
INSERT INTO `community_user_chat` VALUES ('8f90748ddf8dc3d3891b772df5dc74da', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 12:53:12', '0');
INSERT INTO `community_user_chat` VALUES ('ef648bc47107c874aca7f3e07670a63f', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 13:08:35', '0');
INSERT INTO `community_user_chat` VALUES ('b6e0ab63d5e657dbadedaf4786f6cd7b', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 13:08:36', '0');
INSERT INTO `community_user_chat` VALUES ('b4b0077cae7aee090ed9156c2b08dd4d', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 13:08:37', '0');
INSERT INTO `community_user_chat` VALUES ('39c3d2c80293c3372965a43ae227179f', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 13:08:38', '0');
INSERT INTO `community_user_chat` VALUES ('a7750299b94101e6f921dfa8de5d89f7', '674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '你好', '2021-09-26 13:08:40', '0');
INSERT INTO `community_user_chat` VALUES ('50da574390f303f462ebe625ecdbc814', '674b126fec114cdd6a30b1c20f78b337', '62fa7cae653e4051abe917d18cbb59d0', '你好', '2021-09-26 13:22:08', '0');
INSERT INTO `community_user_chat` VALUES ('4ad5ec227724fdb4f429774b3d2e7e03', '674b126fec114cdd6a30b1c20f78b337', '62fa7cae653e4051abe917d18cbb59d0', '就是', '2021-09-26 13:29:39', '0');
INSERT INTO `community_user_chat` VALUES ('0e159b77d62a7ba0ae02d0112abbd2d8', '674b126fec114cdd6a30b1c20f78b337', 'e444b3c199036739d6118db8ab6ecad0', '你好', '2021-09-26 13:36:12', '0');
INSERT INTO `community_user_chat` VALUES ('cc4a823eb100f8529b0ab14ed78734eb', 'fa4029a1c16e5bb1715982e1bb095b14', 'e444b3c199036739d6118db8ab6ecad0', '你好', '2021-09-26 13:45:39', '0');
INSERT INTO `community_user_chat` VALUES ('cb40a1183d09ae611efd7d336d3a718d', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '你好', '2021-09-26 14:09:25', '0');
INSERT INTO `community_user_chat` VALUES ('fd6d5dcd07ea38c1962b896e66aa70b7', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '兄弟', '2021-09-26 14:10:26', '0');
INSERT INTO `community_user_chat` VALUES ('cb27a6d153b5d00be0634ac96b0351ac', 'e444b3c199036739d6118db8ab6ecad0', '44444444444444', '你好，我是红娘，请收藏我', '2021-09-26 14:36:34', '0');
INSERT INTO `community_user_chat` VALUES ('4ce369162947a0344aca5bcff015b7c3', 'e444b3c199036739d6118db8ab6ecad0', '674b126fec114cdd6a30b1c20f78b337', '你好，我是红娘，请收藏我', '2021-09-26 14:48:31', '0');
INSERT INTO `community_user_chat` VALUES ('440ebdbfdae78eed5c1d1073b081939c', 'e444b3c199036739d6118db8ab6ecad0', 'fa4029a1c16e5bb1715982e1bb095b14', '你好，我是红娘，请收藏我', '2021-09-26 15:28:44', '0');
INSERT INTO `community_user_chat` VALUES ('1e81c675f3aa2891300d488f955ee954', 'e444b3c199036739d6118db8ab6ecad0', 'dc6e0c1be91f392055388db72e8d1366', '你好，我是红娘，请收藏我', '2021-09-26 15:30:00', '0');
INSERT INTO `community_user_chat` VALUES ('ebe217034679c730e1bb30f884b4aee1', 'fa4029a1c16e5bb1715982e1bb095b14', 'e444b3c199036739d6118db8ab6ecad0', '还是', '2021-09-26 16:21:17', '0');
INSERT INTO `community_user_chat` VALUES ('a4eaa9268eb49074c0ab072c91d1e89a', 'fa4029a1c16e5bb1715982e1bb095b14', '62fa7cae653e4051abe917d18cbb59d0', '哈卡', '2021-09-26 16:22:17', '0');
INSERT INTO `community_user_chat` VALUES ('206da898686fad9f31c98b8591e7058d', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '什么时间见面', '2021-09-26 16:27:09', '0');
INSERT INTO `community_user_chat` VALUES ('58bcf04e1d5987d79ea8107dce827a3f', '674b126fec114cdd6a30b1c20f78b337', 'e444b3c199036739d6118db8ab6ecad0', '分', '2021-09-26 22:45:11', '0');
INSERT INTO `community_user_chat` VALUES ('55aa17fae07a1ab7b34ae605bf13cb2b', '674b126fec114cdd6a30b1c20f78b337', '53651178f09d6a52fbac415ac8681d66', '你好，我是红娘，请收藏我', '2021-09-29 03:31:12', '0');
INSERT INTO `community_user_chat` VALUES ('f4d0297e6b9095b79f97d58c0b6cb291', 'ab83682fb12e295829b1bffa236530ea', '53651178f09d6a52fbac415ac8681d66', '你好', '2021-09-29 16:57:54', '0');
INSERT INTO `community_user_chat` VALUES ('09bc274c6f66b88c1eee4df136b37d0c', 'e887f89db9c2436a886824b00bf581bd', '674b126fec114cdd6a30b1c20f78b337', '你好', '2021-09-29 17:23:50', '0');
INSERT INTO `community_user_chat` VALUES ('56d0916af9d96bf19dadf571a39c2d05', '674b126fec114cdd6a30b1c20f78b337', '032602991210dd644be04f16f2aea95d', '你好，我是红娘，请收藏我', '2021-09-29 21:38:57', '0');
INSERT INTO `community_user_chat` VALUES ('b4d91c5310085134924c3d3d7b5ec3ea', '674b126fec114cdd6a30b1c20f78b337', '032602991210dd644be04f16f2aea95d', '你好', '2021-09-29 21:39:28', '0');
INSERT INTO `community_user_chat` VALUES ('bbdfa0468a7bcb00eeb79b9906fd5aa3', '032602991210dd644be04f16f2aea95d', '674b126fec114cdd6a30b1c20f78b337', '在吗', '2021-09-29 21:39:31', '0');
INSERT INTO `community_user_chat` VALUES ('d2c4178d2ca5296462de1157cadc846b', '674b126fec114cdd6a30b1c20f78b337', '032602991210dd644be04f16f2aea95d', '1', '2021-09-29 21:40:05', '0');
INSERT INTO `community_user_chat` VALUES ('25e309f519f1a47bafedc3075a2c04e8', '674b126fec114cdd6a30b1c20f78b337', '032602991210dd644be04f16f2aea95d', '留', '2021-09-29 21:40:23', '0');
INSERT INTO `community_user_chat` VALUES ('405584f45f941bcbb02da3c1d011f6c0', '032602991210dd644be04f16f2aea95d', '674b126fec114cdd6a30b1c20f78b337', '在', '2021-09-29 21:40:31', '0');
INSERT INTO `community_user_chat` VALUES ('d38ca81199ab7d230b56867c41823cfd', '032602991210dd644be04f16f2aea95d', 'c80ab681709279b7e59bf6451164d4f0', '你好', '2021-09-29 22:15:34', '0');
INSERT INTO `community_user_chat` VALUES ('41c98529bb621185b7c8dab31014e6f1', '032602991210dd644be04f16f2aea95d', 'e887f89db9c2436a886824b00bf581bd', '你好', '2021-09-29 22:15:48', '0');
INSERT INTO `community_user_chat` VALUES ('75a2ff16df6dc3ab751d6c81b09a6b5d', '674b126fec114cdd6a30b1c20f78b337', '032602991210dd644be04f16f2aea95d', '1', '2021-09-29 22:19:04', '0');
INSERT INTO `community_user_chat` VALUES ('2457348f8101985dccba67516dd84d9f', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '1', '2021-09-29 22:20:13', '0');
INSERT INTO `community_user_chat` VALUES ('bd0a3000351e380a27f5b5f02b7d25eb', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '2', '2021-09-29 22:21:15', '0');
INSERT INTO `community_user_chat` VALUES ('990834e85059fadba669d690c8d80f8a', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '在', '2021-09-29 22:57:31', '0');
INSERT INTO `community_user_chat` VALUES ('b0cf58e887d7f534a29d94cad78c56b8', '674b126fec114cdd6a30b1c20f78b337', '62fa7cae653e4051abe917d18cbb59d0', '得得', '2021-09-29 23:02:45', '0');
INSERT INTO `community_user_chat` VALUES ('b907b01d9efe60866903097ccf0f7217', '674b126fec114cdd6a30b1c20f78b337', '62fa7cae653e4051abe917d18cbb59d0', '想', '2021-09-29 23:05:48', '0');
INSERT INTO `community_user_chat` VALUES ('254e41511693d89e20e6c69094399799', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '是', '2021-09-29 23:15:50', '0');
INSERT INTO `community_user_chat` VALUES ('876d4fe9bc621ae92ef3b29c2c3e280d', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '得得', '2021-09-29 23:20:57', '0');
INSERT INTO `community_user_chat` VALUES ('36fa3c8186f7dcaf7b984bb96d07565b', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '菲菲', '2021-09-29 23:21:06', '0');
INSERT INTO `community_user_chat` VALUES ('1fac6ed0cd9f1c9ca444bd82fe3624d2', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '得得', '2021-09-29 23:21:11', '0');
INSERT INTO `community_user_chat` VALUES ('cbcdcf87d8632f21f473505ff5ca7f39', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '奋斗', '2021-09-29 23:21:23', '0');
INSERT INTO `community_user_chat` VALUES ('faa68addc59fc18fef5f54483f4fe8a7', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '得得', '2021-09-29 23:21:40', '0');
INSERT INTO `community_user_chat` VALUES ('acfc653d1d933c6766d5f9d264c66c3a', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '1', '2021-09-29 23:22:28', '0');
INSERT INTO `community_user_chat` VALUES ('12b487a5c774a53f9e9fc2dec6e495d1', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '2', '2021-09-29 23:22:50', '0');
INSERT INTO `community_user_chat` VALUES ('13847d3b1f7419ea5e5c3e50a9a134b7', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '3', '2021-09-29 23:24:36', '0');
INSERT INTO `community_user_chat` VALUES ('a2a1155bb8b044e7b0efaa6bce63207d', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '4', '2021-09-29 23:26:12', '0');
INSERT INTO `community_user_chat` VALUES ('0800679aad7d9f7cf01ddcdfe74151d9', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '的', '2021-09-29 23:26:32', '0');
INSERT INTO `community_user_chat` VALUES ('19c99a5bbb0b12674c8b183bb9732de2', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '，', '2021-09-29 23:28:55', '0');
INSERT INTO `community_user_chat` VALUES ('a35d2b5429891928eb66b77f4cf9a895', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '吃饭', '2021-09-29 23:30:47', '0');
INSERT INTO `community_user_chat` VALUES ('0bf68dec2a57db82a108f5400fbc8577', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '兄弟', '2021-09-29 23:31:17', '0');
INSERT INTO `community_user_chat` VALUES ('97557e170c7ed8f6eb40f3275875190d', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '得得', '2021-09-29 23:31:24', '0');
INSERT INTO `community_user_chat` VALUES ('536c3fb54bd69fc57f1d16a2e5ff4b20', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '尺寸', '2021-09-29 23:32:12', '0');
INSERT INTO `community_user_chat` VALUES ('8fe737bb66f391ed00d5683c22f49ca2', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '低调低调', '2021-09-29 23:32:40', '0');
INSERT INTO `community_user_chat` VALUES ('a6c5570bd05f88398050546c46b22a9d', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '菲菲', '2021-09-29 23:34:58', '0');
INSERT INTO `community_user_chat` VALUES ('eb12d05f0aef629b170b85aa83963574', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '彻底', '2021-09-29 23:35:04', '0');
INSERT INTO `community_user_chat` VALUES ('61da56a7cb29ee23626d258aeb1d72bf', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '发发发', '2021-09-29 23:36:28', '0');
INSERT INTO `community_user_chat` VALUES ('f265eee01c41e3cb38e2906743a583d2', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '奋斗', '2021-09-29 23:37:48', '0');
INSERT INTO `community_user_chat` VALUES ('cad9da0fe70a492d637ba97676a6402d', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '奋斗', '2021-09-29 23:38:24', '0');
INSERT INTO `community_user_chat` VALUES ('abd59fb4f2f83fe75f5084c7ca4a9959', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '得得', '2021-09-29 23:38:35', '0');
INSERT INTO `community_user_chat` VALUES ('25ae1384528076ef91d3f980412a030d', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '菲菲', '2021-09-29 23:39:38', '0');
INSERT INTO `community_user_chat` VALUES ('740e1b3f5c9c3a694dff1277ca90492e', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', 'vv的', '2021-09-29 23:40:20', '0');
INSERT INTO `community_user_chat` VALUES ('1544c38b40ab56fa3858c8e94a8bebca', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '吃饭', '2021-09-29 23:41:32', '0');
INSERT INTO `community_user_chat` VALUES ('0f382b7bb2339d7f442bc99b31681bf6', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '兄弟', '2021-09-29 23:43:05', '0');
INSERT INTO `community_user_chat` VALUES ('6d767563dcd469c6f813d1d2411df6c0', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '得得', '2021-09-29 23:51:37', '0');
INSERT INTO `community_user_chat` VALUES ('c0268001c082e50aa896441a7e2b2126', '674b126fec114cdd6a30b1c20f78b337', 'e887f89db9c2436a886824b00bf581bd', '得得', '2021-09-29 23:53:33', '0');
INSERT INTO `community_user_chat` VALUES ('e86ca4290e40cb2ce4c8cc3635fb4ef3', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '你好，我是红娘，请收藏我', '2021-09-30 00:01:26', '0');
INSERT INTO `community_user_chat` VALUES ('bdb40904defccc0c926c4ac3aeb24993', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '不会', '2021-09-30 00:01:41', '0');
INSERT INTO `community_user_chat` VALUES ('ccc6c1d117e902984afa4d5a45e0d21b', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '怎了', '2021-09-30 00:01:52', '0');
INSERT INTO `community_user_chat` VALUES ('0963d89045712937c91289b0d0f03006', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '在吗', '2021-09-30 00:01:57', '0');
INSERT INTO `community_user_chat` VALUES ('f23bd5ae30f94ddaed0b6d5568556666', 'c80ab681709279b7e59bf6451164d4f0', '674b126fec114cdd6a30b1c20f78b337', '5555', '2021-09-30 00:02:07', '0');
INSERT INTO `community_user_chat` VALUES ('81c101c7070851f0581b7f56de5f0c9f', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '干干干', '2021-09-30 00:03:17', '0');
INSERT INTO `community_user_chat` VALUES ('a45f6fca50c250478c90674007c109c2', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '更丰富', '2021-09-30 00:04:19', '0');
INSERT INTO `community_user_chat` VALUES ('15d61be5f1bab087c70fda5bfe80be7b', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '哈哈哈', '2021-09-30 00:04:24', '0');
INSERT INTO `community_user_chat` VALUES ('f4203082c39411a5a1ac59c23477f413', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '过过瘾', '2021-09-30 00:04:31', '0');
INSERT INTO `community_user_chat` VALUES ('b40aa45a19498d17044e430189dcb65c', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '哈哈', '2021-09-30 00:04:40', '0');
INSERT INTO `community_user_chat` VALUES ('2c9f1c926edf509f138e9b80bf1055a0', 'c80ab681709279b7e59bf6451164d4f0', '674b126fec114cdd6a30b1c20f78b337', 'GG', '2021-09-30 00:11:53', '0');
INSERT INTO `community_user_chat` VALUES ('4ff74a79bf1a31506f54ddcc4c04c66f', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '大十岁', '2021-09-30 00:20:54', '0');
INSERT INTO `community_user_chat` VALUES ('78d4ee9d005b2ca857784dc38b8d8ca7', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '除非', '2021-09-30 00:20:57', '0');
INSERT INTO `community_user_chat` VALUES ('ea1311eaedd51a73b100eb1231c3c10c', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '房费', '2021-09-30 00:21:16', '0');
INSERT INTO `community_user_chat` VALUES ('6540249ab6fb79b56d6e2b05d75cbe67', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '丰富的', '2021-09-30 00:21:21', '0');
INSERT INTO `community_user_chat` VALUES ('c9aa6cee2e8b3f60e7725ed04d0e4d37', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '菲菲', '2021-09-30 00:21:26', '0');
INSERT INTO `community_user_chat` VALUES ('ff65602ed3786f4d191aab49f2e3e5c3', '674b126fec114cdd6a30b1c20f78b337', 'c80ab681709279b7e59bf6451164d4f0', '房费', '2021-09-30 00:21:39', '0');
INSERT INTO `community_user_chat` VALUES ('67f14c381cb70e9c7c6ff97045d91426', 'c80ab681709279b7e59bf6451164d4f0', '032602991210dd644be04f16f2aea95d', '5555', '2021-09-30 01:36:38', '0');

-- ----------------------------
-- Table structure for `community_user_discuss`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_discuss`;
CREATE TABLE `community_user_discuss` (
  `disc_id` varchar(32) NOT NULL COMMENT '评论ID',
  `disc_did` varchar(32) DEFAULT NULL COMMENT '动态ID',
  `disc_pid` varchar(32) DEFAULT NULL COMMENT '文章ID',
  `disc_uid` varchar(32) DEFAULT NULL COMMENT '评论用户ID',
  `disc_at_uid` varchar(32) DEFAULT NULL COMMENT 'AT的用户ID',
  `disc_message` varchar(200) DEFAULT NULL COMMENT '评论信息',
  `disc_creatime` datetime DEFAULT NULL COMMENT '创建时间',
  `disc_huifu_discid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`disc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='动态评论表';

-- ----------------------------
-- Records of community_user_discuss
-- ----------------------------
INSERT INTO `community_user_discuss` VALUES ('0099c5389845f0957250e311302946a8', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '单刀队', '2021-09-13 02:09:27', '');
INSERT INTO `community_user_discuss` VALUES ('014ed150577c5b16f219eb68f5f6c884', '0032291325ac105c69aa843d8e3c9966', '', '674b126fec114cdd6a30b1c20f78b337', '674b126fec114cdd6a30b1c20f78b337', '多少啊', '2021-09-24 18:51:25', 'f469072d6b1557ec8e0edfdfd2fd66f7');
INSERT INTO `community_user_discuss` VALUES ('015b1b5ee2539d5b45c52e3990141d60', '38a326c690dbce695cb099c192b6c06f', '', '674b126fec114cdd6a30b1c20f78b337', '674b126fec114cdd6a30b1c20f78b337', '染发膏', '2021-09-12 20:20:45', '36860501ce661d800032ef3b16ec73b5');
INSERT INTO `community_user_discuss` VALUES ('0226d780884a5fcf30e716fab423750f', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:03', null);
INSERT INTO `community_user_discuss` VALUES ('055509d08d8fb9726d1dfdae7bd044e8', 'e4a7afa9644293573b428b8714759e2a', '', '44444444444444', '', '哈哈', '2020-01-08 04:30:19', null);
INSERT INTO `community_user_discuss` VALUES ('072c8e6e6ce96610bb3ae58ea67b3ba1', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多877777888-----', '2019-12-06 17:33:51', null);
INSERT INTO `community_user_discuss` VALUES ('0a49dc3385e30895752676e7f42bd78b', 'e4a7afa9644293573b428b8714759e2a', '', '44444444444444', '44444444444444', '怎么了', '2020-01-08 04:48:25', null);
INSERT INTO `community_user_discuss` VALUES ('0f8e5860acf70d4be0f4b91cff252196', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 17:43:58', null);
INSERT INTO `community_user_discuss` VALUES ('10b981f796c24c4fe27fb16d5a327348', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:16:26', null);
INSERT INTO `community_user_discuss` VALUES ('10ef27fe62bb5ea8e8f7aea074600f91', 'ddba612f729bc9a3eb55e787b18cf77b', '', 'fa4029a1c16e5bb1715982e1bb095b14', 'fa4029a1c16e5bb1715982e1bb095b14', '彻底', '2021-08-27 12:59:42', null);
INSERT INTO `community_user_discuss` VALUES ('11111', '04424c3597db40099c9cb0de3bec71d1', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '热热我', '2019-12-06 14:14:46', null);
INSERT INTO `community_user_discuss` VALUES ('111618ed3b2e0e1cda990a17c0f9b8c3', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 17:47:13', null);
INSERT INTO `community_user_discuss` VALUES ('15fd5a211bfdde01dbdafcaf88730273', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:04', null);
INSERT INTO `community_user_discuss` VALUES ('171f2829285d9ddbfb65c1d4bd8bf857', '7dddc8bab34f9a3217cf24ad5a1154e1', '', '674b126fec114cdd6a30b1c20f78b337', '', '', '2021-09-24 17:53:15', '');
INSERT INTO `community_user_discuss` VALUES ('17f8f616930908afc3f7eadd889d90a8', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:25:33', null);
INSERT INTO `community_user_discuss` VALUES ('185f3674f5a0b248b2514aa888832cd5', '2fb74f2f34d8971353cd60142ca533ca', '', '032602991210dd644be04f16f2aea95d', '', '你好', '2021-09-29 22:17:07', '');
INSERT INTO `community_user_discuss` VALUES ('1880489ab312af20b3dcdd69e0359cd7', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 21:55:03', null);
INSERT INTO `community_user_discuss` VALUES ('1a0d96b941ac0db0a42b503e403d7614', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '411234', '2021-09-13 02:15:18', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('1a38c5696e34c089c777c64ca7ae455e', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '4', '2021-09-13 02:14:43', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('1c343c8177e4741a2b54f6952e55feaa', 'bceaeeced3c11c6f9c91b28b91f8ea2c', '', '674b126fec114cdd6a30b1c20f78b337', '', '洗香香', '2021-09-24 18:54:31', '');
INSERT INTO `community_user_discuss` VALUES ('1ca5e335e78baa51414fc5e57caa5407', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:25:34', null);
INSERT INTO `community_user_discuss` VALUES ('1fc821491c6678656b9be0605bb8d8f0', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:25:34', null);
INSERT INTO `community_user_discuss` VALUES ('20cbc5fc82d0b09220e1ad82ba59a5a5', '2fb74f2f34d8971353cd60142ca533ca', '', '032602991210dd644be04f16f2aea95d', '674b126fec114cdd6a30b1c20f78b337', '在吗', '2021-09-29 22:16:43', 'fea07d9fcac504ddbf6daefe6ee79b6b');
INSERT INTO `community_user_discuss` VALUES ('2222', '04424c3597db40099c9cb0de3bec71d1', null, '41479828b219239398cc75363fc6fa9f', null, '特温特we', '2019-12-06 14:15:02', null);
INSERT INTO `community_user_discuss` VALUES ('2346e73bdc1c3d241f25035fa10a32cd', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 21:55:02', null);
INSERT INTO `community_user_discuss` VALUES ('240b50be22d0b2c7d75b0e50b639767a', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多877777888-----', '2019-12-06 17:30:21', null);
INSERT INTO `community_user_discuss` VALUES ('28a021a5be9f2c6e727725667e501e98', '38a326c690dbce695cb099c192b6c06f', '', '674b126fec114cdd6a30b1c20f78b337', '', '大姐大姐吃饭的', '2021-09-21 01:02:35', '');
INSERT INTO `community_user_discuss` VALUES ('28ecaef8afe39cf2ce888ee8f0ec20b3', '1b0906665a09a6dde23f4aa15be503cf', '', 'fa4029a1c16e5bb1715982e1bb095b14', 'fa4029a1c16e5bb1715982e1bb095b14', '好啥好刚才', '2021-08-27 12:25:05', null);
INSERT INTO `community_user_discuss` VALUES ('299a7edad78357861dd220c03e180a17', 'd513ac404eed33e73cbb7bd68d2b1d3c', '', '44444444444444', '', '这是WWE吗？', '2020-01-08 04:32:14', null);
INSERT INTO `community_user_discuss` VALUES ('2bfd7d585493a0ddf8682197a9d9e353', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '41123', '2021-09-13 02:15:13', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('313b7124e6ff703e7af809108dd8074b', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:02', null);
INSERT INTO `community_user_discuss` VALUES ('3333', '04424c3597db40099c9cb0de3bec71d1', null, '87b1c6eff2c4a1b98cd381246c89fae8', null, '是上电视大V', '2019-12-06 14:17:10', null);
INSERT INTO `community_user_discuss` VALUES ('3643262f1c21154703b9fd3619520914', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:00', null);
INSERT INTO `community_user_discuss` VALUES ('36860501ce661d800032ef3b16ec73b5', '38a326c690dbce695cb099c192b6c06f', '', '674b126fec114cdd6a30b1c20f78b337', '', 'djsjjs', '2021-09-12 20:16:16', '');
INSERT INTO `community_user_discuss` VALUES ('38649d0689a69b3ca896f8b92f3c652f', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '41', '2021-09-13 02:14:52', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('3a45ea90f9a5519386927c3004642b9f', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '耍我玩', '2021-09-13 02:09:11', '');
INSERT INTO `community_user_discuss` VALUES ('3bac0ff79b8284da7cec5ea36b876cb7', '1b0906665a09a6dde23f4aa15be503cf', '', 'fa4029a1c16e5bb1715982e1bb095b14', '44444444444444', '销售', '2021-08-27 15:09:39', '10ef27fe62bb5ea8e8f7aea074600f91');
INSERT INTO `community_user_discuss` VALUES ('44665a75b7af7e9a1848c2cd02c61aaf', '38a326c690dbce695cb099c192b6c06f', '', '674b126fec114cdd6a30b1c20f78b337', '', '大姐大姐', '2021-09-21 01:02:31', '');
INSERT INTO `community_user_discuss` VALUES ('47134e3f51faad7ce69ce0e7087e5aa1', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '55555', '2021-09-13 02:16:52', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('476a460e8d6a7cc9db05e69a0401d0ce', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:25:34', null);
INSERT INTO `community_user_discuss` VALUES ('4971d722f1a902aae2230b05e8e2e7f9', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 21:57:07', null);
INSERT INTO `community_user_discuss` VALUES ('4aad446533819bc8cb58c39a221894d8', '3054650f520fdb9d07084399812ec897', '', '674b126fec114cdd6a30b1c20f78b337', '', '得得', '2021-09-12 23:30:37', '');
INSERT INTO `community_user_discuss` VALUES ('4bbfb029d1f12a3e1c9b5ef1d8bd7a2f', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '2', '2021-09-12 23:40:35', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('4f5f181567134dab4b23812d8c7419e7', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多8888-----', '2019-12-06 17:21:52', null);
INSERT INTO `community_user_discuss` VALUES ('5ecde6660895e74961324c834fa907c9', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111大萨达撒大所多877777888-----', '2019-12-06 17:35:06', null);
INSERT INTO `community_user_discuss` VALUES ('67ec59690b053c16944f4613649a2d1d', '710dc98f6321b245aade624e970c277d', '', '674b126fec114cdd6a30b1c20f78b337', '674b126fec114cdd6a30b1c20f78b337', '谢谢兄弟', '2021-09-24 18:41:09', '1f17e08895517ce657f5cbe116ae3e2d');
INSERT INTO `community_user_discuss` VALUES ('6dfad922ed43e610a58ee1c30ac67085', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '4112', '2021-09-13 02:15:07', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('7182b8ad7ccf8c821e7ba16e79f1bae7', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:24:58', null);
INSERT INTO `community_user_discuss` VALUES ('73dab06ed8bc63b5c48b3a0a9198403d', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '单刀队单刀队', '2021-09-13 02:09:36', '');
INSERT INTO `community_user_discuss` VALUES ('7a57169c0901e796fbf48cc1c29a9545', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '兄弟', '2021-09-13 01:35:01', '');
INSERT INTO `community_user_discuss` VALUES ('8018ece82e0b6a7d81b6d2b1009db346', '0032291325ac105c69aa843d8e3c9966', '', '674b126fec114cdd6a30b1c20f78b337', '', '吹风扇', '2021-09-24 18:51:07', '');
INSERT INTO `community_user_discuss` VALUES ('89d75ade5ea9dc2054f037c9dc84e39b', '2fb74f2f34d8971353cd60142ca533ca', '', '032602991210dd644be04f16f2aea95d', '', '你好哈', '2021-09-29 22:17:42', '');
INSERT INTO `community_user_discuss` VALUES ('89e882ca5b27e085e1d06b391849a344', '1b0906665a09a6dde23f4aa15be503cf', '', 'fa4029a1c16e5bb1715982e1bb095b14', 'fa4029a1c16e5bb1715982e1bb095b14', '销111111', '2021-08-27 15:10:35', '');
INSERT INTO `community_user_discuss` VALUES ('8a881aebb3ea2733610d7bf38096b34a', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '4112345', '2021-09-13 02:15:23', '36860501ce661d800032ef3b16ec73b5');
INSERT INTO `community_user_discuss` VALUES ('90bd8addb03a7fd6e46072618065f142', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '好吧', '2021-09-13 01:25:03', '');
INSERT INTO `community_user_discuss` VALUES ('9276435eaaf61dcff3ff3a660e733aa7', 'd513ac404eed33e73cbb7bd68d2b1d3c', '', '44444444444444', '', '是', '2020-01-08 04:37:48', null);
INSERT INTO `community_user_discuss` VALUES ('92a7ccfc306b2ec1e57e84ea72e2f4f2', 'bceaeeced3c11c6f9c91b28b91f8ea2c', '', '674b126fec114cdd6a30b1c20f78b337', '', '洗香香', '2021-09-24 18:54:29', '');
INSERT INTO `community_user_discuss` VALUES ('92d2eea5a131852678d1265a9883d20b', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '好的好的好好说说还是炫舞洗马河或许很不撤回试试吧初五呼呼就行对不对就休息结婚的好办法家常菜吃吃吃蔡文姬警察局坚持坚持酒精发酵吃饭跟我说', '2021-09-13 02:25:44', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('94512d09a203a10d4004a04433326e89', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:25:33', null);
INSERT INTO `community_user_discuss` VALUES ('964abf6ab0ad5942393cbdcc1e4f838d', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多8888-----', '2019-12-06 17:23:27', null);
INSERT INTO `community_user_discuss` VALUES ('9aa86ec05328b0221d1e2c0cfd705d04', '2fb74f2f34d8971353cd60142ca533ca', '', '032602991210dd644be04f16f2aea95d', '', '在吗', '2021-09-29 22:16:33', '');
INSERT INTO `community_user_discuss` VALUES ('9cbc7c22081278b4a0a73dff66c5245f', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '3', '2021-09-13 01:24:46', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('9db92ec1ceb486a5818e860b28325247', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:25:26', null);
INSERT INTO `community_user_discuss` VALUES ('9f8b3623d55ad4bdb299a9bac9bbcaf5', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '411', '2021-09-13 02:15:02', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('a08c171cf4531e99362fbe4282a5ac29', '1b0906665a09a6dde23f4aa15be503cf', '', '44444444444444', '', 'hshh', '2021-08-27 13:11:58', '10ef27fe62bb5ea8e8f7aea074600f91');
INSERT INTO `community_user_discuss` VALUES ('a50f77689ff928f322be315a4a174e4f', '1b0906665a09a6dde23f4aa15be503cf', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '得得111', '2021-08-27 12:57:00', '10ef27fe62bb5ea8e8f7aea074600f91');
INSERT INTO `community_user_discuss` VALUES ('a5f98da0519a48bf5bf6903e05257b3d', '0e832ce0242fc8b8f45d1ee75cae3965', '', 'ab83682fb12e295829b1bffa236530ea', '', 'haob', '2021-09-29 17:05:33', '');
INSERT INTO `community_user_discuss` VALUES ('ac41ab08357ed7032ee18b620516cb09', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2019-12-06 17:42:12', null);
INSERT INTO `community_user_discuss` VALUES ('adf1744c31ecace08ea429c427d55738', 'ddba612f729bc9a3eb55e787b18cf77b', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '一个个', '2021-08-22 19:31:30', null);
INSERT INTO `community_user_discuss` VALUES ('b81b34ce5d8871219a74e93026059236', '38a326c690dbce695cb099c192b6c06f', '', '674b126fec114cdd6a30b1c20f78b337', '', 'dddd', '2021-09-12 20:16:26', '');
INSERT INTO `community_user_discuss` VALUES ('c687d5149ba701ad00aa09006c4e8c07', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-01 22:25:33', null);
INSERT INTO `community_user_discuss` VALUES ('ca0c552f4cc9a3eea4343589ceaff9bf', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '1111333333333333377888-----', '2020-01-08 03:54:13', null);
INSERT INTO `community_user_discuss` VALUES ('cce9b95d011e82f01bb0aa01e891ad77', '3054650f520fdb9d07084399812ec897', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '说说', '2021-09-12 23:39:10', '4aad446533819bc8cb58c39a221894d8');
INSERT INTO `community_user_discuss` VALUES ('cd41f33ff7d9b6bb1910cb776a0185b3', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', 'fa4029a1c16e5bb1715982e1bb095b14', '穿多大', '2021-09-13 01:38:56', '90bd8addb03a7fd6e46072618065f142');
INSERT INTO `community_user_discuss` VALUES ('ce3687e1076c1d3535a8433038e9102e', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', 'fa4029a1c16e5bb1715982e1bb095b14', '怎么了', '2021-09-13 01:33:40', '90bd8addb03a7fd6e46072618065f142');
INSERT INTO `community_user_discuss` VALUES ('ce8686803fc62352a86a1e11f4ec1523', '1b0906665a09a6dde23f4aa15be503cf', '', 'fa4029a1c16e5bb1715982e1bb095b14', '', '好啥好', '2021-08-27 12:22:14', null);
INSERT INTO `community_user_discuss` VALUES ('d331bc1dd41c37696298849840fc1782', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '1', '2021-09-12 23:39:48', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('d570b83fd667c9266935972eddd40d59', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '', '大萨达撒大所多', '2019-12-06 15:21:03', null);
INSERT INTO `community_user_discuss` VALUES ('d57aa5c8b1775b52b0c12f267b4b1060', '10b981f796c24c4fe27fb16d5a327348', '', '44444444444444', '5484f6fdc9b0faf1a32f31476d38c9d9', '大萨达撒大所多8888-----', '2019-12-06 17:15:30', null);
INSERT INTO `community_user_discuss` VALUES ('dcce7694ca11c2463533684628ff54bf', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '是的', '2021-09-13 01:39:24', '36860501ce661d800032ef3b16ec73b5');
INSERT INTO `community_user_discuss` VALUES ('e286407ca54a69b685184d7606923dca', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '喜欢喜欢小火花喜欢喜欢擦擦并不想八点半', '2021-09-13 01:32:14', '36860501ce661d800032ef3b16ec73b5');
INSERT INTO `community_user_discuss` VALUES ('e4efa2c4ccad45a15e835bec9c365427', '3054650f520fdb9d07084399812ec897', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '泽慢慢来', '2021-09-12 23:39:02', '4aad446533819bc8cb58c39a221894d8');
INSERT INTO `community_user_discuss` VALUES ('e80057d950f4dc0fe3342453837ddfee', '38a326c690dbce695cb099c192b6c06f', '', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '狗', '2021-09-12 23:38:25', '36860501ce661d800032ef3b16ec73b5');
INSERT INTO `community_user_discuss` VALUES ('e83bf6aa4c1b558965c304c7ba368f09', '1b0906665a09a6dde23f4aa15be503cf', '', 'fa4029a1c16e5bb1715982e1bb095b14', 'fa4029a1c16e5bb1715982e1bb095b14', '得得', '2021-08-27 12:56:13', null);
INSERT INTO `community_user_discuss` VALUES ('e8e9c45c48e5731c15af7c91afd1cecd', 'ddba612f729bc9a3eb55e787b18cf77b', '', '44444444444444', '', '1111333333333333377888-----', '2020-01-08 04:34:49', null);
INSERT INTO `community_user_discuss` VALUES ('fd8477d35a33539f56d0c6b9d66b4113', '38a326c690dbce695cb099c192b6c06f', '', '674b126fec114cdd6a30b1c20f78b337', '674b126fec114cdd6a30b1c20f78b337', '下次吃吃吃', '2021-09-12 23:26:19', 'b81b34ce5d8871219a74e93026059236');
INSERT INTO `community_user_discuss` VALUES ('fe0c235d9eb7a71ac0f19ff60027f950', '02edf2638aa19aa35fba25ae4e927928', '', '674b126fec114cdd6a30b1c20f78b337', '', '洗香香', '2021-09-24 18:57:38', '');
INSERT INTO `community_user_discuss` VALUES ('fea07d9fcac504ddbf6daefe6ee79b6b', '2fb74f2f34d8971353cd60142ca533ca', '', '674b126fec114cdd6a30b1c20f78b337', '', '大家支持一下', '2021-09-26 23:13:08', '');

-- ----------------------------
-- Table structure for `community_user_dynamics`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_dynamics`;
CREATE TABLE `community_user_dynamics` (
  `did` varchar(32) NOT NULL COMMENT '动态ID',
  `uid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `d_message` varchar(500) DEFAULT NULL COMMENT '动态标题',
  `d_posttime` datetime DEFAULT NULL COMMENT '发布时间',
  `d_title` varchar(100) DEFAULT NULL COMMENT '动态标题',
  `d_type` int(2) DEFAULT '0' COMMENT '动态类型:0文字1图片和文字2语音和文字3视频和文字',
  `d_isdel` int(2) DEFAULT '0' COMMENT '是否删除',
  `d_hotscore` int(10) DEFAULT '0' COMMENT '热度分数观看数',
  `d_zancount` int(10) DEFAULT '0' COMMENT '点赞数',
  `d_discusscount` int(10) DEFAULT '0' COMMENT '评论数',
  `location` varchar(100) DEFAULT NULL COMMENT '位置',
  `d_url` varchar(1000) DEFAULT NULL COMMENT '资源ur：9张图片，1视频，1语音',
  PRIMARY KEY (`did`),
  KEY `post_time_index` (`d_posttime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块动态表';

-- ----------------------------
-- Records of community_user_dynamics
-- ----------------------------
INSERT INTO `community_user_dynamics` VALUES ('0032291325ac105c69aa843d8e3c9966', 'e444b3c199036739d6118db8ab6ecad0', '嘻嘻嘻嘻这是事实', '2021-09-22 14:13:51', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('01d970cadb25d4211b902b13997a84df', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '888', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('0348621f1ab3d623858c0ef728e86e44', '7a5b1b93493830b297b569baf967b5dc', '单刀队', '2021-09-28 16:50:31', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('04424c3597db40099c9cb0de3bec71d1', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('054a602555157c9d4b440dca1b6e7736', '62fa7cae653e4051abe917d18cbb59d0', '还是啥的', '2019-12-27 19:06:51', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('0559c8a42cfd1769f4f00c9b1bd53481', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('05748d6dff6ed5a01e253a9522c402d2', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('08bd8511ba87073f5a15f3aa5cc3681e', '44444444444444', '这是一个信息1', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('09661ac8289502796ed81e14ae79e3a1', '44444444444444', '这是一个信息2', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('099a99dfcd299a1def3d5479f4c165b9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('0c335da2235eb8fc9879b999c056302c', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('0e832ce0242fc8b8f45d1ee75cae3965', '53651178f09d6a52fbac415ac8681d66', '单刀队v吃醋', '2021-09-28 17:18:42', '', '0', '0', '0', '0', '1', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('0ea5d26dec7f057ce32dc7810f3704d9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('10b981f796c24c4fe27fb16d5a327348', '44444444444444', '这是一个信息666', '2020-01-01 22:42:45', '这是一个标题', '0', '0', '0', '0', '21', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('131e13abe2cf65e4d0fbf8c9a5f7e637', '44444444444444', '这是一个信息3', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('183a1c909d72a852e9b224ad70f7aa91', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:02:38', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912527512_TleRDF.png|');
INSERT INTO `community_user_dynamics` VALUES ('1854a48517e416e8d5e77ac9b6f5f0af', '44444444444444', '这是一个信息4', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('18bf535e5c3728001d56d8c0ce81492a', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。', '2020-01-02 05:22:06', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577913725043_MEjUUC.gif|');
INSERT INTO `community_user_dynamics` VALUES ('18c771cdf53df7b9113564db85e1feae', 'fa4029a1c16e5bb1715982e1bb095b14', 'uuuu', '2021-08-22 23:36:34', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('1aeafc3d86963f8fcdd8d6cc5e776050', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('1b03f463cf30dd321e3ac0e8a11c5ef8', 'fa4029a1c16e5bb1715982e1bb095b14', '大兄弟', '2021-08-25 17:53:00', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('1b0906665a09a6dde23f4aa15be503cf', 'fa4029a1c16e5bb1715982e1bb095b14', '', '2021-08-27 12:21:44', '', '1', '0', '0', '1', '7', '北京市海淀区', 'photo/1630038283202_GlGMkh.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('1ebbbeeb68dde554ea7f8f6d7b834841', '44444444444444', '这是一个信息5', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('20b29bab70d17dac9e8bfab71a296fcf', '62fa7cae653e4051abe917d18cbb59d0', 'ggaga', '2021-08-27 19:28:11', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1630063869135_FiVOwC.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('2263ed45aa384d1b890fa2775a88896b', '62fa7cae653e4051abe917d18cbb59d0', '3', '2019-12-19 01:18:13', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1576689491508_GZNYeD.jpg|photo/1576689491122_PECcmU.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('241b69556be9e9fe4429687e326b7c1d', '44444444444444', '这是一个信息6', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('273292b720fc9cbea0f7160c0c9397d2', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('28680e8c17a84ad637b97be7ce6e0307', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('2a1fc2c7c4f95233f6e19c2e08255ab8', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('2abf9c92f759d6fd95bb3580c40b090e', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('2ac06a460dd9d6f9677bc9569e6a2e2d', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('2cbe46f5786e14b1d1acec74279b2b4b', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:33:50', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo//storage/emulated/0/tencent/MicroMsg/WeiXin/mmexport1577564654560.gif|');
INSERT INTO `community_user_dynamics` VALUES ('2d67a2cd6f0e20df811e0eea723fbb8a', '44444444444444', '这是一个信息7', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('2f07798cabfd0375830af82b0f0be495', '44444444444444', '这是一个信息8', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('2fb74f2f34d8971353cd60142ca533ca', '674b126fec114cdd6a30b1c20f78b337', '找对象', '2021-09-26 23:12:36', '', '1', '0', '0', '0', '4', '北京市海淀区', 'photo/1632669367334_pKNVBa.jpg|photo/1632669367946_eiDVLs.jpg|photo/1632669367632_PqSFFP.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('3054650f520fdb9d07084399812ec897', 'e444b3c199036739d6118db8ab6ecad0', '', '2021-09-08 04:23:00', '', '1', '0', '0', '1', '3', '北京市海淀区', 'photo/1631046374274_SnLvNq.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('3570c6a38edfe50bd5b7ae1f81c677c9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('3678d9f740c59f6929447ab8b78acb5f', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('37bac6c4a2fba9eabc8d6eb9a3aae565', 'fa4029a1c16e5bb1715982e1bb095b14', '得得', '2021-08-22 23:42:53', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1629647146197_OwIroF.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('39c24f7b4678e5aa992fed983b470a02', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:36', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('39d3c92359d67107b3351818a9446500', '44444444444444', '计算机三级', '2020-01-07 18:08:21', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('3a5799e62dc6ded096b3d34ad55bc3fb', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('3a9aee6cc9f24b80883e4f870fdd17e8', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('3b2644c28a527bd54c4fe13e8dbea4b8', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('40606aa1b9be11372f0583e023dfb34e', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:36:49', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577914607579_wURFsb.gif|');
INSERT INTO `community_user_dynamics` VALUES ('4095da18e797f969612f897a73a3033e', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('420b3ce5ab90b0316ff5e5af6336b1ba', '44444444444444', '这是一个信息9', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('469de6843c0083ae3dda75800635b36b', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('47dc1fd24073d8ee0f8e6d0e4bc15f98', '44444444444444', '这是一个信息10', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('493af65f72bb10b8e3fdb40a08c80ea8', '44444444444444', '这是一个信息11', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('4954343cba16724f17a9964c56f27e32', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:02:23', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('4cae346cc62fcb5cd017d4e8cd619300', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:00:40', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912438121_UdXCMb.png|');
INSERT INTO `community_user_dynamics` VALUES ('4d2560b1bbbdf6401fcde16be0b42ccf', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('4d7f6d84cba4723f6adef192e9e5ba19', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('4f3bcc9370daad71b1219c1dbf960843', '44444444444444', '这是一个信息12', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('543fd63a53479653c22f0b55764998e0', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('55e47f0c8694eb6059358d45d751fad9', '44444444444444', '江河湖海', '2020-01-07 19:38:26', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1578397245929_eEAvEx.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('56eccfea3f92ba851f39774a3c12f89c', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('57424bcb71ad245e5356399244d904f4', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:07:08', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912826604_UThKbH.png|');
INSERT INTO `community_user_dynamics` VALUES ('574455801b4a3d2eca17e70109b7c9e3', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '33', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('5d1a42a704749e5eec651a7517ff1709', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('5e689b618166ca8e2c9f4b1c4cf480a2', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('602fa5f37d585dfe67194499451eba1c', '44444444444444', '这是一个信息13', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('614e2e263917366bbd438fd0c4d9ff42', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:18:42', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577913520809_idheRe.gif|');
INSERT INTO `community_user_dynamics` VALUES ('63d55424515ea9cbb2ddf508b59d7f5c', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('6485ccce4d6a4f65ea06916403c85f7e', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('67efdef0f573732789fe6654be7cc484', '62fa7cae653e4051abe917d18cbb59d0', '我爱你', '2020-01-01 09:36:05', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('693b54594c070f62aae60613857765ab', '44444444444444', '这是一个信息14', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('69a89adfc988542f2f090a7d945765b5', '44444444444444', '这是一个信息15', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('6a12491c5c61a2e8a51dea016f01ae88', '44444444444444', '这是一个信息16', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('6d13fd01f7c9cdc619fd50e78b2406ec', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈112', '2020-01-01 08:02:36', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('6d612c36214588830b03ef17d5fe9f2b', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('6f4a5bbc745ec2d4cb36a9242d0b5ba2', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('6f4cc19661a4e6547191e9328291c154', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('70c1ee7cf19aecf12feda6ff5cfab4fe', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('710dc98f6321b245aade624e970c277d', 'dc6e0c1be91f392055388db72e8d1366', '语音', '2021-09-22 14:39:46', '', '0', '0', '0', '1', '1', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('725dce607e570a33b09309d6975a2b87', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('72c84fe5243e0c1bb71ba03c34fd2198', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。', '2020-01-02 05:33:17', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo//storage/emulated/0/tencent/MicroMsg/WeiXin/mmexport1577564654560.gif|');
INSERT INTO `community_user_dynamics` VALUES ('78763c1fee128e417d2d3cf406042b98', '44444444444444', '这是一个信息17', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('7c1ed9e360fea5fb3499681cd0ee52f3', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:38:25', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577914703920_CIkSUB.gif|');
INSERT INTO `community_user_dynamics` VALUES ('7e5afa5489b516abef0d09a914636c9c', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈112', '2020-01-01 08:02:56', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('80c44635e0a095f7ef402d72be1c5cc0', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈哈哈', '2019-12-20 14:38:26', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1576823903618_hxyhur.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('82e6222e2c8f7d97ca0866669da59ceb', 'e444b3c199036739d6118db8ab6ecad0', '嘻嘻嘻嘻', '2021-09-22 14:13:41', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('82ff6275c2d466169a3d2d652b17577b', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('83170974f636c2b8d2ec9ae70211ce2e', '44444444444444', '这是一个信息18', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('8389becdc3cbb335c4e1c09e25122cd9', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('845a1d478c13dae38628be6480477de8', 'fa4029a1c16e5bb1715982e1bb095b14', '找个对象', '2021-08-26 03:52:44', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1629921341322_feSvGJ.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('878aadbc1a12edfcae7a3cf6d33a0daf', '44444444444444', '这是一个信息19', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('88940627518f5e5b9d6e23f7e88aaad1', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:32', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('890d2bc1abb84abfdae81e3b728b1c76', '44444444444444', '这是一个信息20', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('8a2d4be7b2fa718c130167ee9dd4fc9e', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:07:52', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912870815_BKXbVh.gif|');
INSERT INTO `community_user_dynamics` VALUES ('8da11d7cb1fd0cbcb8a129b0e093df8f', '44444444444444', '这是一个信息21', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('90e99501a150be665393b9e7a6888f2d', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('92055b682d1807cca4c2250a21278a2c', '62fa7cae653e4051abe917d18cbb59d0', '我去', '2020-01-01 10:42:13', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577846530369_naJcJs.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('92ee90518cefd217d34fa5d551642bab', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('934c95bd69a4108fe938a1ec55769013', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:17:33', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577913451808_jGUNHL.gif|');
INSERT INTO `community_user_dynamics` VALUES ('95d13ad96f122a879a4982e150006959', '44444444444444', '这是一个信息22', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('995a175e5a5fbd12d124b589b04b9966', '62fa7cae653e4051abe917d18cbb59d0', '局域12', '2020-01-01 11:48:17', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850494542_wHyqJb.png|photo/1577850494338_oyrBDP.png|photo/1577850494177_NMNQhy.png|photo/1577850494758_FVNmnt.png|photo/1577850494956_yyEheB.png|photo/1577850494665_lcVLjF.png|');
INSERT INTO `community_user_dynamics` VALUES ('9aec5db3345cc7ea94b68c5c55b5fd81', '62fa7cae653e4051abe917d18cbb59d0', '我去', '2020-01-01 11:39:09', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577849947591_fvdAYa.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('9d395c10f69c0de2406ed1de638b97bf', '62fa7cae653e4051abe917d18cbb59d0', '还是啥的', '2019-12-27 19:07:17', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577444834649_BmcCmC.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('9d96d2b61b795e41f07404019d7a232b', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:38', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('9dc62776212bb16697e36c32f534f627', 'fa4029a1c16e5bb1715982e1bb095b14', '肉肉肉', '2021-08-22 19:29:08', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('9eaaba9833e92902a4afc02161d099c1', '555555', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a0cc90927e5c3a361a674025f85a1df5', '44444444444444', '这是一个信息23', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a164e859bbf356da785d7c2027ee06f7', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a1f49fd354be81f585e13d30e5169367', '44444444444444', '这是一个信息24', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a261b5d9cea215b7c359d6df5cd870f2', '44444444444444', '这是一个信息25', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a3219dc227b60646f9474c5f7bb08220', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a3fa9e6557b718428fe34c0b832bfabc', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a497192993684b6581321d896fd27534', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:02:26', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('a7f3abf41edd9223c532fcaef1209bf1', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('a8847bab20e050234dc56bcfbe47e64d', '44444444444444', '这是一个信息26', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('a9e69b08f68404431da8b1437a55b673', '62fa7cae653e4051abe917d18cbb59d0', '局域', '2020-01-01 11:45:54', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850352198_MuCehj.png|photo/1577850352522_aTuRXb.png|');
INSERT INTO `community_user_dynamics` VALUES ('aae43f2e8187c35884711044a8499ae2', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ab17cd55cfec6bd4af89812edea74c64', '62fa7cae653e4051abe917d18cbb59d0', '我们的', '2020-01-02 05:58:59', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('ad195212b5f002e22c652d8d19e4739a', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b2427ee05663d61b032506d3ed690ada', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b47f204b64c1160de0842fa0bbf85a2a', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b491fceef94385a9978e4fd35cca9b04', '62fa7cae653e4051abe917d18cbb59d0', '局域12', '2020-01-01 11:47:25', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850443667_mpOuyq.png|');
INSERT INTO `community_user_dynamics` VALUES ('b63486c12bba28efd3acf361fa933f06', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('b75bbf2f5150004d8fde94cea1425df8', '44444444444444', '这是一个信息27', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('bb28c123275846ca86fb156c2565bafd', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('bc0b6c73206f41b13480a6d04375300e', 'fa4029a1c16e5bb1715982e1bb095b14', '好吧', '2021-09-26 13:58:16', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1632636106927_wAlEZr.jpg|photo/1632636106221_MYWtsb.jpg|photo/1632636107129_cYamby.jpg|photo/1632636107602_oJmZON.jpg|photo/1632636107410_xkApbP.jpg|photo/1632636106658_myyMrh.jpg|photo/1632636107925_iezpkp.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('bcecfe248adc0d2fc2712740cb8ca7f7', '62fa7cae653e4051abe917d18cbb59d0', '局域1', '2020-01-01 11:46:21', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577850379073_RDTfVO.png|');
INSERT INTO `community_user_dynamics` VALUES ('bf9aa8ce5c7267252be5dbc63301c5ea', '62fa7cae653e4051abe917d18cbb59d0', '基督教诶嘿嘿122', '2019-12-19 02:17:02', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('bfa865791f3e4e91bc62df428232c92a', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c041fc743028542bb074f6bab493c099', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c134074f1fe14f9788ad5948bce5f3d6', 'fa4029a1c16e5bb1715982e1bb095b14', '单刀队', '2021-09-26 14:07:01', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1632636633080_qkFGIi.jpg|photo/1632636633172_ArpCKM.jpg|photo/1632636633344_qwogBC.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('c55337f71fc9ba666f33c301de8e0dfd', '44444444444444', '这是一个信息28', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('c6604f73548dc536b2e90bce2287f6b8', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c6a830861d993febe0d641768148f658', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('c75e4588239dab7cd5787a57641803d7', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('c8a36e511a37e27bb54cc976a1fdb51b', '44444444444444', '这是一个信息29', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '3', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('caca24b421b3eaaee0400611b162bb7b', '7777777', '这是一个信息999', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('cbb0a6e8d958adf2fb547ac62a1a6a7b', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('cde0d3f0b5442c23a1c323e85d3f61dc', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:03', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('ce6a2dffd556e0eb3bbb927ae49f1d91', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ce6df0ff95285fa11ebd986878a6145e', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。哦', '2020-01-02 05:35:11', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577914481444_wswBnR.gif|');
INSERT INTO `community_user_dynamics` VALUES ('ceb0b923d0958c18c3a5904ce30e5e2c', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 20:50:31', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577883030181_tNZAgb.png|');
INSERT INTO `community_user_dynamics` VALUES ('d10c0947c706a86d7b237bfc1444c6b6', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d2dc40d38e90033a3f835a84c9b74e9f', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d3945f0897932c02092d223b0f9986c1', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d513ac404eed33e73cbb7bd68d2b1d3c', '62fa7cae653e4051abe917d18cbb59d0', '我们', '2020-01-02 05:49:51', '', '1', '0', '0', '0', '4', '北京市海淀区', 'photo/1577915384044_uIeKRT.gif|photo/1577915383284_QkdZec.gif|photo/1577915384060_eQdngW.gif|');
INSERT INTO `community_user_dynamics` VALUES ('d528da6c853c6d952c1bd112995b4b9b', '44444444444444', '这是一个信息30', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('d5bfb491b43c61df0fdbed78ceaa6ada', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d7ed5f0474f209ce3f2cdf24f55a22bf', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('d983358b6ab76170b27344a5123bac3f', '44444444444444', '这是一个信息31', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('ddba612f729bc9a3eb55e787b18cf77b', '62fa7cae653e4051abe917d18cbb59d0', 'hahah', '2020-01-07 17:56:31', '', '0', '0', '0', '1', '3', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('de90943fbf369d8e1e6721e085742e6a', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:34', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('e08ff3705a5b38582fca527329e154ae', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('e4a7afa9644293573b428b8714759e2a', '44444444444444', 'jzj', '2020-01-08 03:26:59', '', '1', '0', '0', '1', '2', '北京市海淀区', 'photo/1578425359040_cqgJFP.jpg|');
INSERT INTO `community_user_dynamics` VALUES ('e4cc43cc34557607f4adf135101e856d', '44444444444444', '这是一个信息32', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e548bc0c56cd66d383a2c47fa42e0c6d', '44444444444444', '这是一个信息33', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e62a821bc66b35bd3ec4b728eb5977da', '44444444444444', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e6b4a552cba83c7daf8264fd375e5771', '62fa7cae653e4051abe917d18cbb59d0', '233334', '2020-01-02 05:07:17', '', '1', '0', '0', '0', '0', '北京市海淀区', 'photo/1577912835706_IwRiVd.gif|');
INSERT INTO `community_user_dynamics` VALUES ('e73bd86678ae03b602873d013706eebc', '44444444444444', '这是一个信息34', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('e85de1d4c39d3ab4277f117a2c71b8b2', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('e956769c2be1980beee4a7f5f8aaa6dd', '44444444444444', '这是一个信息0', '2019-12-14 22:42:45', '是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('ebb210fb7a1ec2e0022077f57496f48f', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ec7d6f7a253f3bc3df44b92bebf24064', '62fa7cae653e4051abe917d18cbb59d0', '233334。。。', '2020-01-02 05:27:12', '', '1', '0', '0', '1', '0', '北京市海淀区', 'photo/1577914030312_rONYIh.gif|');
INSERT INTO `community_user_dynamics` VALUES ('ec80f18c937da625563e29b351dbee0d', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-14 22:42:45', '这是一个标题999', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('ee422f12ab105ecf2eb0b3178987ad70', '555555', '这是一个信息', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '1', '0', '北京市海淀区', null);
INSERT INTO `community_user_dynamics` VALUES ('ef18262d3576d50f44eef30dab09288a', '44444444444444', '这是一个信息35', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('f534b6d87822175a85d6009081c2bc22', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈', '2020-01-01 08:01:41', '', '0', '0', '0', '0', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('f7bb7af53aa56b17d184e27de5aa82f6', '44444444444444', '这是一个信息36', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('f8122cc88a1f18ff2643a121ea136b0a', '44444444444444', '这是一个信息37', '2019-12-14 22:42:45', '这是一个标题', '0', '0', '0', '0', '0', null, null);
INSERT INTO `community_user_dynamics` VALUES ('f9a8ead94bf9127b4f116a6f0c8472cc', '62fa7cae653e4051abe917d18cbb59d0', '这是一个信息91111199', '2019-12-19 01:05:06', '这是一个标题999', '1', '0', '0', '1', '0', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');
INSERT INTO `community_user_dynamics` VALUES ('fb0b6857a9aed463f6858fa0c8874a50', '44444444444444', '作者是个傻逼', '2019-12-14 22:42:45', '这是一个标题', '1', '0', '0', '0', '0', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');
INSERT INTO `community_user_dynamics` VALUES ('fd8d12b01eb36ad7a35d4dbe85474c90', '62fa7cae653e4051abe917d18cbb59d0', '哈哈哈哈112', '2020-01-01 08:02:53', '', '0', '0', '0', '1', '0', '北京市海淀区', '');
INSERT INTO `community_user_dynamics` VALUES ('fe6d5de2e320fc95aa1ab83ae771e4e0', '62fa7cae653e4051abe917d18cbb59d0', '嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯嗯', '2019-12-15 22:42:45', '这是一个标题999', '1', '0', '0', '1', '0', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');
INSERT INTO `community_user_dynamics` VALUES ('ffcf73ebb228b97ccd7891cf52cb6e89', '44444444444444', '哈哈我是小姐姐', '2019-12-14 22:42:45', '巴巴巴不得尼玛', '1', '0', '0', '1', '0', '北京市海淀区', '/photo/1576402740103_cKWayP.jpg|/photo/1576402740103_cKWayP.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg|/photo/1576402154557_xynVSi.jpg');

-- ----------------------------
-- Table structure for `community_user_follows`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_follows`;
CREATE TABLE `community_user_follows` (
  `from_uid` varchar(32) DEFAULT NULL COMMENT '粉丝ID',
  `to_uid` varchar(32) DEFAULT NULL COMMENT '关注ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `rel_type` int(1) DEFAULT '0' COMMENT '状态0正常1拉黑',
  UNIQUE KEY `follow_ufollow_index` (`from_uid`,`to_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块关注粉丝表';

-- ----------------------------
-- Records of community_user_follows
-- ----------------------------
INSERT INTO `community_user_follows` VALUES ('62fa7cae653e4051abe917d18cbb59d0', '44444444444444', '2019-12-06 22:08:17', '0');

-- ----------------------------
-- Table structure for `community_user_zanlist`
-- ----------------------------
DROP TABLE IF EXISTS `community_user_zanlist`;
CREATE TABLE `community_user_zanlist` (
  `did` varchar(32) DEFAULT NULL COMMENT '动态id',
  `zan_uid` varchar(32) DEFAULT NULL COMMENT '点赞的用户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  UNIQUE KEY `follow_ufollow_index` (`did`,`zan_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='社交板块动态点赞列表';

-- ----------------------------
-- Records of community_user_zanlist
-- ----------------------------
INSERT INTO `community_user_zanlist` VALUES ('099a99dfcd299a1def3d5479f4c165b9', '41479828b219239398cc75363fc6fa9f', '2019-12-03 15:37:53');
INSERT INTO `community_user_zanlist` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '1', '2019-12-03 22:35:10');
INSERT INTO `community_user_zanlist` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '5484f6fdc9b0faf1a32f31476d38c9d9', '2019-12-06 21:58:57');
INSERT INTO `community_user_zanlist` VALUES ('c9903c7b65d03ece01e7d0ebbecb5f94', '111', '2019-12-10 12:43:58');
INSERT INTO `community_user_zanlist` VALUES ('b47f204b64c1160de0842fa0bbf85a2a', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:12:42');
INSERT INTO `community_user_zanlist` VALUES ('574455801b4a3d2eca17e70109b7c9e3', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:13:20');
INSERT INTO `community_user_zanlist` VALUES ('d7ed5f0474f209ce3f2cdf24f55a22bf', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:13:34');
INSERT INTO `community_user_zanlist` VALUES ('6d612c36214588830b03ef17d5fe9f2b', '7777777', '2019-12-10 13:14:05');
INSERT INTO `community_user_zanlist` VALUES ('2abf9c92f759d6fd95bb3580c40b090e', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:15:29');
INSERT INTO `community_user_zanlist` VALUES ('ebb210fb7a1ec2e0022077f57496f48f', '62fa7cae653e4051abe917d18cbb59d0', '2019-12-10 13:15:39');
INSERT INTO `community_user_zanlist` VALUES ('56eccfea3f92ba851f39774a3c12f89c', '7777777', '2019-12-10 13:15:43');
INSERT INTO `community_user_zanlist` VALUES ('caca24b421b3eaaee0400611b162bb7b', '7777777', '2019-12-10 15:43:23');
INSERT INTO `community_user_zanlist` VALUES ('aae43f2e8187c35884711044a8499ae2', '7777777', '2019-12-10 15:43:24');
INSERT INTO `community_user_zanlist` VALUES ('92ee90518cefd217d34fa5d551642bab', '7777777', '2019-12-10 15:43:35');
INSERT INTO `community_user_zanlist` VALUES ('39d3c92359d67107b3351818a9446500', '44444444444444', '2020-01-07 18:08:36');
INSERT INTO `community_user_zanlist` VALUES ('e4a7afa9644293573b428b8714759e2a', '44444444444444', '2020-01-08 03:27:13');
INSERT INTO `community_user_zanlist` VALUES ('ddba612f729bc9a3eb55e787b18cf77b', '62fa7cae653e4051abe917d18cbb59d0', '2021-08-22 19:31:18');
INSERT INTO `community_user_zanlist` VALUES ('37bac6c4a2fba9eabc8d6eb9a3aae565', 'fa4029a1c16e5bb1715982e1bb095b14', '2021-08-23 00:24:36');
INSERT INTO `community_user_zanlist` VALUES ('18c771cdf53df7b9113564db85e1feae', 'fa4029a1c16e5bb1715982e1bb095b14', '2021-08-23 00:24:37');
INSERT INTO `community_user_zanlist` VALUES ('1b0906665a09a6dde23f4aa15be503cf', 'fa4029a1c16e5bb1715982e1bb095b14', '2021-08-27 15:21:19');
INSERT INTO `community_user_zanlist` VALUES ('845a1d478c13dae38628be6480477de8', 'fa4029a1c16e5bb1715982e1bb095b14', '2021-09-05 09:16:47');
INSERT INTO `community_user_zanlist` VALUES ('9dc62776212bb16697e36c32f534f627', 'fa4029a1c16e5bb1715982e1bb095b14', '2021-09-05 09:17:01');
INSERT INTO `community_user_zanlist` VALUES ('ec7d6f7a253f3bc3df44b92bebf24064', '62fa7cae653e4051abe917d18cbb59d0', '2021-09-08 16:19:11');
INSERT INTO `community_user_zanlist` VALUES ('18bf535e5c3728001d56d8c0ce81492a', '62fa7cae653e4051abe917d18cbb59d0', '2021-09-08 16:19:15');
INSERT INTO `community_user_zanlist` VALUES ('3054650f520fdb9d07084399812ec897', 'e444b3c199036739d6118db8ab6ecad0', '2021-09-08 21:24:09');
INSERT INTO `community_user_zanlist` VALUES ('38a326c690dbce695cb099c192b6c06f', '674b126fec114cdd6a30b1c20f78b337', '2021-09-13 02:19:49');
INSERT INTO `community_user_zanlist` VALUES ('20b29bab70d17dac9e8bfab71a296fcf', '62fa7cae653e4051abe917d18cbb59d0', '2021-09-13 02:20:16');
INSERT INTO `community_user_zanlist` VALUES ('7dddc8bab34f9a3217cf24ad5a1154e1', '674b126fec114cdd6a30b1c20f78b337', '2021-09-24 18:24:58');
INSERT INTO `community_user_zanlist` VALUES ('710dc98f6321b245aade624e970c277d', 'dc6e0c1be91f392055388db72e8d1366', '2021-09-26 04:21:30');
INSERT INTO `community_user_zanlist` VALUES ('c134074f1fe14f9788ad5948bce5f3d6', 'fa4029a1c16e5bb1715982e1bb095b14', '2021-09-26 14:08:14');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(32) NOT NULL,
  `log_type` int(2) DEFAULT NULL COMMENT '日志类型（1登录日志，2操作日志）',
  `log_content` varchar(1000) DEFAULT NULL COMMENT '日志内容',
  `operate_type` int(2) DEFAULT NULL COMMENT '操作类型',
  `userid` varchar(32) DEFAULT NULL COMMENT '操作用户账号',
  `username` varchar(100) DEFAULT NULL COMMENT '操作用户名称',
  `ip` varchar(100) DEFAULT NULL COMMENT 'IP',
  `method` varchar(500) DEFAULT NULL COMMENT '请求java方法',
  `request_url` varchar(255) DEFAULT NULL COMMENT '请求路径',
  `request_param` longtext COMMENT '请求参数',
  `request_type` varchar(10) DEFAULT NULL COMMENT '请求类型',
  `cost_time` bigint(20) DEFAULT NULL COMMENT '耗时',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_table_userid` (`userid`) USING BTREE,
  KEY `index_logt_ype` (`log_type`) USING BTREE,
  KEY `index_operate_type` (`operate_type`) USING BTREE,
  KEY `index_log_type` (`log_type`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('a0985235b5b8b3ae8e525dfab65fc879', '1', '用户名1: oKBv6w7sBYovMieU7siO0q3EUDfU,登录成功！', null, null, null, '192.168.10.98', null, null, null, null, null, null, '2021-09-29 21:20:48', null, null);
INSERT INTO `sys_log` VALUES ('1002e31851d76ee44dd94a6edf11eae7', '1', '用户名2: oKBv6w7sBYovMieU7siO0q3EUDfU,登录成功！', null, null, null, '192.168.10.98', null, null, null, null, null, null, '2021-09-29 21:20:48', null, null);
INSERT INTO `sys_log` VALUES ('4316a757cb31c7208abe88ba8fdcd58f', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:38:03', null, null);
INSERT INTO `sys_log` VALUES ('9694114600298f414768946d5cddc659', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:38:45', null, null);
INSERT INTO `sys_log` VALUES ('b5470611eb1c246c2409422ec3221391', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:38:45', null, null);
INSERT INTO `sys_log` VALUES ('eadd4d888cd65c126bd71cff72d36532', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:38:45', null, null);
INSERT INTO `sys_log` VALUES ('47ade5cc5d9090fdbaa089a592d57a18', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:38:45', null, null);
INSERT INTO `sys_log` VALUES ('b9fc9b851d05206079d16abb0a2ae6d3', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:47:57', null, null);
INSERT INTO `sys_log` VALUES ('662e9ccf07f64a6664af0c06f1a181f9', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:48:06', null, null);
INSERT INTO `sys_log` VALUES ('a8b3e6a98b979aa687a7ec947c4f2c37', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:48:06', null, null);
INSERT INTO `sys_log` VALUES ('f63f8a2c747a0018d790213b6245f9aa', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:49:53', null, null);
INSERT INTO `sys_log` VALUES ('6c82c9bea6306855169dade775d697e7', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:50:08', null, null);
INSERT INTO `sys_log` VALUES ('9592c401c7296e2aaacf596fbf64ea1c', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 21:50:08', null, null);
INSERT INTO `sys_log` VALUES ('c2eb4b3a96515bc8a9c3bef2b99a2698', '1', '用户名1: oKBv6w7sBYovMieU7siO0q3EUDfU,登录成功！', null, null, null, '192.168.10.98', null, null, null, null, null, null, '2021-09-30 01:33:23', null, null);
INSERT INTO `sys_log` VALUES ('d6f54f59f2a5ab77568fedf491adf9d5', '1', '用户名2: oKBv6w7sBYovMieU7siO0q3EUDfU,登录成功！', null, null, null, '192.168.10.98', null, null, null, null, null, null, '2021-09-30 01:33:23', null, null);
INSERT INTO `sys_log` VALUES ('3fb663a5b1a6aabf2f5b54e4ae674970', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-30 02:08:02', null, null);
INSERT INTO `sys_log` VALUES ('c7ba7b091a8b57f2751183018b583232', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-30 02:08:53', null, null);
INSERT INTO `sys_log` VALUES ('c34237c89f9bb5be7e68b0043b1a69bd', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-30 02:08:53', null, null);
INSERT INTO `sys_log` VALUES ('6e8a6635d2808aa159c761c5c72d97b3', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-30 02:09:02', null, null);
INSERT INTO `sys_log` VALUES ('429de537f37ca857875576cf19b54ac6', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-30 02:24:49', null, null);
INSERT INTO `sys_log` VALUES ('699920d64fb5b6d0c123e202764b2994', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-30 02:24:49', null, null);
INSERT INTO `sys_log` VALUES ('5dfe6d46dc0a239e26ab914d6f7a978f', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-11-30 17:36:33', null, null);
INSERT INTO `sys_log` VALUES ('f3e2f5507800fde4916ac3ba72357aa5', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-01 22:30:55', null, null);
INSERT INTO `sys_log` VALUES ('a9984c7aecce5092b4b4421780037234', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 16:47:31', null, null);
INSERT INTO `sys_log` VALUES ('b8149b62ea2f7aad07dc03d75a541111', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 19:42:55', null, null);
INSERT INTO `sys_log` VALUES ('de4431c704441090b18e7b0f3adc0ba4', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 21:16:03', null, null);
INSERT INTO `sys_log` VALUES ('17d7cbe0cb36672655e1ee7ab0b05f91', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 22:14:13', null, null);
INSERT INTO `sys_log` VALUES ('1054293d9ca2d6b14706918643280c6b', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-02 23:36:42', null, null);
INSERT INTO `sys_log` VALUES ('e6c3903ac5fabbe45af3e861f9cf1b2d', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 00:37:11', null, null);
INSERT INTO `sys_log` VALUES ('1a6ac59124a3b7cf4c883d707613b23b', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 00:56:04', null, null);
INSERT INTO `sys_log` VALUES ('edb17c76423a539108aef05e4d9bc2b2', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 00:56:51', null, null);
INSERT INTO `sys_log` VALUES ('6a25393dcca14d056fad1ae16ad59867', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:18:41', null, null);
INSERT INTO `sys_log` VALUES ('31b82309ec9c376df21a4b76d7124c47', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:23:03', null, null);
INSERT INTO `sys_log` VALUES ('d71406f980df21d137f1e13293601fe7', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:34:31', null, null);
INSERT INTO `sys_log` VALUES ('b53dc43f17998829c22e7e07a85b8f9a', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:38:55', null, null);
INSERT INTO `sys_log` VALUES ('8280a588764f106e64a09bec385e6f60', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-03 01:40:14', null, null);
INSERT INTO `sys_log` VALUES ('48720529859a040515495789759e1cfb', '1', '用户名: test,退出成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-15 01:58:16', null, null);
INSERT INTO `sys_log` VALUES ('fd94148393c40c2f23e7691faeb1dbeb', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2019-12-22 16:12:31', null, null);
INSERT INTO `sys_log` VALUES ('1dfb8a45b7812ac54f1659a4f7397ba9', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-06 10:41:46', null, null);
INSERT INTO `sys_log` VALUES ('e35f3995daf1708ff5c4fbf73259b07b', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-06 10:41:52', null, null);
INSERT INTO `sys_log` VALUES ('c4f28eba5dd1120d4a5425e383c39f8f', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-06 11:13:15', null, null);
INSERT INTO `sys_log` VALUES ('b2e9b58bd3a4a85cfd30c0ed97a61c38', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-06 12:07:01', null, null);
INSERT INTO `sys_log` VALUES ('5ceb840fbb33a062199c79498b7938d8', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-06 12:23:56', null, null);
INSERT INTO `sys_log` VALUES ('6e0fc79281d3c5d1ea24559bf3defdc4', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-06 12:26:08', null, null);
INSERT INTO `sys_log` VALUES ('981c61387878bdf34a932bf671f18343', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-07 14:46:58', null, null);
INSERT INTO `sys_log` VALUES ('360e6a308d659a87c4a67f78d2b926a3', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-07 17:54:39', null, null);
INSERT INTO `sys_log` VALUES ('82572ded0d2b67ee1b7f8b01c189b199', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-07 17:54:41', null, null);
INSERT INTO `sys_log` VALUES ('7f3ce01e2fd6d6574cf44a12fcd81cee', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-08 02:41:24', null, null);
INSERT INTO `sys_log` VALUES ('63fd156baa1772c2a2380814d56912c8', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-08 02:57:06', null, null);
INSERT INTO `sys_log` VALUES ('679af464e8333effbc189bd4bcd7436f', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-08 03:03:14', null, null);
INSERT INTO `sys_log` VALUES ('61cef16265109bcce4172d741ff4831d', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-08 03:06:24', null, null);
INSERT INTO `sys_log` VALUES ('f6741c085ad8cde39d8df60b2f523fe9', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-08 03:09:27', null, null);
INSERT INTO `sys_log` VALUES ('253d0a339a996fc290f3f6b73f86596b', '1', '用户名: test,登录成功！', null, null, null, '192.168.1.100', null, null, null, null, null, null, '2020-01-08 03:18:25', null, null);
INSERT INTO `sys_log` VALUES ('6552628c7626d1888b147ba771b2def7', '1', '用户名: test,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-11 06:30:11', null, null);
INSERT INTO `sys_log` VALUES ('a8619fd48c45206c34b017f471b134d6', '1', '用户名: test2,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-11 06:30:20', null, null);
INSERT INTO `sys_log` VALUES ('933fa3b7693084fc43a9853f0327cc36', '1', '用户名: test2,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-01-24 01:43:36', null, null);
INSERT INTO `sys_log` VALUES ('1fd570447faff15d18306c42c86ced4c', '1', '用户名: test,登录成功！', null, null, null, '192.168.0.101', null, null, null, null, null, null, '2020-01-24 01:52:47', null, null);
INSERT INTO `sys_log` VALUES ('a2eeb7f1b309034db17730efdaf5baeb', '1', '用户名: test,登录成功！', null, null, null, '192.168.0.103', null, null, null, null, null, null, '2020-01-24 02:26:53', null, null);
INSERT INTO `sys_log` VALUES ('a6d7714ff153049cb1e2de4f422efe63', '1', '用户名: test,登录成功！', null, null, null, '192.168.0.101', null, null, null, null, null, null, '2020-01-24 02:44:51', null, null);
INSERT INTO `sys_log` VALUES ('5af1a9588ff10a163739cb56bb796ddc', '1', '用户名: test2,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-03-20 10:09:09', null, null);
INSERT INTO `sys_log` VALUES ('3278f86cf2f1c6afc3c8d81f4b785bba', '1', '用户名: test2,登录成功！', null, null, null, '192.168.59.3', null, null, null, null, null, null, '2020-03-20 10:16:04', null, null);
INSERT INTO `sys_log` VALUES ('bb9218163886efc6551a2327f15eccfb', '1', '用户名1: test,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 06:30:15', null, null);
INSERT INTO `sys_log` VALUES ('758c4fb59048251b2f8b5c5dce9c144a', '1', '用户名2: test,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 06:30:15', null, null);
INSERT INTO `sys_log` VALUES ('d4447833753bb4c020e15936973b43fd', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 07:32:45', null, null);
INSERT INTO `sys_log` VALUES ('76555267feb87d3a2c45e6957f0b1685', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 07:32:46', null, null);
INSERT INTO `sys_log` VALUES ('cbc0c7a632ea6504b7fe453197cd3c40', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 07:33:13', null, null);
INSERT INTO `sys_log` VALUES ('62338bf56b82922156f05ae445e03abd', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 07:33:13', null, null);
INSERT INTO `sys_log` VALUES ('b9d34b611505d2a2213ff641e5cfbdf5', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 07:33:34', null, null);
INSERT INTO `sys_log` VALUES ('1d5c44ef8dc8b3ac84d24267c0b468b8', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 07:33:34', null, null);
INSERT INTO `sys_log` VALUES ('a2e3b9279a037c333968d2937771bf35', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 08:32:34', null, null);
INSERT INTO `sys_log` VALUES ('aaacec9f6e7d67ff83b6620a3359006c', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 08:32:34', null, null);
INSERT INTO `sys_log` VALUES ('719029ce3a4aaac060fcc8f814682b56', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 09:05:45', null, null);
INSERT INTO `sys_log` VALUES ('8fbe7a5c8ae3d9a6dcee9792ad9dd035', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 09:05:45', null, null);
INSERT INTO `sys_log` VALUES ('ec63bb2fc0d4fbf7ac7185d3a85b333f', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 09:16:22', null, null);
INSERT INTO `sys_log` VALUES ('c0be3eee9859b9046a926766fbf6570f', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 09:16:22', null, null);
INSERT INTO `sys_log` VALUES ('d4ffd47b469935cbac951880eda1f146', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 09:38:03', null, null);
INSERT INTO `sys_log` VALUES ('2a90bc61ec4e71a693828c24dc1388c0', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 09:38:03', null, null);
INSERT INTO `sys_log` VALUES ('8f821c066a1af5854eedf8a9d4238312', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 17:03:57', null, null);
INSERT INTO `sys_log` VALUES ('5fa83f62d04ec246a4bc96ea43d73483', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-22 17:03:57', null, null);
INSERT INTO `sys_log` VALUES ('d50678adb7b48ad409c7aa38e6cdca72', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-22 17:39:43', null, null);
INSERT INTO `sys_log` VALUES ('eb9e2bd08cdbace8988635bf636ec54e', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-22 17:39:43', null, null);
INSERT INTO `sys_log` VALUES ('f55620a1ca3b6c67b93f1b57adf5a95e', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-22 18:43:55', null, null);
INSERT INTO `sys_log` VALUES ('a611d104d8433b2d9b1dcc352260f061', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-22 18:43:55', null, null);
INSERT INTO `sys_log` VALUES ('aff47de6ace03729e984c3d677f6bdaf', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-22 19:09:22', null, null);
INSERT INTO `sys_log` VALUES ('040f8785ad9bbc46e1f3d8ee992c9444', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-22 19:09:22', null, null);
INSERT INTO `sys_log` VALUES ('7570cbe4200a00a66dfe0a3e15bb26a1', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-23 20:19:27', null, null);
INSERT INTO `sys_log` VALUES ('0a5298cd87397146044921860e455338', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-23 20:19:27', null, null);
INSERT INTO `sys_log` VALUES ('280774124b65e11221b8b74e156568d0', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 13:00:57', null, null);
INSERT INTO `sys_log` VALUES ('f59f14669fe46a3a7b0b4aa84ed5df82', '1', '用户名1: test,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 13:11:10', null, null);
INSERT INTO `sys_log` VALUES ('7b21d736a4ec376b048cca8150539564', '1', '用户名2: test,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 13:11:10', null, null);
INSERT INTO `sys_log` VALUES ('80354e9c6be46ae0159b81cb3916cfea', '1', '用户名: test,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 15:08:50', null, null);
INSERT INTO `sys_log` VALUES ('2297b80668690bfa66142820afcb3314', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 15:09:06', null, null);
INSERT INTO `sys_log` VALUES ('ac9a3b33cee4ca0ef56a0dad6fd9e345', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 15:09:06', null, null);
INSERT INTO `sys_log` VALUES ('e37123bac66c86e40e620c11f9de4e5c', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 18:01:20', null, null);
INSERT INTO `sys_log` VALUES ('ac0557ad06d2de0eaeb6d64c20e0cfc7', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 18:01:50', null, null);
INSERT INTO `sys_log` VALUES ('d9a1a4dca7dd0892683c8accce6151d8', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 18:01:50', null, null);
INSERT INTO `sys_log` VALUES ('c5c520643ec8e88ad0a2c7eacef3b1ae', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:16:22', null, null);
INSERT INTO `sys_log` VALUES ('55d021a7e320135ac6394aa0043de780', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:17:32', null, null);
INSERT INTO `sys_log` VALUES ('81c715ea045de41cfff9093de2ff268a', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:17:32', null, null);
INSERT INTO `sys_log` VALUES ('deb66c0d4c516a753cbbe28bdfad8bbc', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:18:51', null, null);
INSERT INTO `sys_log` VALUES ('bafed634e7cdb05e206d68cbd1e6ddb3', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:19:25', null, null);
INSERT INTO `sys_log` VALUES ('eaf88e368cc99af089b22dc3ab6c8df1', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:19:25', null, null);
INSERT INTO `sys_log` VALUES ('86e95191571225663c2357833d61191e', '1', '用户名: test2,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:26:49', null, null);
INSERT INTO `sys_log` VALUES ('235d9359110a3307cf4964ade76f5e2d', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:27:26', null, null);
INSERT INTO `sys_log` VALUES ('651fd94e8796b2f3587b859db251e327', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:27:26', null, null);
INSERT INTO `sys_log` VALUES ('ff4c8049fa2aed5fab450246ad3dc427', '1', '用户名: test2,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:29:12', null, null);
INSERT INTO `sys_log` VALUES ('e3ccdcc77d5d3d7b552975c9fe622551', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:29:47', null, null);
INSERT INTO `sys_log` VALUES ('d81549e00c1e19fa3b8b9ff100f81652', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:29:47', null, null);
INSERT INTO `sys_log` VALUES ('cc10976bc7770da3b36f1773811a3b03', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:30:52', null, null);
INSERT INTO `sys_log` VALUES ('e0fc2bb5ad5566ebd60b451d0c53b6ac', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:31:18', null, null);
INSERT INTO `sys_log` VALUES ('1b200d2d391059f547e34d24852cbe74', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-27 19:31:18', null, null);
INSERT INTO `sys_log` VALUES ('962ffe911036d9bc98a7747ff6e9f992', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-30 18:10:18', null, null);
INSERT INTO `sys_log` VALUES ('348fcc97203e976dcb583a60bc53b42f', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-30 18:10:18', null, null);
INSERT INTO `sys_log` VALUES ('78f082756aabeaba0cc21577849698ff', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-30 18:14:44', null, null);
INSERT INTO `sys_log` VALUES ('4f2768fd945a33b360e17af2b07b350f', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-08-30 18:14:44', null, null);
INSERT INTO `sys_log` VALUES ('6e168e05fa50ee718ee118e1bec0a71a', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-30 19:26:14', null, null);
INSERT INTO `sys_log` VALUES ('3f060ae24cd848a136b858cc7c6409c2', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-30 19:26:14', null, null);
INSERT INTO `sys_log` VALUES ('998015ee432173ba2481e0dbb12bc42c', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 15:42:11', null, null);
INSERT INTO `sys_log` VALUES ('86485d113b1d91e8f917a57c295ad3eb', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('a4392d7100bbda08dba0f3f211d96e27', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('20f21e1a44c4bb21fe71e237cc385dcb', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('1487a45b0fb9c5a6d4621f8177e7798e', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('9e0f82f1f52bd2ad2450be097c82ce21', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('bfeb262ca8d8749877d5ee6342d2c841', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('32365be37ea1040bf422900bf6c3081c', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('772d38d7261f8c668eb7e7b1ccb7fb55', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('d1b3a21ff1166faa82350108ca6132f7', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('03086b2e3f11e6250786307ad675b219', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-08-31 16:15:06', null, null);
INSERT INTO `sys_log` VALUES ('5326d53958534a68e9c59531425711e5', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-01 13:09:41', null, null);
INSERT INTO `sys_log` VALUES ('43732ff29d53594a1f18aa41cd342f48', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-01 13:09:41', null, null);
INSERT INTO `sys_log` VALUES ('7f73a0491bb2da25df53f54baa07dd2c', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-02 02:11:40', null, null);
INSERT INTO `sys_log` VALUES ('25f2b49fb42bdd97450bcb20da3b1832', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-02 02:11:40', null, null);
INSERT INTO `sys_log` VALUES ('d8eea2471034f10caf349f5242dc42bd', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:30:06', null, null);
INSERT INTO `sys_log` VALUES ('cbb7ab3841e674149672dd2989af9441', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:30:31', null, null);
INSERT INTO `sys_log` VALUES ('1918b132229d3c8fb49cb156405ad470', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:30:31', null, null);
INSERT INTO `sys_log` VALUES ('80947ce86a02064eab26f6e36f51e515', '1', '用户名: test2,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:53:43', null, null);
INSERT INTO `sys_log` VALUES ('bc01a84821cbd6e5b7de10823d167ce8', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:53:59', null, null);
INSERT INTO `sys_log` VALUES ('61903d46b8301a28e5b972b193fc3213', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:53:59', null, null);
INSERT INTO `sys_log` VALUES ('9384b2b48e54e99ec3a4e31b65a894cc', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:54:25', null, null);
INSERT INTO `sys_log` VALUES ('ec2f75346e18e792cd6aa987a2420994', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:54:47', null, null);
INSERT INTO `sys_log` VALUES ('de29bc418131c7fdaba7b960f52f13df', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:54:47', null, null);
INSERT INTO `sys_log` VALUES ('ebaca1229f2000211ca5d641b64279a6', '1', '用户名: test2,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:55:22', null, null);
INSERT INTO `sys_log` VALUES ('d18b8500276e2f32bedd764012d5bc1d', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:55:49', null, null);
INSERT INTO `sys_log` VALUES ('b050d2581ed71e9252fbdcbf6443e102', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-04 03:55:49', null, null);
INSERT INTO `sys_log` VALUES ('9513b5fd2ffe859544443f9c1c0ecb38', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-05 11:31:27', null, null);
INSERT INTO `sys_log` VALUES ('798f337ea150abde6da793ba6833c4f3', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-05 11:33:20', null, null);
INSERT INTO `sys_log` VALUES ('fc505d0214b7377410ee970648810bf9', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-05 11:33:20', null, null);
INSERT INTO `sys_log` VALUES ('cea37a3ffe6f38c8e50e067a09f3e05b', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-06 02:49:14', null, null);
INSERT INTO `sys_log` VALUES ('a74d7f30c902e5e12bc08d948b3bcf02', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-06 02:49:14', null, null);
INSERT INTO `sys_log` VALUES ('f82f59c3aa6868e30cb4ab0019ae4d60', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-06 12:40:44', null, null);
INSERT INTO `sys_log` VALUES ('734c1381deb20ab79d3f91f5bcddf437', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-06 12:41:15', null, null);
INSERT INTO `sys_log` VALUES ('19c6c638f4ef2f0769ca9616e9549edf', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-06 12:41:15', null, null);
INSERT INTO `sys_log` VALUES ('6e0656226a215b50a7f662d939f663a7', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-06 23:57:13', null, null);
INSERT INTO `sys_log` VALUES ('abdecfd87dcdf0f53a085732f91f15dd', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-06 23:57:36', null, null);
INSERT INTO `sys_log` VALUES ('c2af584f29afac37558924a834443d0c', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-06 23:57:36', null, null);
INSERT INTO `sys_log` VALUES ('034b4a8173bce6bb20713f9fda9ad2c6', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 00:39:43', null, null);
INSERT INTO `sys_log` VALUES ('ac15ca0c97366eabee68e90453c44cc4', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 00:39:56', null, null);
INSERT INTO `sys_log` VALUES ('eb66921b22fd0064fbc1ff26f570e9d9', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 00:39:56', null, null);
INSERT INTO `sys_log` VALUES ('4ebe0dc7edf3e251cbd182458499d223', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 00:47:16', null, null);
INSERT INTO `sys_log` VALUES ('326e2e1866bfc5faec589297b8e8905d', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 00:47:16', null, null);
INSERT INTO `sys_log` VALUES ('e88061a1fd1fd2845f043a64aa5c236c', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 01:03:14', null, null);
INSERT INTO `sys_log` VALUES ('10a5b6acc038b7a7bc467bd9d528274d', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 01:03:14', null, null);
INSERT INTO `sys_log` VALUES ('231ca3517c99790489ebaf9358a57e45', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:12:05', null, null);
INSERT INTO `sys_log` VALUES ('0406a44100bbb2ea11e35676c09ae9cc', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:12:21', null, null);
INSERT INTO `sys_log` VALUES ('08e8483a6594977ab58a6a5bbca6ce5c', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:12:21', null, null);
INSERT INTO `sys_log` VALUES ('1d4dca67b0904ff196e979d8b4dcd3a8', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:27:48', null, null);
INSERT INTO `sys_log` VALUES ('056da55fc7ee0ecce8dec4e4a86aa5c2', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:28:03', null, null);
INSERT INTO `sys_log` VALUES ('7fa79a38e44ed3870af380a58240443f', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:28:03', null, null);
INSERT INTO `sys_log` VALUES ('552bb72402928ba1c68123254247bbb5', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:41:26', null, null);
INSERT INTO `sys_log` VALUES ('386330de7520fda623384002bd46af12', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:41:40', null, null);
INSERT INTO `sys_log` VALUES ('d6c6b44e7d5b883cef8cb73ea04d79a8', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:41:40', null, null);
INSERT INTO `sys_log` VALUES ('f78c092f2fbc55ddcb7d5744c4e5a11f', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:51:26', null, null);
INSERT INTO `sys_log` VALUES ('ffc05afd5714bbb541e870ea97e55d69', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:51:55', null, null);
INSERT INTO `sys_log` VALUES ('712f25051ff80c22189f43d56d16555a', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:51:55', null, null);
INSERT INTO `sys_log` VALUES ('62d123780f5a17caf0a0d0011df9b8ae', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:52:12', null, null);
INSERT INTO `sys_log` VALUES ('d2d817565e5468b667d1e6398d1d70c7', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:52:28', null, null);
INSERT INTO `sys_log` VALUES ('0efe97d44238977c8aa460aa5338a57f', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 04:52:28', null, null);
INSERT INTO `sys_log` VALUES ('e593abc393e1e6d87deab53604589454', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 21:56:32', null, null);
INSERT INTO `sys_log` VALUES ('36413c7f63cc3a2eeef98cbcd660feb4', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-07 21:56:32', null, null);
INSERT INTO `sys_log` VALUES ('3578763b5856f89c72b8de3ca23e51cc', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 04:11:47', null, null);
INSERT INTO `sys_log` VALUES ('0f20b1b572392442cfb3eb83d42c5cd9', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 04:12:05', null, null);
INSERT INTO `sys_log` VALUES ('010f67e655fd01e86dfc9b8922bb0610', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 04:12:05', null, null);
INSERT INTO `sys_log` VALUES ('b11ea74a36a5dd67d12e04d9656f8560', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 18:21:02', null, null);
INSERT INTO `sys_log` VALUES ('bce75fb48263ed67a00cbb41b4a6b899', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 18:21:16', null, null);
INSERT INTO `sys_log` VALUES ('7ea536b6e4691cf0dadcaccfb0c65666', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 18:21:16', null, null);
INSERT INTO `sys_log` VALUES ('9e1c286c284d0079be383ef29ff22130', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:37:59', null, null);
INSERT INTO `sys_log` VALUES ('26e808a25c9ce9053faddbcbcf062fdf', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:38:14', null, null);
INSERT INTO `sys_log` VALUES ('e5b7afb5d88db4a6cba55f4200c56e86', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:38:14', null, null);
INSERT INTO `sys_log` VALUES ('d149aa70018ab77eb52e47c11ad71ba3', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:47:17', null, null);
INSERT INTO `sys_log` VALUES ('18f4b9ff5fb1f91b603dccde45fa6ee0', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:47:30', null, null);
INSERT INTO `sys_log` VALUES ('67d7e8c6ade51ae818ab2d42c783c878', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:47:30', null, null);
INSERT INTO `sys_log` VALUES ('17b955eb7206eae4635fdce0909a4b25', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:48:02', null, null);
INSERT INTO `sys_log` VALUES ('f5df9bab6aa51300abaf3d12655edbe5', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:48:14', null, null);
INSERT INTO `sys_log` VALUES ('438cd3c4c85fbb2cc234cd000025924e', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:48:14', null, null);
INSERT INTO `sys_log` VALUES ('6d65479a10b51f927c425f3aa1095569', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:51:29', null, null);
INSERT INTO `sys_log` VALUES ('36050c594979cdb0fef57640fa6d99b3', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:51:42', null, null);
INSERT INTO `sys_log` VALUES ('b169c5407e51a8a3a663f9355038d5c9', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 21:51:42', null, null);
INSERT INTO `sys_log` VALUES ('06d6c63501a175fc0a011a6e394c5389', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:39:50', null, null);
INSERT INTO `sys_log` VALUES ('ba44176a2eaef15912c18398058ac9a9', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:40:16', null, null);
INSERT INTO `sys_log` VALUES ('d0263488a01390c2f128e4a9c4cc9daf', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:40:16', null, null);
INSERT INTO `sys_log` VALUES ('1997883678d894c5bf63cca7c22a6948', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:40:52', null, null);
INSERT INTO `sys_log` VALUES ('f5b8eb92bc5dfd0841fc020d78231299', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:41:05', null, null);
INSERT INTO `sys_log` VALUES ('d3167a43507802fa1273d77994397ea1', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:41:05', null, null);
INSERT INTO `sys_log` VALUES ('13a6bd9eff2a1c0c2f79155aca41a50b', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:42:57', null, null);
INSERT INTO `sys_log` VALUES ('cb0626a4872e1939690168f7b0a9d9c4', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:43:19', null, null);
INSERT INTO `sys_log` VALUES ('0574038e509e19a631a66da543caa20f', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:43:19', null, null);
INSERT INTO `sys_log` VALUES ('1870b82447ec0611cb17855924865ecd', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:46:50', null, null);
INSERT INTO `sys_log` VALUES ('bfbd8baf15dfefe481de958b6dbf2ef2', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:47:04', null, null);
INSERT INTO `sys_log` VALUES ('252904d70199d00f6a40592b2f5ea584', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-08 23:47:04', null, null);
INSERT INTO `sys_log` VALUES ('f1dfb76e46fd5c8ad16021864575e512', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-09 10:11:01', null, null);
INSERT INTO `sys_log` VALUES ('498feabf0fda7477e08681086760ea24', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-10 01:01:42', null, null);
INSERT INTO `sys_log` VALUES ('3eaaca2feb729feaa68f50ea582df66b', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-10 01:01:43', null, null);
INSERT INTO `sys_log` VALUES ('b388a8ca4a12505b1ce52a4e91678bae', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-10 02:03:47', null, null);
INSERT INTO `sys_log` VALUES ('d01a0d0b38ada2a7d99a75162de150e4', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-10 02:03:47', null, null);
INSERT INTO `sys_log` VALUES ('9fcc82fb9a1a87e70e3f19fabf9b1a89', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-10 11:20:14', null, null);
INSERT INTO `sys_log` VALUES ('2cf6ec4fac37d74f8181763e85ce1860', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-10 11:20:14', null, null);
INSERT INTO `sys_log` VALUES ('432e4c6aa808ecb0c774a33d0338997a', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 04:33:16', null, null);
INSERT INTO `sys_log` VALUES ('ee72af8b30ed37e09d74681980adb517', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 05:28:40', null, null);
INSERT INTO `sys_log` VALUES ('ecf9a00ed43272ab7fc15bb8c6241009', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 05:28:40', null, null);
INSERT INTO `sys_log` VALUES ('a1c7a53206bc537a21f3d430d29f31ac', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 05:33:54', null, null);
INSERT INTO `sys_log` VALUES ('74a725291e1d22a80ec509cc252b6cd9', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 05:41:58', null, null);
INSERT INTO `sys_log` VALUES ('e39325ac2d75d385d057e5d2e2489095', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 05:41:58', null, null);
INSERT INTO `sys_log` VALUES ('1af4562971af363c48121f53a45aaaa8', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 05:53:23', null, null);
INSERT INTO `sys_log` VALUES ('aa0b421f72a34ec1359d10097f28f72c', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 06:04:13', null, null);
INSERT INTO `sys_log` VALUES ('08819893f2cecd4c281e119599641f2c', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 06:04:13', null, null);
INSERT INTO `sys_log` VALUES ('d801fc7515ff0d38d9f08e847675d7c8', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-12 22:48:35', null, null);
INSERT INTO `sys_log` VALUES ('a3658b74189796123a96ba4193366fce', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-12 22:48:35', null, null);
INSERT INTO `sys_log` VALUES ('a355e5b1982ea479a53568d6ccd90a6d', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 23:36:37', null, null);
INSERT INTO `sys_log` VALUES ('a33315a5242883e1f51b7683e8b83bf7', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 23:36:51', null, null);
INSERT INTO `sys_log` VALUES ('10b60f57c2ccb1d3d482a1bc9df1e485', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-12 23:36:51', null, null);
INSERT INTO `sys_log` VALUES ('cef1521622e1a2a545e263c191f693a6', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-13 05:06:30', null, null);
INSERT INTO `sys_log` VALUES ('f237aa0d74d635ea1ce05bb97cc1c2a3', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-13 05:07:57', null, null);
INSERT INTO `sys_log` VALUES ('2a180fe57736e2817f33997edd3a49cd', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-13 05:07:57', null, null);
INSERT INTO `sys_log` VALUES ('9cf8fccb68b6dec9bf6b8514361d8161', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-19 02:49:37', null, null);
INSERT INTO `sys_log` VALUES ('5755ecf0afa63704e834430afac2d7c2', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-19 02:50:10', null, null);
INSERT INTO `sys_log` VALUES ('984dd0a10ca73c3d15ed9b6f81fb5fe1', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-19 02:50:10', null, null);
INSERT INTO `sys_log` VALUES ('5789802a086959c28debe2c3f71483d6', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 16:29:51', null, null);
INSERT INTO `sys_log` VALUES ('4ed07a72cb8e4a7272b5b466e8dd3f9d', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 16:30:27', null, null);
INSERT INTO `sys_log` VALUES ('d13b086bc6dbde05471b9b8f31013ab0', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 16:30:27', null, null);
INSERT INTO `sys_log` VALUES ('7489b6e0b8af4bfbda3987e0b35a2613', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 16:32:22', null, null);
INSERT INTO `sys_log` VALUES ('aa4d9e962818bb59ccf4ed31c63f5654', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 16:32:38', null, null);
INSERT INTO `sys_log` VALUES ('5d691bdc1bb1c358c269d2e59d1744bb', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 16:32:38', null, null);
INSERT INTO `sys_log` VALUES ('cf80d0d3534c9c405438ad712b0a8578', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 21:05:59', null, null);
INSERT INTO `sys_log` VALUES ('9acb04717bcc288ab9a13fb111ee81e4', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 21:06:27', null, null);
INSERT INTO `sys_log` VALUES ('1181f62b661999f14386e41f950fd841', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-20 21:06:27', null, null);
INSERT INTO `sys_log` VALUES ('a6b4eba3223331a04b68b2c44e9e3868', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:56:28', null, null);
INSERT INTO `sys_log` VALUES ('9693682acf71bb66d6766d6588dc0033', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:56:51', null, null);
INSERT INTO `sys_log` VALUES ('3930bf5b7a37337980c291b1b64048f7', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:56:51', null, null);
INSERT INTO `sys_log` VALUES ('b3591ddc0f6e100e283726f09f5781c6', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:57:52', null, null);
INSERT INTO `sys_log` VALUES ('489da2bc6c845155db80734128ad21a3', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:58:07', null, null);
INSERT INTO `sys_log` VALUES ('8867fdae7e40954f26235a65306fb9d4', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:58:07', null, null);
INSERT INTO `sys_log` VALUES ('a44b976e768f79f5aca5affcf54eb09a', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:58:07', null, null);
INSERT INTO `sys_log` VALUES ('2573502f2450b89d23d4e217333c7abe', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 12:58:07', null, null);
INSERT INTO `sys_log` VALUES ('aa4847d7b492285158e7cd7f285c309f', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 13:48:25', null, null);
INSERT INTO `sys_log` VALUES ('5795b93125fd4a9cac0aab74b0f0ac90', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 13:49:06', null, null);
INSERT INTO `sys_log` VALUES ('3fc74928b67bf0a9814543cb748b3231', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 13:49:06', null, null);
INSERT INTO `sys_log` VALUES ('6899573f6d09d59f7f51bebe610c6bb4', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 14:36:28', null, null);
INSERT INTO `sys_log` VALUES ('d4c4ff6e92b47fefe6573be6abe7d3d8', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 14:37:15', null, null);
INSERT INTO `sys_log` VALUES ('eaf86d63dc5c19e5713fba873343ae25', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-22 14:37:15', null, null);
INSERT INTO `sys_log` VALUES ('b720bfbd2e5da6d2d88edf2691c140fc', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 16:55:07', null, null);
INSERT INTO `sys_log` VALUES ('605e07a6ea8b78d9ea65e4aae71bdc76', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 16:55:07', null, null);
INSERT INTO `sys_log` VALUES ('68201bdc19df97e2319b82ca0b41318f', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:35:34', null, null);
INSERT INTO `sys_log` VALUES ('1e9ad52889ba21ed7ba49942589ee14d', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:35:53', null, null);
INSERT INTO `sys_log` VALUES ('0a3f2a13ecbc7f8c4a300a71e748d908', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:35:53', null, null);
INSERT INTO `sys_log` VALUES ('e6495ab34c2eb909fdcedee76cbd6b53', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:37:12', null, null);
INSERT INTO `sys_log` VALUES ('722758ec4b6f4bd3040878b572b71f8d', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:37:26', null, null);
INSERT INTO `sys_log` VALUES ('608f6cbf0d84f7269918e043f10365d5', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:37:26', null, null);
INSERT INTO `sys_log` VALUES ('b891392d87834a060ab3a1f494328bd7', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:39:03', null, null);
INSERT INTO `sys_log` VALUES ('cc9a2662cfcf22cf8bbadd576705e8f8', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:39:27', null, null);
INSERT INTO `sys_log` VALUES ('1a90ddaab8585703da3d88ab04e2c7b9', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:39:27', null, null);
INSERT INTO `sys_log` VALUES ('66b3c89f3a638f050aa45b5c2f805235', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:39:49', null, null);
INSERT INTO `sys_log` VALUES ('2f7b192d0535637bac2aad6a8e4d686c', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:40:02', null, null);
INSERT INTO `sys_log` VALUES ('a69ee74c6831378ad2534cea036d7bf2', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-24 22:40:02', null, null);
INSERT INTO `sys_log` VALUES ('0486720353ade8dab09751dc7057ccb8', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 00:28:24', null, null);
INSERT INTO `sys_log` VALUES ('c71ab38bca909a8b7df6d1f459ba3509', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 00:29:23', null, null);
INSERT INTO `sys_log` VALUES ('486b3a2ef647e929a3b7018a236889b1', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 00:29:23', null, null);
INSERT INTO `sys_log` VALUES ('5ad37ab5bb26aa390ff73c80c388971a', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 17:30:39', null, null);
INSERT INTO `sys_log` VALUES ('e0748a622e3148e6d3ae698873b1fb38', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 17:30:52', null, null);
INSERT INTO `sys_log` VALUES ('d85339c6fc8368648deb601a5a24539a', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 17:30:52', null, null);
INSERT INTO `sys_log` VALUES ('1a810119a62d5648af8f81e8da6af0a8', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 17:49:03', null, null);
INSERT INTO `sys_log` VALUES ('c41bac8be298a442aeedb33cc9ffaa81', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 17:49:48', null, null);
INSERT INTO `sys_log` VALUES ('c096e3407d379cb84262dbef578590c9', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 17:49:48', null, null);
INSERT INTO `sys_log` VALUES ('799d94ac7deecde42878c1fbbba376d8', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:13:06', null, null);
INSERT INTO `sys_log` VALUES ('6c0b7cb4138f6c73341b6df1d980441f', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:13:19', null, null);
INSERT INTO `sys_log` VALUES ('dd268e8cf6ff727c3094fdc060f0af4a', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:13:19', null, null);
INSERT INTO `sys_log` VALUES ('aa5ccd351171473e5fa783809f402490', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:14:30', null, null);
INSERT INTO `sys_log` VALUES ('fe58b080542c20db37545a0e77f88874', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:14:42', null, null);
INSERT INTO `sys_log` VALUES ('afbfdc81aba3406090cd0f9646142439', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:14:42', null, null);
INSERT INTO `sys_log` VALUES ('eeecd1368a75d981c5e9f2bd4853ceaf', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:17:02', null, null);
INSERT INTO `sys_log` VALUES ('eebba01f854fb1dfb6c2108521ecef0b', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:17:36', null, null);
INSERT INTO `sys_log` VALUES ('c2666c6d52be31667ca32f0966092d15', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-25 18:17:36', null, null);
INSERT INTO `sys_log` VALUES ('72a79fc576af39a01f4a090d489ca7c9', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-26 00:36:49', null, null);
INSERT INTO `sys_log` VALUES ('82d050802302b34228a49d516e7be5cb', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-26 00:36:49', null, null);
INSERT INTO `sys_log` VALUES ('723e10a66d0caebe708bedb36ae9d7d6', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 01:15:16', null, null);
INSERT INTO `sys_log` VALUES ('073d607704848c5361c1c464063e6e7c', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 01:15:31', null, null);
INSERT INTO `sys_log` VALUES ('b5c3c7b09759cce93cdc237063f3e3ad', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 01:15:31', null, null);
INSERT INTO `sys_log` VALUES ('bd533ab74338bfbc649728730f124e3d', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 02:15:10', null, null);
INSERT INTO `sys_log` VALUES ('f0f3fa42032fc92867ccfc1ef63463be', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 02:15:25', null, null);
INSERT INTO `sys_log` VALUES ('15c8fba1000e69e85266cce4df0cbdf7', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 02:15:25', null, null);
INSERT INTO `sys_log` VALUES ('fd2a88b5add83a17c70c3356eb7a7733', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 02:44:52', null, null);
INSERT INTO `sys_log` VALUES ('7cc1023786cc3cd0b37c8d1ea3e97066', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 02:44:52', null, null);
INSERT INTO `sys_log` VALUES ('299e4a9c9d8d86c5496b01b22d5828a1', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 03:00:37', null, null);
INSERT INTO `sys_log` VALUES ('3b1a2335f5e155722b7720fa0da4b8d5', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 03:00:53', null, null);
INSERT INTO `sys_log` VALUES ('34220f17dd159dc31a501440b7d730d3', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 03:00:53', null, null);
INSERT INTO `sys_log` VALUES ('e7fe27d2b87ef0d55269d6b634cff94b', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:51:27', null, null);
INSERT INTO `sys_log` VALUES ('97893b8763f3a34da771da1f24cdd6b4', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:52:26', null, null);
INSERT INTO `sys_log` VALUES ('0a81e2a50f17a3f543c7cfad8062b92e', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:52:26', null, null);
INSERT INTO `sys_log` VALUES ('272343c2a29bd82587cbcc7d31e908ec', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:54:31', null, null);
INSERT INTO `sys_log` VALUES ('9d8cab2a11804b99d005bee2e6e3de4c', '1', '用户名1: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:55:33', null, null);
INSERT INTO `sys_log` VALUES ('5295e5172a218ba3759389f8220ea91a', '1', '用户名2: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:55:33', null, null);
INSERT INTO `sys_log` VALUES ('daa5ddb1d7bb7a500ee9ac868048159f', '1', '用户名: 11111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:56:11', null, null);
INSERT INTO `sys_log` VALUES ('e61f9ffdc9d61df803ba1641457f4420', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:56:27', null, null);
INSERT INTO `sys_log` VALUES ('464239c8555ca6f234ee140b8a47f09d', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 04:56:27', null, null);
INSERT INTO `sys_log` VALUES ('7dc3f84ac8ca40651acda98c28cee777', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:27:18', null, null);
INSERT INTO `sys_log` VALUES ('00dadaa34f82b03708bf26c7efb212f0', '1', '用户名1: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:27:31', null, null);
INSERT INTO `sys_log` VALUES ('8593e2b6864f28aa225f76dd84a97e54', '1', '用户名2: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:27:31', null, null);
INSERT INTO `sys_log` VALUES ('15dce78c9f4765cb242485f5b473d496', '1', '用户名: 11111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:28:43', null, null);
INSERT INTO `sys_log` VALUES ('8f3ebea76a7c80f369441aa80db5ef21', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:28:56', null, null);
INSERT INTO `sys_log` VALUES ('6ac92ca19697268ac18a5f6563959d06', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:28:56', null, null);
INSERT INTO `sys_log` VALUES ('bf1d9628b9b4bc016cb3fc027eb29f9e', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:40:46', null, null);
INSERT INTO `sys_log` VALUES ('597bc3e1e034dbd9bde7ef9a7b434670', '1', '用户名1: 111111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:41:01', null, null);
INSERT INTO `sys_log` VALUES ('5ce015e959434096f5a2da26c7b91eaf', '1', '用户名2: 111111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:41:02', null, null);
INSERT INTO `sys_log` VALUES ('8dc4cd5f3045bb023f29d653b2b06cee', '1', '用户名: 111111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:49:06', null, null);
INSERT INTO `sys_log` VALUES ('80259a29e7de8ab613e31c2dd34fcb6f', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:49:22', null, null);
INSERT INTO `sys_log` VALUES ('a22cc64749682801d9d1fa6e7bcb0f3d', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 05:49:22', null, null);
INSERT INTO `sys_log` VALUES ('89f9cc468aa2ed7f098cd5b77f32511a', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:24:11', null, null);
INSERT INTO `sys_log` VALUES ('50e41d6c2e9d3a17f94732f338198418', '1', '用户名1: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:24:29', null, null);
INSERT INTO `sys_log` VALUES ('a8c4dd67105cd3bbd07ea3c2e5fbd53f', '1', '用户名2: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:24:29', null, null);
INSERT INTO `sys_log` VALUES ('308c00ebc2b627d31cef33834cc3b23f', '1', '用户名: 11111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:24:39', null, null);
INSERT INTO `sys_log` VALUES ('ebd963e2899a77ca955ecc5f4bfb9b39', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:24:52', null, null);
INSERT INTO `sys_log` VALUES ('15472250f7b6490c0db1a32074d90191', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:24:52', null, null);
INSERT INTO `sys_log` VALUES ('3c726ac4a8734a22cd4d5086c708dc2c', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:27:53', null, null);
INSERT INTO `sys_log` VALUES ('bd2e26a01904fc56fd174aea2e3703e3', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:28:06', null, null);
INSERT INTO `sys_log` VALUES ('7b85b61da3d2322bdb866850cb6ce09b', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:28:06', null, null);
INSERT INTO `sys_log` VALUES ('5e57548c9c62708094e3d3cf523805b4', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:29:55', null, null);
INSERT INTO `sys_log` VALUES ('6dd0c965e3460d1a6961d5913fccbcd9', '1', '用户名1: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:30:09', null, null);
INSERT INTO `sys_log` VALUES ('bf60b2693bedffce317e86694496446b', '1', '用户名2: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:30:09', null, null);
INSERT INTO `sys_log` VALUES ('e05522f3a19e0b058a7f9dcc2ccc5d29', '1', '用户名: 11111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:30:21', null, null);
INSERT INTO `sys_log` VALUES ('f541b20b6b094f2f8d9562060bc8f91c', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:30:36', null, null);
INSERT INTO `sys_log` VALUES ('3ab4cbd434a1383d114d20f850095467', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 06:30:36', null, null);
INSERT INTO `sys_log` VALUES ('0afdffc4babb3820aa029532cf60290b', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:17:52', null, null);
INSERT INTO `sys_log` VALUES ('d93819236b102687a4a3264b9396df2f', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:18:08', null, null);
INSERT INTO `sys_log` VALUES ('cf1ae28fb8f56611f8b1dde4a2ed80b2', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:18:08', null, null);
INSERT INTO `sys_log` VALUES ('006e6cf6e87830594ff0e06c21bf3f76', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:18:59', null, null);
INSERT INTO `sys_log` VALUES ('5e842dbe5817ac3f87013c37ac5ae45f', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:19:12', null, null);
INSERT INTO `sys_log` VALUES ('3b4a1fdca4edcccd0405302210ccf783', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:19:12', null, null);
INSERT INTO `sys_log` VALUES ('cd81e2abaee85aa13b97e671374be688', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:19:20', null, null);
INSERT INTO `sys_log` VALUES ('01779f9ddbd1c1103101cbd92f5e0259', '1', '用户名1: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:19:35', null, null);
INSERT INTO `sys_log` VALUES ('4876b99fc50a61de8164bc96a2acd3d4', '1', '用户名2: 11111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:19:35', null, null);
INSERT INTO `sys_log` VALUES ('c15c91e3bb8fced22f1c923e313ecdcf', '1', '用户名: 11111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:20:23', null, null);
INSERT INTO `sys_log` VALUES ('5513190f767f571e92097a4c8db169c6', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:20:34', null, null);
INSERT INTO `sys_log` VALUES ('c0ba4191c9a83c2a81f487d7d61dab00', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:20:34', null, null);
INSERT INTO `sys_log` VALUES ('0a1a6d8493a2f5ef75580d6b933226be', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:32:37', null, null);
INSERT INTO `sys_log` VALUES ('1dc64ac3911350f230561e89a74a722a', '1', '用户名1: test,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:32:57', null, null);
INSERT INTO `sys_log` VALUES ('7c6f3928f77f290f3473567de2d1855f', '1', '用户名2: test,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:32:57', null, null);
INSERT INTO `sys_log` VALUES ('d633ec6fb1a441e2bc13610fc8293e90', '1', '用户名: test,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:34:48', null, null);
INSERT INTO `sys_log` VALUES ('037ee286a6260eea3d08cece762c7391', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:35:01', null, null);
INSERT INTO `sys_log` VALUES ('3c9e4719f93f1ef52be4e8f54c1b5ead', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:35:01', null, null);
INSERT INTO `sys_log` VALUES ('ed8b5062ec04f729ae8bd714caf19d56', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:35:30', null, null);
INSERT INTO `sys_log` VALUES ('30c8328ef42341f7a9dbcca66c59d7cb', '1', '用户名1: test,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:35:56', null, null);
INSERT INTO `sys_log` VALUES ('d94e57ee1b7f66ddf5d37d02862fa20f', '1', '用户名2: test,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:35:56', null, null);
INSERT INTO `sys_log` VALUES ('8beaa19d82193d52bbd80103fd94bd78', '1', '用户名: test,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:54:55', null, null);
INSERT INTO `sys_log` VALUES ('07973da4d813b4be79a1104cacd4c2ab', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:55:13', null, null);
INSERT INTO `sys_log` VALUES ('2f3a96cf2f7fc6db059cff87143ca404', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 07:55:13', null, null);
INSERT INTO `sys_log` VALUES ('a5527fa41818152783f85f28e788c6ea', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 13:41:11', null, null);
INSERT INTO `sys_log` VALUES ('4e83d6e7116d2e88553534e742badfcc', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 13:41:25', null, null);
INSERT INTO `sys_log` VALUES ('04a107e7218688fe80e69ea43fb7828e', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 13:41:25', null, null);
INSERT INTO `sys_log` VALUES ('6d3fc7c57958cdbd76a6e9a5a80ec4e6', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 14:34:07', null, null);
INSERT INTO `sys_log` VALUES ('46965bc5708129c90ecec8421f34e978', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 14:34:23', null, null);
INSERT INTO `sys_log` VALUES ('5fc25b4e681370c3281fa70eca60dc58', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 14:34:23', null, null);
INSERT INTO `sys_log` VALUES ('86fb9617475dd91d7e0b67af551dc7c8', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 14:35:08', null, null);
INSERT INTO `sys_log` VALUES ('9d616efd9b24b9f039d433483952c4eb', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 14:35:50', null, null);
INSERT INTO `sys_log` VALUES ('573c8d190c28ce20aba76461facb0345', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 14:35:50', null, null);
INSERT INTO `sys_log` VALUES ('303265afe9acb0b4999e075d39451c12', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 15:33:08', null, null);
INSERT INTO `sys_log` VALUES ('71ada066bf01ab536d0463212f722145', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 15:33:23', null, null);
INSERT INTO `sys_log` VALUES ('c269eaa9d6ae1abd9a90b832949b4138', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 15:33:23', null, null);
INSERT INTO `sys_log` VALUES ('df8843bfb6ff407f58e3a63103843d72', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 17:20:48', null, null);
INSERT INTO `sys_log` VALUES ('3a5a6c5b7fcf9941aab4a0a26a3542e9', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 17:21:04', null, null);
INSERT INTO `sys_log` VALUES ('fcfa43bdd8119df874013f5653921c7e', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 17:21:04', null, null);
INSERT INTO `sys_log` VALUES ('2cec64ecc511cd95d7cc5284c326ef48', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 18:46:12', null, null);
INSERT INTO `sys_log` VALUES ('72dd35b3f1162e7245eb49f6d2c2d7cd', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 18:46:45', null, null);
INSERT INTO `sys_log` VALUES ('27ec6b3fe984298262ddfb83f0932f32', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 18:46:45', null, null);
INSERT INTO `sys_log` VALUES ('33d9d057b0ad4f556b82b1e4b776766f', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:30:30', null, null);
INSERT INTO `sys_log` VALUES ('ae3e4ade459fb82c88c80d8094b6d6da', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:30:47', null, null);
INSERT INTO `sys_log` VALUES ('edcee6e8b2c18ccf159605ae352a76a0', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:30:47', null, null);
INSERT INTO `sys_log` VALUES ('ef3bb0916ced8fec11725b46690c0bbf', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:51:45', null, null);
INSERT INTO `sys_log` VALUES ('5e410ac62dec8cc1035db6441763a3b3', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:52:03', null, null);
INSERT INTO `sys_log` VALUES ('a8f6e003cff5950061d22870a67bb77b', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:52:03', null, null);
INSERT INTO `sys_log` VALUES ('79ce98a5e68bb7baa3491407d26e32fa', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:52:42', null, null);
INSERT INTO `sys_log` VALUES ('4c4ff3756ecc840e231388850c3b4ae5', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:52:54', null, null);
INSERT INTO `sys_log` VALUES ('3e53727b6b117e2d494a21fcfe298e77', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:52:54', null, null);
INSERT INTO `sys_log` VALUES ('8a6dfd066aee428a0900818201a92625', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:53:47', null, null);
INSERT INTO `sys_log` VALUES ('71a2c95d5dfc552792d43ae7831ab193', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:54:01', null, null);
INSERT INTO `sys_log` VALUES ('7bb910e90aadd5692ab23840842dc049', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:54:01', null, null);
INSERT INTO `sys_log` VALUES ('01a34689bbbd45dd8394743ea483f33c', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:57:32', null, null);
INSERT INTO `sys_log` VALUES ('bc2e833f0a41af479e931c3a0596f75a', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:57:48', null, null);
INSERT INTO `sys_log` VALUES ('b986809f1bbfac3c997dbbd80430baca', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:57:48', null, null);
INSERT INTO `sys_log` VALUES ('7c355aa3b071584973eb7b82921e147c', '1', '用户名: test2,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:58:06', null, null);
INSERT INTO `sys_log` VALUES ('4df576dd7da27b44385c1dda9dda1ea3', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:58:32', null, null);
INSERT INTO `sys_log` VALUES ('dd894f0ec5e30bd01c8e6e9d8ab5ba71', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 20:58:32', null, null);
INSERT INTO `sys_log` VALUES ('8baa4945a7fc482f307c14d83a5e8726', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 21:18:59', null, null);
INSERT INTO `sys_log` VALUES ('f3c53cfbb338195b22521c200a2a8c83', '1', '用户名1: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 21:19:13', null, null);
INSERT INTO `sys_log` VALUES ('15926478c9ee67af4c9cd7a563c727ea', '1', '用户名2: 1111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 21:19:13', null, null);
INSERT INTO `sys_log` VALUES ('d6d96f83d988b03957b81379c4093e00', '1', '用户名: 1111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 21:47:35', null, null);
INSERT INTO `sys_log` VALUES ('3599e2c6a4d059c48d0338479e0c24bc', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 21:47:54', null, null);
INSERT INTO `sys_log` VALUES ('ddb05258b77ac76a928596b69e2dc724', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-26 21:47:54', null, null);
INSERT INTO `sys_log` VALUES ('e43d81f92ca89ffc0dd1aa021e6ea597', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 15:40:40', null, null);
INSERT INTO `sys_log` VALUES ('12a3c7fc49abcaa646bb460a97b87ef7', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 16:46:34', null, null);
INSERT INTO `sys_log` VALUES ('5ebbab04c78e274e77bca6681c3be9a8', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 16:46:34', null, null);
INSERT INTO `sys_log` VALUES ('5bed3a74e755c6504af701cb7e3b2dc3', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 16:56:23', null, null);
INSERT INTO `sys_log` VALUES ('2e476c1398841efe590fe4a851c1d494', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 16:56:34', null, null);
INSERT INTO `sys_log` VALUES ('80f652a8267333a509cc13c8df862644', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 16:56:34', null, null);
INSERT INTO `sys_log` VALUES ('60d9bbaa15ab276e620ec5c4681734fd', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 17:40:24', null, null);
INSERT INTO `sys_log` VALUES ('7af8b865c0fa81e6ecab839d4811e21c', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 17:40:57', null, null);
INSERT INTO `sys_log` VALUES ('ad1837f538a412a331fb084a2930e38f', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 17:40:57', null, null);
INSERT INTO `sys_log` VALUES ('7a9d753523d68602d5bea98c303ed04f', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 17:41:06', null, null);
INSERT INTO `sys_log` VALUES ('d98185bbbbbfdf3f3551431842dc24bb', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 17:41:18', null, null);
INSERT INTO `sys_log` VALUES ('9f0bea7a9a19a1a28e935e3e5e1dbf82', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-28 17:41:18', null, null);
INSERT INTO `sys_log` VALUES ('2d24b057e2f52794e6f6e57803fd4df9', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-28 21:02:36', null, null);
INSERT INTO `sys_log` VALUES ('66c19b4f0100289d942437e8a05b41d3', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-28 21:02:36', null, null);
INSERT INTO `sys_log` VALUES ('c97503b302b85b8f3d407b72f65fd089', '1', '用户名1: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-29 02:54:56', null, null);
INSERT INTO `sys_log` VALUES ('c763f6727dd49a9cbbe7e7deab3ba011', '1', '用户名2: 4,登录成功！', null, null, null, '127.0.0.1', null, null, null, null, null, null, '2021-09-29 02:54:56', null, null);
INSERT INTO `sys_log` VALUES ('e717acf1f4838fd2a786e449c53d2b7c', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:21:17', null, null);
INSERT INTO `sys_log` VALUES ('cedc65f3ce11b0c2c4ae0ffd1a0dd7ab', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:21:27', null, null);
INSERT INTO `sys_log` VALUES ('eb03176fafc95e346d22f50b37e48753', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:21:27', null, null);
INSERT INTO `sys_log` VALUES ('6a15a7c5a90a41c467a40375eeac34fa', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:43:57', null, null);
INSERT INTO `sys_log` VALUES ('ce55701eb4b1d3963d26b9b5837ad66e', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:45:48', null, null);
INSERT INTO `sys_log` VALUES ('056963ef8522e63fe69b246b0d36c2ed', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:45:48', null, null);
INSERT INTO `sys_log` VALUES ('b4ce09b16ae4b269c50a79839c300fa6', '1', '用户名: oKBv6w16s4yqD96a6ROIz4gCtEKc,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:47:09', null, null);
INSERT INTO `sys_log` VALUES ('8822b5b092a33221bf663e0cf4ac53c5', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:47:26', null, null);
INSERT INTO `sys_log` VALUES ('33bee5f5f8e181c1e145a59733ad0f9f', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 04:47:26', null, null);
INSERT INTO `sys_log` VALUES ('dcc4dd1e31d3c09d01b3eb34ddceadc2', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 13:55:56', null, null);
INSERT INTO `sys_log` VALUES ('770831102a61ba94a96f575fe944abf2', '1', '用户名1: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 13:56:09', null, null);
INSERT INTO `sys_log` VALUES ('f23314f415387ac339d753296dde1c2a', '1', '用户名2: 4,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 13:56:09', null, null);
INSERT INTO `sys_log` VALUES ('1e9037fcc9ee57039fe19c11c0fa9e51', '1', '用户名: 4,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 13:56:27', null, null);
INSERT INTO `sys_log` VALUES ('08ee60e2d045961f0d901fbd0eb3335f', '1', '用户名1: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 13:56:39', null, null);
INSERT INTO `sys_log` VALUES ('cb4b92dfd8eda5f586a74de2944f54c7', '1', '用户名2: 111,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 13:56:39', null, null);
INSERT INTO `sys_log` VALUES ('2bb06a20a3b1c5953ffff18c2277864e', '1', '用户名: 111,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 15:09:01', null, null);
INSERT INTO `sys_log` VALUES ('e00b80c16cafacd0d5637846365a7a44', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 15:09:21', null, null);
INSERT INTO `sys_log` VALUES ('d7e899bfecc15f0e05e912e64a6244e0', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 15:09:21', null, null);
INSERT INTO `sys_log` VALUES ('0c36e0fa0c463f6666bec3e66a5449be', '1', '用户名: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:12:40', null, null);
INSERT INTO `sys_log` VALUES ('ebe9a5cf3dd0182f2bafe87fc55b2793', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:13:05', null, null);
INSERT INTO `sys_log` VALUES ('975f78821380bf015a4d59cf3c79b235', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:13:05', null, null);
INSERT INTO `sys_log` VALUES ('aa33072ddaa8e03d4a9aa1fc0aea3c58', '1', '用户名1: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:13:05', null, null);
INSERT INTO `sys_log` VALUES ('ac19a21fa5b32805d6e9bdb009ebffda', '1', '用户名2: test2,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:13:05', null, null);
INSERT INTO `sys_log` VALUES ('683305756b2123274c6fd878fd4ca189', '1', '用户名: test2,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:14:01', null, null);
INSERT INTO `sys_log` VALUES ('aba8da8d78bb5cd6c78db90f80d7f9c9', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:14:19', null, null);
INSERT INTO `sys_log` VALUES ('47a4d8ac3f26749502ba6dcfc36ef47b', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:14:19', null, null);
INSERT INTO `sys_log` VALUES ('bd41b29719c53947d842b72c36ea6e36', '1', '用户名: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:15:40', null, null);
INSERT INTO `sys_log` VALUES ('ab5b713a8a016398e8504158a2e69fe5', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:32:16', null, null);
INSERT INTO `sys_log` VALUES ('0a05a802956f416bcbc178e521dbb96b', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:32:16', null, null);
INSERT INTO `sys_log` VALUES ('42a8f143cf12ebacba36729dca05c772', '1', '用户名: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:35:03', null, null);
INSERT INTO `sys_log` VALUES ('6b35f7bf8680e15815f28a79c85c43c7', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:35:20', null, null);
INSERT INTO `sys_log` VALUES ('55e4f8385182751af2affd67a8d159b8', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:35:20', null, null);
INSERT INTO `sys_log` VALUES ('4f598c0ba036f83360ab934db53b6d29', '1', '用户名: UID_1F4D2E16DAB58CCD8CBD4480C595C132,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:39:12', null, null);
INSERT INTO `sys_log` VALUES ('6fdd1f1bfcaa4bccc8b20120f18e3d45', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:39:22', null, null);
INSERT INTO `sys_log` VALUES ('8a136b9b5848c827ab1d9af31a67607f', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:39:22', null, null);
INSERT INTO `sys_log` VALUES ('c348f14c66d8ceac58461af736550633', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:44:20', null, null);
INSERT INTO `sys_log` VALUES ('26fbd99409e084611ec3781c62515c03', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 16:44:20', null, null);
INSERT INTO `sys_log` VALUES ('c9ce82013c0937548deaffa1c7957442', '1', '用户名: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 17:18:49', null, null);
INSERT INTO `sys_log` VALUES ('97c4acdac05d539e88866f831df5a727', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 17:19:15', null, null);
INSERT INTO `sys_log` VALUES ('1642514773d56ee49800d3e5c0d140b8', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 17:19:15', null, null);
INSERT INTO `sys_log` VALUES ('4b31cbf3a4080681e561ead39d5f35e6', '1', '用户名: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 17:19:34', null, null);
INSERT INTO `sys_log` VALUES ('41c41f8b32340ad9e4d54624040ad78d', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 17:19:44', null, null);
INSERT INTO `sys_log` VALUES ('1b1b0ffdadba3e857e281ee9be878d63', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 17:19:44', null, null);
INSERT INTO `sys_log` VALUES ('85121a603f75107307e24bc7ff2479a7', '1', '用户名: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 18:13:31', null, null);
INSERT INTO `sys_log` VALUES ('9c7d87a24d10b67d6c1e9183a1cbf300', '1', '用户名1: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 18:14:44', null, null);
INSERT INTO `sys_log` VALUES ('125e3754fd0aba88fa5499f5ab48deee', '1', '用户名2: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 18:14:44', null, null);
INSERT INTO `sys_log` VALUES ('850c3e6bf290f04c9fb58bb5ba6a0aa6', '1', '用户名: oKBv6w2LZZ3uVdp0VNBi0VF35pVk,退出成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 18:15:26', null, null);
INSERT INTO `sys_log` VALUES ('306ff5b2eb4520c00196f014bd2042f8', '1', '用户名1: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 18:16:11', null, null);
INSERT INTO `sys_log` VALUES ('e4eb5095f77ef3ad91335188f8da6221', '1', '用户名2: UID_1F4D2E16DAB58CCD8CBD4480C595C132,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 18:16:11', null, null);
INSERT INTO `sys_log` VALUES ('6d310bc81b9aafdb431a55acaf2164f5', '1', '用户名1: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 20:55:26', null, null);
INSERT INTO `sys_log` VALUES ('a0baf7bfadd4e7566bccfdf703be34f7', '1', '用户名2: oKBv6w16s4yqD96a6ROIz4gCtEKc,登录成功！', null, null, null, '192.168.10.136', null, null, null, null, null, null, '2021-09-29 20:55:26', null, null);
INSERT INTO `sys_log` VALUES ('324ec28ce18f716b62daeff0338b9ae2', '1', '用户名1: oKBv6wy6ccTE0W_7e-sq9edTh0wU,登录成功！', null, null, null, '192.168.10.169', null, null, null, null, null, null, '2021-09-29 21:04:16', null, null);
INSERT INTO `sys_log` VALUES ('276926f30f02e5d844f5cd7124685d43', '1', '用户名2: oKBv6wy6ccTE0W_7e-sq9edTh0wU,登录成功！', null, null, null, '192.168.10.169', null, null, null, null, null, null, '2021-09-29 21:04:16', null, null);

-- ----------------------------
-- Table structure for `ucenter_fee`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_fee`;
CREATE TABLE `ucenter_fee` (
  `uuid` varchar(32) DEFAULT NULL,
  `orderno` varchar(32) DEFAULT NULL,
  `fee` varchar(11) DEFAULT NULL,
  `creatime` datetime DEFAULT NULL,
  `isok` int(1) DEFAULT NULL,
  `uid` varchar(32) DEFAULT NULL,
  `prepayid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_fee
-- ----------------------------
INSERT INTO `ucenter_fee` VALUES ('adddfe2153da7a1cf3bf26c92fb8c9bc', 'no_20507469451', '1', '2021-09-28 23:55:20', '0', '674b126fec114cdd6a30b1c20f78b337', null);
INSERT INTO `ucenter_fee` VALUES ('ca2353cb4db15dd1b75dd17bb76109a3', 'no_19366675661', '1', '2021-09-29 00:11:06', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2900144820558036e3598b9c1949a70000');
INSERT INTO `ucenter_fee` VALUES ('716f71a1c5691327387d7be09378309d', 'no_8242479532', '1', '2021-09-29 00:30:10', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290033525305591400395677e267b00000');
INSERT INTO `ucenter_fee` VALUES ('0e34032c9d4c489c16fe495667820a52', 'no_7355084721', '1', '2021-09-29 00:37:47', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2900412910917702b66875ad924e8c0000');
INSERT INTO `ucenter_fee` VALUES ('17cb840c5953653fd72a1fcaf49ee790', 'no_14320998011', '1', '2021-09-29 01:06:04', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2901094638303234d89592f2fd0ae10000');
INSERT INTO `ucenter_fee` VALUES ('e6b0e33a087fce6af22fe901067964f4', 'no_8386533772', '1', '2021-09-29 01:08:43', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29011225567940b325a9bfaa5bd5920000');
INSERT INTO `ucenter_fee` VALUES ('878dab2b41ad98367c0a758ce34ccffd', 'no_14995804573', '1', '2021-09-29 01:11:03', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29011444998025587ce93cc96924e30000');
INSERT INTO `ucenter_fee` VALUES ('ea2e501a996d9835af180de49bec32d3', 'no_4696886581', '1', '2021-09-29 01:18:17', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2901215885671434648f59a0e91c580000');
INSERT INTO `ucenter_fee` VALUES ('055e3247f1eb9ccab7a57d7220fb4a7e', 'no_18336848392', '1', '2021-09-29 01:23:15', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290126576870788f6ffa05e03c69480000');
INSERT INTO `ucenter_fee` VALUES ('4b37688c3c667210bdbf85e7314129f9', 'no_19886806883', '1', '2021-09-29 01:27:17', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29013059640226c5e640ebea30238e0000');
INSERT INTO `ucenter_fee` VALUES ('365e7d155f39d8e0414970565d149c97', 'no_1396421141', '1', '2021-09-29 01:49:42', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29015324626934a671e292b2dbba6c0000');
INSERT INTO `ucenter_fee` VALUES ('bebb98461fd8dbf488555659edf11c03', 'no_3846025492', '1', '2021-09-29 01:54:49', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290158314438241da077f191395fd50000');
INSERT INTO `ucenter_fee` VALUES ('bcc3bffc169e9f055b43c95a2eb59cdd', 'no_10765766521', '1', '2021-09-29 01:56:36', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29020018170099798fc7e83d9470db0000');
INSERT INTO `ucenter_fee` VALUES ('2cdb2070a8aed091d0912a440b912282', 'no_17938499841', '1', '2021-09-29 02:02:06', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290205479682603e9778fbba90ea7f0000');
INSERT INTO `ucenter_fee` VALUES ('d8110cac74036ea40c887d5b9370ae37', 'no_12664671951', '1', '2021-09-29 02:08:13', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290211547880487ef3c2b0442ed7f30000');
INSERT INTO `ucenter_fee` VALUES ('204e311356f30756fbbe03c8adba46e8', 'no_16024599981', '1', '2021-09-29 02:11:13', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29021455034331c2b0bd8aeb15d8bc0000');
INSERT INTO `ucenter_fee` VALUES ('7db090c645f84c018b3f0a9a3b819d40', 'no_7054734891', '100', '2021-09-29 02:25:55', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2902293746157632e96d4dfc729cb60000');
INSERT INTO `ucenter_fee` VALUES ('4f13aa426a7c4bcf0f76cdc3b1eeed8d', 'no_13974984361', '100', '2021-09-29 02:42:40', '1', '674b126fec114cdd6a30b1c20f78b337', 'wx290246223465293a46b899d0bdcac40000');
INSERT INTO `ucenter_fee` VALUES ('6c4f64623226cf6ab962d005f567b615', 'no_2819464461', '100', '2021-09-29 02:51:41', '1', '674b126fec114cdd6a30b1c20f78b337', 'wx290255229250196628467417b8ac800000');
INSERT INTO `ucenter_fee` VALUES ('1102860cb98421862f6c7146c8aab20b', 'no_12585520362', '1000', '2021-09-29 03:05:07', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290308488207182119b679b02377ff0000');
INSERT INTO `ucenter_fee` VALUES ('6fd81d5bea621476cdfab9a67719c699', 'no_18289632093', '9800', '2021-09-29 03:05:20', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29030901882609ee4e66eeca930b640000');
INSERT INTO `ucenter_fee` VALUES ('c32ed550dd2c9fdce38320554cbc620b', 'no_11336965124', '29800', '2021-09-29 03:05:29', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290309111566903ed6177ac04f143c0000');
INSERT INTO `ucenter_fee` VALUES ('422c59b77f6f87a02fe41be7793c29bb', 'no_20796631955', '1000', '2021-09-29 03:05:43', '1', '674b126fec114cdd6a30b1c20f78b337', 'wx2903092541206207adddf9ff6d6f440000');
INSERT INTO `ucenter_fee` VALUES ('bcb8aa66fc07b44afd54ad02d0ffad33', 'no_5395342396', '29800', '2021-09-29 03:09:57', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290313390983748faa2bc117baa1c50000');
INSERT INTO `ucenter_fee` VALUES ('5527d395f6edd6f30a7226fbb33fe184', 'no_21367929737', '19800', '2021-09-29 03:10:04', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290313461481441dc3a7f9e47321ad0000');
INSERT INTO `ucenter_fee` VALUES ('fd30789674e6e0a591a303038aecc60e', 'no_7075779428', '9800', '2021-09-29 03:10:11', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29031352845456120c643d1589b62e0000');
INSERT INTO `ucenter_fee` VALUES ('f15366bb876ac9b44cb991e61e091ca6', 'no_8190218279', '1000', '2021-09-29 03:10:18', '1', '674b126fec114cdd6a30b1c20f78b337', 'wx290314006388501db482f1ef767aad0000');
INSERT INTO `ucenter_fee` VALUES ('ecdf24d388bbef70a2f05b44297d4f8b', 'no_11516530611', '1000', '2021-09-29 03:22:09', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2903255139541187e5d3475b1bc46f0000');
INSERT INTO `ucenter_fee` VALUES ('68c04f4b67faf8de85529eb3634aec9a', 'no_14112439412', '29800', '2021-09-29 03:22:19', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx290326011497144f08d77ebd64eee00000');
INSERT INTO `ucenter_fee` VALUES ('7bfe5f5d053aa9c97c74ae8755a57b9b', 'no_20211569993', '19800', '2021-09-29 03:22:24', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29032606580976509832c0ffb154f10000');
INSERT INTO `ucenter_fee` VALUES ('30f2001c05df7153338626b7e0bc67a6', 'no_11723487434', '1000', '2021-09-29 03:22:30', '1', '674b126fec114cdd6a30b1c20f78b337', 'wx2903261235586072598037cdac2b2a0000');
INSERT INTO `ucenter_fee` VALUES ('988f0706da0dacbf996fa8c06367dffa', 'no_11516530611', '1000', '2021-09-29 03:24:19', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2903280165166587e5d3475b978b840000');
INSERT INTO `ucenter_fee` VALUES ('eca4a629624cbde358a85f8e4be0cfea', 'no_19370552655', '1000', '2021-09-29 03:28:13', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx29033155683548adbc459cdd2da2df0000');
INSERT INTO `ucenter_fee` VALUES ('f7e399cead55557006cb0131543dc61f', 'no_19146839066', '9800', '2021-09-29 03:28:30', '0', '674b126fec114cdd6a30b1c20f78b337', 'wx2903321270439344e04e5373c3b1ae0000');
INSERT INTO `ucenter_fee` VALUES ('89ba9b83852b345136e082b3c2ee502d', 'no_20193766297', '1000', '2021-09-29 03:28:45', '1', '674b126fec114cdd6a30b1c20f78b337', 'wx2903322747436761665507c5c2bb200000');
INSERT INTO `ucenter_fee` VALUES ('fc46bb1b82105fe8dcb6201144524e31', 'no_2231323253', '1000', '2021-09-29 21:33:20', '0', '53651178f09d6a52fbac415ac8681d66', 'wx292137031718795f9dc5b4b656c26b0000');

-- ----------------------------
-- Table structure for `ucenter_guanxi`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_guanxi`;
CREATE TABLE `ucenter_guanxi` (
  `uid` varchar(32) DEFAULT NULL,
  `hn_uid` varchar(32) DEFAULT NULL,
  `fu_uid` varchar(32) DEFAULT NULL,
  `mu_uid` varchar(32) DEFAULT NULL,
  `hpy_uid` varchar(32) DEFAULT NULL,
  `gx_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_guanxi
-- ----------------------------
INSERT INTO `ucenter_guanxi` VALUES ('674b126fec114cdd6a30b1c20f78b337', 'e444b3c199036739d6118db8ab6ecad0', null, null, null, '2021-09-29 21:45:38');
INSERT INTO `ucenter_guanxi` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', 'e444b3c199036739d6118db8ab6ecad0', null, null, null, '2021-09-24 22:36:06');
INSERT INTO `ucenter_guanxi` VALUES ('dc6e0c1be91f392055388db72e8d1366', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-24 22:40:34');
INSERT INTO `ucenter_guanxi` VALUES ('ab83682fb12e295829b1bffa236530ea', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-29 16:34:47');
INSERT INTO `ucenter_guanxi` VALUES ('e887f89db9c2436a886824b00bf581bd', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-29 17:24:38');
INSERT INTO `ucenter_guanxi` VALUES ('c80ab681709279b7e59bf6451164d4f0', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-30 01:34:59');

-- ----------------------------
-- Table structure for `ucenter_guanxi_hj`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_guanxi_hj`;
CREATE TABLE `ucenter_guanxi_hj` (
  `uid` varchar(32) DEFAULT NULL,
  `hn_uid` varchar(32) DEFAULT NULL,
  `fu_uid` varchar(32) DEFAULT NULL,
  `mu_uid` varchar(32) DEFAULT NULL,
  `hpy_uid` varchar(32) DEFAULT NULL,
  `gx_time` datetime DEFAULT NULL,
  UNIQUE KEY `uid_index` (`uid`,`hn_uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_guanxi_hj
-- ----------------------------
INSERT INTO `ucenter_guanxi_hj` VALUES ('674b126fec114cdd6a30b1c20f78b337', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-24 22:12:34');
INSERT INTO `ucenter_guanxi_hj` VALUES ('674b126fec114cdd6a30b1c20f78b337', 'e444b3c199036739d6118db8ab6ecad0', null, null, null, '2021-09-24 22:14:51');
INSERT INTO `ucenter_guanxi_hj` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-24 22:36:05');
INSERT INTO `ucenter_guanxi_hj` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', 'e444b3c199036739d6118db8ab6ecad0', null, null, null, '2021-09-24 22:36:06');
INSERT INTO `ucenter_guanxi_hj` VALUES ('dc6e0c1be91f392055388db72e8d1366', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-24 22:40:13');
INSERT INTO `ucenter_guanxi_hj` VALUES ('dc6e0c1be91f392055388db72e8d1366', 'e444b3c199036739d6118db8ab6ecad0', null, null, null, '2021-09-24 22:40:14');
INSERT INTO `ucenter_guanxi_hj` VALUES ('ab83682fb12e295829b1bffa236530ea', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-29 16:34:47');
INSERT INTO `ucenter_guanxi_hj` VALUES ('e887f89db9c2436a886824b00bf581bd', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-29 17:24:38');
INSERT INTO `ucenter_guanxi_hj` VALUES ('c80ab681709279b7e59bf6451164d4f0', '674b126fec114cdd6a30b1c20f78b337', null, null, null, '2021-09-30 01:34:59');

-- ----------------------------
-- Table structure for `ucenter_hongniang`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_hongniang`;
CREATE TABLE `ucenter_hongniang` (
  `hn_uid` varchar(50) DEFAULT NULL,
  `toupiao_uid` varchar(50) DEFAULT NULL,
  `creatime` datetime DEFAULT NULL,
  `fuwuhuiyuan` varchar(50) DEFAULT NULL,
  `fuwushouru` varchar(100) DEFAULT NULL,
  `biaoyu` varchar(100) DEFAULT NULL,
  `hnmobile` varchar(30) DEFAULT NULL,
  `shuoming` varchar(1000) DEFAULT NULL,
  `fuwufei` int(10) DEFAULT NULL,
  `isopen` int(1) DEFAULT NULL,
  UNIQUE KEY `hn_unique` (`hn_uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_hongniang
-- ----------------------------
INSERT INTO `ucenter_hongniang` VALUES ('674b126fec114cdd6a30b1c20f78b337', null, '2021-09-29 03:51:42', '普通会员', '100万以上', '选择我为您挑选省时间', '111111222', 'qq联系我把157595075', '198', '1');
INSERT INTO `ucenter_hongniang` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', null, '2021-09-20 16:27:54', '不限', '5万以下', '选择我为您约见保驾护航', '123456', '山东威海环翠区高区花园一路906号', '198', '1');
INSERT INTO `ucenter_hongniang` VALUES ('e444b3c199036739d6118db8ab6ecad0', null, '2021-09-20 21:01:06', '不限', '5万以下', '选择我为您约见保驾护航', '123456', '哈哈哈哈啊哈哈哈哈', '0', '1');

-- ----------------------------
-- Table structure for `ucenter_shoucanghn`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_shoucanghn`;
CREATE TABLE `ucenter_shoucanghn` (
  `myuid` varchar(32) DEFAULT NULL,
  `hnuid` varchar(32) DEFAULT NULL,
  `shoucangtime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_shoucanghn
-- ----------------------------
INSERT INTO `ucenter_shoucanghn` VALUES ('674b126fec114cdd6a30b1c20f78b337', '674b126fec114cdd6a30b1c20f78b337', '2021-09-24 22:12:55');
INSERT INTO `ucenter_shoucanghn` VALUES ('674b126fec114cdd6a30b1c20f78b337', 'e444b3c199036739d6118db8ab6ecad0', '2021-09-21 03:16:42');
INSERT INTO `ucenter_shoucanghn` VALUES ('e444b3c199036739d6118db8ab6ecad0', 'e444b3c199036739d6118db8ab6ecad0', '2021-09-22 13:50:15');
INSERT INTO `ucenter_shoucanghn` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '2021-09-24 22:36:17');
INSERT INTO `ucenter_shoucanghn` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', 'e444b3c199036739d6118db8ab6ecad0', '2021-09-24 22:36:18');
INSERT INTO `ucenter_shoucanghn` VALUES ('dc6e0c1be91f392055388db72e8d1366', 'e444b3c199036739d6118db8ab6ecad0', '2021-09-24 22:40:20');
INSERT INTO `ucenter_shoucanghn` VALUES ('dc6e0c1be91f392055388db72e8d1366', '674b126fec114cdd6a30b1c20f78b337', '2021-09-26 04:20:26');
INSERT INTO `ucenter_shoucanghn` VALUES ('674b126fec114cdd6a30b1c20f78b337', 'fa4029a1c16e5bb1715982e1bb095b14', '2021-09-29 05:03:50');
INSERT INTO `ucenter_shoucanghn` VALUES ('e887f89db9c2436a886824b00bf581bd', '674b126fec114cdd6a30b1c20f78b337', '2021-09-29 17:24:40');
INSERT INTO `ucenter_shoucanghn` VALUES ('032602991210dd644be04f16f2aea95d', '674b126fec114cdd6a30b1c20f78b337', '2021-09-29 21:49:12');
INSERT INTO `ucenter_shoucanghn` VALUES ('c80ab681709279b7e59bf6451164d4f0', '674b126fec114cdd6a30b1c20f78b337', '2021-09-30 01:34:57');

-- ----------------------------
-- Table structure for `ucenter_tuijian`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_tuijian`;
CREATE TABLE `ucenter_tuijian` (
  `uid` varchar(32) DEFAULT NULL,
  `usertype` int(3) DEFAULT NULL COMMENT '0颜值1高收入2高学历3好身材4高质量男性5高质量女性',
  `touid` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_tuijian
-- ----------------------------
INSERT INTO `ucenter_tuijian` VALUES ('62fa7cae653e4051abe917d18cbb59d0', '1', null);
INSERT INTO `ucenter_tuijian` VALUES ('62fa7cae653e4051abe917d18cbb59d0', '2', null);
INSERT INTO `ucenter_tuijian` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', '1', null);
INSERT INTO `ucenter_tuijian` VALUES ('e444b3c199036739d6118db8ab6ecad0', '2', null);
INSERT INTO `ucenter_tuijian` VALUES ('e444b3c199036739d6118db8ab6ecad0', '3', null);
INSERT INTO `ucenter_tuijian` VALUES ('e444b3c199036739d6118db8ab6ecad0', '1', null);
INSERT INTO `ucenter_tuijian` VALUES ('e444b3c199036739d6118db8ab6ecad0', '5', null);
INSERT INTO `ucenter_tuijian` VALUES ('e887f89db9c2436a886824b00bf581bd', '0', null);
INSERT INTO `ucenter_tuijian` VALUES ('e887f89db9c2436a886824b00bf581bd', '3', null);
INSERT INTO `ucenter_tuijian` VALUES ('032602991210dd644be04f16f2aea95d', '0', null);
INSERT INTO `ucenter_tuijian` VALUES ('c80ab681709279b7e59bf6451164d4f0', '0', null);
INSERT INTO `ucenter_tuijian` VALUES ('674b126fec114cdd6a30b1c20f78b337', '2', null);
INSERT INTO `ucenter_tuijian` VALUES ('674b126fec114cdd6a30b1c20f78b337', '3', null);
INSERT INTO `ucenter_tuijian` VALUES ('674b126fec114cdd6a30b1c20f78b337', '1', null);
INSERT INTO `ucenter_tuijian` VALUES ('674b126fec114cdd6a30b1c20f78b337', '5', null);

-- ----------------------------
-- Table structure for `ucenter_user_oplog`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_user_oplog`;
CREATE TABLE `ucenter_user_oplog` (
  `uid` varchar(32) NOT NULL COMMENT '用户ID',
  `op_no` varchar(32) DEFAULT NULL COMMENT '操作流水号',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `op_type` int(2) DEFAULT NULL COMMENT '操作类型',
  `op_dec` varchar(200) DEFAULT NULL COMMENT '操作描述',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户操作记录';

-- ----------------------------
-- Records of ucenter_user_oplog
-- ----------------------------

-- ----------------------------
-- Table structure for `ucenter_user_thirdinfo`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_user_thirdinfo`;
CREATE TABLE `ucenter_user_thirdinfo` (
  `uid` varchar(32) NOT NULL COMMENT '用户ID',
  `wx_id` varchar(50) DEFAULT NULL COMMENT '微信ID',
  `qq_id` varchar(50) DEFAULT NULL COMMENT '腾讯ID',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户三方信息';

-- ----------------------------
-- Records of ucenter_user_thirdinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `ucenter_userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_userinfo`;
CREATE TABLE `ucenter_userinfo` (
  `uid` varchar(32) NOT NULL DEFAULT '' COMMENT '用户ID',
  `userbianhao` varchar(32) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL COMMENT '用户名',
  `pwd` varchar(100) DEFAULT '' COMMENT '密码',
  `nickname` varchar(50) DEFAULT '' COMMENT '昵称',
  `sex` varchar(2) DEFAULT '' COMMENT '性别',
  `age` varchar(2) DEFAULT '' COMMENT '年龄',
  `addr` varchar(100) DEFAULT '' COMMENT '地址',
  `location` varchar(50) DEFAULT '' COMMENT '定位',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号',
  `guanzhu_count` int(8) DEFAULT '0' COMMENT '关注数',
  `fensi_count` int(8) DEFAULT '0' COMMENT '粉丝数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(1) unsigned zerofill DEFAULT '0' COMMENT '用户状态。0正常1删除2停用',
  `salt` varchar(50) DEFAULT NULL COMMENT 'md5密码盐',
  `lat` decimal(10,6) DEFAULT NULL COMMENT '纬度',
  `lng` decimal(10,6) DEFAULT NULL COMMENT '经度',
  `head` varchar(500) DEFAULT NULL COMMENT '头像',
  `qianming` varchar(500) DEFAULT NULL,
  `shengao` varchar(10) DEFAULT NULL,
  `shencai` varchar(10) DEFAULT NULL,
  `login_time` varchar(20) DEFAULT NULL,
  `education` varchar(10) DEFAULT NULL COMMENT '学历',
  `shouru` varchar(10) DEFAULT NULL,
  `jifen` int(11) DEFAULT '0',
  `regifrom` varchar(10) DEFAULT NULL,
  `fenghao` int(1) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `weiyi_username` (`username`),
  KEY `jifen` (`jifen`),
  KEY `time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户基础信息表';

-- ----------------------------
-- Records of ucenter_userinfo
-- ----------------------------
INSERT INTO `ucenter_userinfo` VALUES ('032602991210dd644be04f16f2aea95d', '6366106241', 'oKBv6wy6ccTE0W_7e-sq9edTh0wU', '94a14f56e4d84fc5b9c5f4e3c2dea315baf901d71b36ff298c6584dbd920c3f8', '越来越好', '0', '', '', '', null, '0', '0', '2021-09-29 21:04:16', null, '0', 'QgnVaBwm', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEJyQyNOz2Fsiaot2TZs2utQcrN4b1ibN5a9O0ZD7a0zU5cUiaPFtEoibdicm3Xj8hOfeIsnV9Yib4dTFtFg/132', null, null, null, null, null, null, '80', 'WX', null);
INSERT INTO `ucenter_userinfo` VALUES ('252b1d14e8b96d194cc9b449c90ea98a', null, '1133334444', '111111', '李斯', '0', '1', '111', '', '11', null, null, '2019-12-05 00:06:20', null, '0', null, null, null, '/head/sexboy.jpg', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('266c044e191bb489891d106bf6a4d4a3', '8913373762', '11111112', 'aa2fc937d45682dff6376640851da141', '111', '1', '11', '111', '', '111', '0', '0', '2021-09-05 20:10:30', null, '0', 'rgJWVDNf', null, null, '111', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('328d0d0f27ebe193c276c89471d2c070', '1718930304', '3111113', 'fc2445ef294b5ee5', '111', '1', '11', '111', '', '111', '0', '0', '2021-09-05 20:14:39', null, '0', 'vFbcQzNB', null, null, '111', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('3a8c3d78a575765bba49326e605fe4b2', '36727', '11111', 'a434bd0fc96bdadf', '111', '1', '21', '111', '', '111', '0', '0', '2021-09-05 20:02:13', null, '0', 'GBhe8Vxk', null, null, '111', null, null, null, null, null, null, '70', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('41479828b219239398cc75363fc6fa9f', null, 'elastic', '111111', '333333333', '0', '31', '333', '', '13426159617', null, null, '2019-12-05 00:00:59', null, '0', null, null, null, '/head/sexboy.jpg', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('44444444444444', null, 'test', '160d38b75f047246', 'suibin999', '1', '21', '11', '北京市海淀区', '1', '0', '0', '2019-12-05 00:00:59', '2020-01-11 06:45:27', '0', 'BPVCIgUd', null, null, '/head/timg.jpg', '11111', '160', '苗条', null, '本科', null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('53651178f09d6a52fbac415ac8681d66', '13571827412', 'oKBv6w16s4yqD96a6ROIz4gCtEKc', '262221f190301594f7d5416835e760348c1ec7be885ebe43d3a6ec335deb7bdd', '四眼舔dog', '1', '20', '', '', '13426159617', '0', '0', '2021-09-28 16:56:33', '2021-09-28 17:18:25', '0', 'cmAuDkWf', null, null, 'photo/1632820910273_DqxkAY.jpg', '1', '160', '中等', null, '本科', '5万以下', '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('5484f6fdc9b0faf1a32f31476d38c9d9', null, '1133334', '111111', '115t444', '1', '1', '111', '', '11', null, null, '2019-12-05 00:00:59', null, '0', null, null, null, '/head/timg.jpg', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('62fa7cae653e4051abe917d18cbb59d0', null, 'test2', '53cdcce12f4e3488', '张三', '11', '1', '111', '北京市海淀区', '11', '0', '0', '2020-01-11 06:27:37', '2020-01-11 06:29:46', '0', 'N4cbLBSl', null, null, '/head/timg.jpg', null, null, null, null, null, null, '20', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('62fa7cae653e4051abe917d18cbb59d2', null, '11', '111111', '张三1', '1', '19', '111', '', '11', null, null, '2019-12-04 23:58:28', null, '0', null, null, null, '/head/timg.jpg', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('674b126fec114cdd6a30b1c20f78b337', '16409656021', 'UID_1F4D2E16DAB58CCD8CBD4480C595C132', 'a59014ad1c445106b39789e0a65b1659a13ceb44be7b782241b2a3f1841520085cea3b2a79a90b00', '大S', '1', '30', '', '北京市-北京市-海淀区', '13426159617', '0', '0', '2021-09-12 06:04:13', '2021-09-30 03:29:18', '0', 'm7i9BQQI', null, null, 'photo/1632667949127_lRwdHN.jpg', '找个170以上的漂亮女孩，互相有眼缘...本人工作是搞互联网开发，有房，\n来聊聊', '188', 'S型', null, '硕士', '30-50万', '210', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('7b98d623f71434c0db3e446c3e479798', null, '5', 'c34c9f174077b009', '5', '5', '5', '5', '', '5', '0', '0', '2021-08-22 07:35:53', null, '0', 'K6ynxV9z', null, null, '5', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('87b1c6eff2c4a1b98cd381246c89fae8', null, '555555224', '111111', '11', '0', '1', '111', '', '11', null, null, '2019-12-05 00:08:32', null, '0', null, null, null, '/head/sexboy.jpg', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('88c068c2dde2ae8e1334878e81743a35', '36728', '111111', 'dbc2c98c2e211f28', '111', '1', '21', '111', '', '111', '0', '0', '2021-09-05 20:02:39', null, '0', 'Za62eAJb', null, null, '111', null, null, null, null, null, null, '90', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('8a551f76bd69079ea6e04f81e494c656', '5815488221', '2111113', '93fd2d0d0470d332', '111', '1', '11', '111', '', '111', '0', '0', '2021-09-05 20:14:37', null, '0', 'CNNcfpGe', null, null, '111', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('c80ab681709279b7e59bf6451164d4f0', '9147170592', 'oKBv6w7sBYovMieU7siO0q3EUDfU', 'dd9601523dc8bb08b7aaf6ce53c6e5897dd92ee030c3bb3dd9a114f3e36e2d02', '明天更美好', '1', '', '', '', null, '0', '0', '2021-09-29 21:20:47', null, '0', 'xdswUeAG', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/nMg3EGicm0bYqsceg9GVQPKsiaGibvHb24icTibezzrZ65YDPk0cd27QXNkuB1bsYonmC7wvfmh7VmKvwIbPib6rgHOg/132', null, null, null, null, null, null, '100', 'WX', null);
INSERT INTO `ucenter_userinfo` VALUES ('dc6e0c1be91f392055388db72e8d1366', '36726', '1111', '79950de29f79435b', '幸福一瞬间', '1', '18', '111', '山西省-太原市-小店区', '111', '0', '0', '2021-09-05 20:01:55', '2021-09-26 04:20:46', '0', 'ORnLfaGS', null, null, 'photo/1632596843315_KogWlG.jpg', '搭讪我', '160', '中等', null, '本科', '5万以下', '55335', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('e444b3c199036739d6118db8ab6ecad0', '36724', '111', '35d8b3835b83fb42', '美人', '1', '18', '111', '北京市-北京市-朝阳区', '111', '0', '0', '2021-09-05 19:59:54', '2021-09-08 18:20:11', '0', 'SMfuAUYh', null, null, 'photo/1631045743111_xKsuvF.jpg', '嘻嘻嘻嘻', '160', '大长腿', null, '硕士', '50-100万', '110', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('e887f89db9c2436a886824b00bf581bd', '3628348591', 'oKBv6w2LZZ3uVdp0VNBi0VF35pVk', '243cde33bc0546828ca753bf452c9096cc5426b06ea40881a73df30a60a2ddb8', '。', '1', '20', '', '', '1', '0', '0', '2021-09-29 17:19:44', '2021-09-29 17:21:25', '0', 'WXE67Du4', null, null, 'http://thirdwx.qlogo.cn/mmopen/vi_32/5TFsGRmNCKDjUbVKPZ6IiaIN3picF2PFYCBbRZL1Ujmzlc8tmZ06rIjVF2Jr8Pk4MTfVZia26qHuicOd8rkJSx2icMQ/132', '1', '160', '大长腿', null, '本科', '5万以下', '90', 'WX', null);
INSERT INTO `ucenter_userinfo` VALUES ('eccf1ea73ba27160c2c4154aa5185f33', '8913373763', '1111112', '49201a91a4d2c7e7', '111', '1', '11', '111', '', '111', '0', '0', '2021-09-05 20:11:03', null, '0', 'ExWPMZbz', null, null, '111', null, null, null, null, null, null, '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('fa4029a1c16e5bb1715982e1bb095b14', '3425425', '4', 'b2fd20fd14fac477', '我爱瑶瑶但是只能在心里', '0', '34', '4', '北京市-北京市-密云区', '1111', '0', '0', '2021-08-22 07:32:07', '2021-09-10 01:05:55', '0', 'nKp70aCK', null, null, 'photo/1631124960803_UtuYek.jpg', '对对对电动车', '189', '健壮', null, '本科', '5万以下', '0', null, null);
INSERT INTO `ucenter_userinfo` VALUES ('fd0789403c7294f424144c53425b0963', '36721', '1111113', '7f4bb469bd54aeb0', '111', '1', '11', '111', '', '111', '0', '0', '2021-09-05 20:12:59', null, '0', 'MOq3A03U', null, null, '111', null, null, null, null, null, null, '0', null, null);

-- ----------------------------
-- Table structure for `ucenter_yuejian`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_yuejian`;
CREATE TABLE `ucenter_yuejian` (
  `uuid` varchar(32) NOT NULL,
  `jiauid` varchar(32) DEFAULT NULL,
  `yiuid` varchar(32) DEFAULT NULL,
  `jiaok` int(1) DEFAULT NULL,
  `yiok` int(1) DEFAULT NULL,
  `jiatime` datetime DEFAULT NULL,
  `yitime` datetime DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `bianhao` varchar(32) DEFAULT NULL,
  `jiedanok` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_yuejian
-- ----------------------------
INSERT INTO `ucenter_yuejian` VALUES ('e47658713deaaef95dc8eb696f7809ff', 'fa4029a1c16e5bb1715982e1bb095b14', '62fa7cae653e4051abe917d18cbb59d0', '1', null, '2021-09-26 16:27:58', null, '2021-09-04 18:28:09', '12389798131', '0');
INSERT INTO `ucenter_yuejian` VALUES ('fe325b1abf997583f957b3e7aa784e8a', 'e444b3c199036739d6118db8ab6ecad0', 'fa4029a1c16e5bb1715982e1bb095b14', '1', '1', '2021-09-08 04:57:09', '2021-09-20 16:29:03', '2021-09-08 04:56:42', '2400546211', '0');
INSERT INTO `ucenter_yuejian` VALUES ('909b12193d7bf27ba5801defa7d899bc', '674b126fec114cdd6a30b1c20f78b337', 'e444b3c199036739d6118db8ab6ecad0', '1', '1', '2021-09-18 13:41:22', '2021-09-20 16:33:46', '2021-09-12 06:49:59', '19837935012', '0');
INSERT INTO `ucenter_yuejian` VALUES ('58c16f6ee89aef3fd3593cf3bccd5fa4', 'fa4029a1c16e5bb1715982e1bb095b14', '674b126fec114cdd6a30b1c20f78b337', '1', '1', '2021-09-20 16:29:47', '2021-09-20 21:17:37', '2021-09-20 16:29:39', '7071929241', '0');
INSERT INTO `ucenter_yuejian` VALUES ('aabd01cb35779096e2fc39440c6547d4', 'e444b3c199036739d6118db8ab6ecad0', '674b126fec114cdd6a30b1c20f78b337', '1', '1', '2021-09-26 14:47:13', '2021-09-26 17:22:30', '2021-09-22 14:14:46', '14610812031', '1');
INSERT INTO `ucenter_yuejian` VALUES ('07adc3419964cff116118f5f0dd8b30a', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '1', '1', '2021-09-26 20:31:14', '2021-09-26 17:21:22', '2021-09-26 07:31:58', '2352093702', '1');
INSERT INTO `ucenter_yuejian` VALUES ('2c5c9c48ab09cf2e5e2c904964d0b6c3', '44444444444444', 'dc6e0c1be91f392055388db72e8d1366', '1', '1', '2021-09-26 07:34:34', '2021-09-26 17:22:07', '2021-09-26 07:34:34', '9842180053', '1');
INSERT INTO `ucenter_yuejian` VALUES ('3f5062e30aa8f3a2ddb97f57f26ace50', '674b126fec114cdd6a30b1c20f78b337', '44444444444444', '1', null, '2021-09-29 21:46:04', null, '2021-09-26 08:16:18', '19820997102', '0');
INSERT INTO `ucenter_yuejian` VALUES ('979f9f4755a5f80cc77fffcb203bf56b', 'e444b3c199036739d6118db8ab6ecad0', '44444444444444', '1', null, '2021-09-29 14:07:40', null, '2021-09-26 14:36:54', '18113419712', '0');
INSERT INTO `ucenter_yuejian` VALUES ('ca7b7ac7a0aab9fad570166687085e60', 'dc6e0c1be91f392055388db72e8d1366', '62fa7cae653e4051abe917d18cbb59d0', '1', '1', '2021-09-26 20:53:26', '2021-09-26 20:57:58', '2021-09-26 20:53:26', '3949405812', '1');
INSERT INTO `ucenter_yuejian` VALUES ('168cae775843d24f52786e3ceecea167', 'ab83682fb12e295829b1bffa236530ea', '62fa7cae653e4051abe917d18cbb59d0', '1', '1', '2021-09-29 16:39:36', '2021-09-29 16:13:41', '2021-09-29 15:20:13', '1472136501', '1');
INSERT INTO `ucenter_yuejian` VALUES ('c70bc3e6b024b62a1b9a001b68032f24', 'ab83682fb12e295829b1bffa236530ea', '53651178f09d6a52fbac415ac8681d66', '1', null, '2021-09-29 17:01:50', null, '2021-09-29 16:44:37', 'yu_10821013391', '0');
INSERT INTO `ucenter_yuejian` VALUES ('5ba7daa21414b9145fc811192a82ca6b', '53651178f09d6a52fbac415ac8681d66', '032602991210dd644be04f16f2aea95d', '1', null, '2021-09-29 21:37:55', null, '2021-09-29 21:37:55', 'yu_9022470764', '0');
INSERT INTO `ucenter_yuejian` VALUES ('80a6b91414584f70f373c233262ee946', '032602991210dd644be04f16f2aea95d', '53651178f09d6a52fbac415ac8681d66', '0', '1', '2021-09-29 22:18:45', '2021-09-29 21:48:19', '2021-09-29 21:47:39', 'yu_11046329165', '0');
INSERT INTO `ucenter_yuejian` VALUES ('98519883ee56a1ba4000c349101b9334', '032602991210dd644be04f16f2aea95d', 'c80ab681709279b7e59bf6451164d4f0', '1', '1', '2021-09-29 22:15:37', '2021-09-30 01:36:20', '2021-09-29 22:15:37', 'yu_18808318596', '0');

-- ----------------------------
-- Table structure for `ucenter_yuejianimg`
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_yuejianimg`;
CREATE TABLE `ucenter_yuejianimg` (
  `yuejianid` varchar(32) DEFAULT NULL,
  `img` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ucenter_yuejianimg
-- ----------------------------
INSERT INTO `ucenter_yuejianimg` VALUES ('0', '');
INSERT INTO `ucenter_yuejianimg` VALUES ('14610812031', 'photo/1632899483039_fhPcSB.jpg|photo/1632899482686_uBAqFb.jpg|');
INSERT INTO `ucenter_yuejianimg` VALUES ('1472136501', 'photo/1632936296848_wJuOAj.jpg|');
INSERT INTO `ucenter_yuejianimg` VALUES ('3949405812', 'photo/1632934526196_lYzrJg.jpg|');
INSERT INTO `ucenter_yuejianimg` VALUES ('9842180053', 'photo/1632934575279_ypTmCB.jpg|photo/1632934575479_IflDyN.jpg|photo/1632934575127_yLOFVC.jpg|');
INSERT INTO `ucenter_yuejianimg` VALUES ('2352093702', 'photo/1632934613389_SNHgdc.jpg|photo/1632934613515_LbqpKx.jpg|');
